
#include "MUQ/Inference/MCMC/MMALA.h"

// standard library includes
#include <string>

#include <boost/property_tree/ptree.hpp>

// other muq related includes
#include "MUQ/Modelling/RandVarBase.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"
#include "MUQ/Utilities/LogConfig.h"

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

REGISTER_MCMCPROPOSAL(MMALA);

/** This constructor defins the Langevin diffusion MCMC by reading necessary parameters in
 *  paramList
 */
MMALA::MMALA(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
             boost::property_tree::ptree                            & properties) : MCMCProposal(inferenceProblem,
                                                                                                 properties)
{
  LOG(INFO) << "Constructing Simplified Manifold MALA MCMC";

  stepLength    = ReadAndLogParameter(properties, "MCMC.MMALA.StepLength", 1.0);
  stepLengthExp = log(stepLength);

  acceptanceTarget = ReadAndLogParameter(properties, "MCMC.MMALA.AcceptanceTarget", 0.6);

  stepLengthScaling = ReadAndLogParameter(properties, "MCMC.MMALA.StepLengthScaling", 1.0);

  adaptDuration = ReadAndLogParameter(properties, "MCMC.MMALA.AdaptDuration", 5000);

  adaptBatchSize = ReadAndLogParameter(properties, "MCMC.MMALA.AdaptFrequency", 50);

  samplingProblem->SetStateComputations(true, true, true);
}

/** Takes a step using a shifted isotropic proposal.  The shift is defined by the gradient and proposal size. */
std::shared_ptr<muq::Inference::MCMCState> MMALA::DrawProposal(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
  int const                                     currIteration)
{
  auto forwardProposal           = ConstructMMALAProposal(currentState);
  Eigen::VectorXd proposedVector = forwardProposal->rv->Sample();

  return samplingProblem->ConstructState(proposedVector, currIteration, logEntry);
}

double MMALA::ProposalDensity(std::shared_ptr<muq::Inference::MCMCState> currentState,
                              std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
  auto   forwardProposal = ConstructMMALAProposal(currentState);
  double forwardDens     = forwardProposal->density->LogDensity(proposedState->state);

  return forwardDens;
}

std::shared_ptr<GaussianPair> MMALA::ConstructMMALAProposal(shared_ptr<MCMCState> const state)
{
  Eigen::VectorXd mean = state->state + stepLength * stepLength / 2.0 *
                         state->GetMetric().colPivHouseholderQr().solve(state->GetDensityGrad());
  Eigen::MatrixXd precision =  (1.0 / stepLength / stepLength) * state->GetMetric();


  auto proposalDistribution = make_shared<GaussianPair>(mean,precision,GaussianSpecification::PrecisionMatrix);

  return proposalDistribution;
}

void MMALA::PostProposalProcessing(std::shared_ptr<muq::Inference::MCMCState> const newState,
                                   MCMCProposal::ProposalStatus const               proposalAccepted,
                                   int const                                        iteration,
                                   std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry)
{
  if (proposalAccepted == MCMCProposal::ProposalStatus::notRun) {
    return; //not run, we didn't learn anything
  } else if (proposalAccepted == MCMCProposal::ProposalStatus::accepted) {
    ++acceptedThisBatch;
  }

  ++iterationWithinBatch;

  if ((iteration < adaptDuration) && (iterationWithinBatch % adaptBatchSize == 0)) {
    if (acceptedThisBatch < .4 * adaptBatchSize) {
      stepLength *= 0.8;
    } else if (acceptedThisBatch > .7 * adaptBatchSize) {
      stepLength *= 1.2;
    }

    //log just before it gets wiped
    logEntry->loggedData["batchAcceptedPercent"] = Eigen::VectorXd::Constant(1,
                                                                             static_cast<double>(acceptedThisBatch) /
                                                                             static_cast<double>(adaptBatchSize));
    acceptedThisBatch = 0;
  } else {
    //or log the value in progress on the other iterations
    logEntry->loggedData["batchAcceptedPercent"] = Eigen::VectorXd::Constant(1,
                                                                             static_cast<double>(acceptedThisBatch) /
                                                                             static_cast<double>(iterationWithinBatch %
                                                                                                 adaptBatchSize));
  }
  logEntry->loggedData["stepLength"] = Eigen::VectorXd::Constant(1, stepLength);
}

void MMALA::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "MMALA");
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MMALA - Step size", stepLength);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MMALA - Acceptance Target", acceptanceTarget);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MMALA - Length Scaling", stepLengthScaling);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MMALA - Adapt Duration", adaptDuration);
  HDF5Wrapper::WriteScalarAttribute(groupName, prefix + "MMALA - Adapt Batch Size", adaptBatchSize);
}

