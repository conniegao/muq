#include "MUQ/Inference/MCMC/LocalApproximationKernel.h"

#include <MUQ/Utilities/LogConfig.h>
#include <MUQ/Utilities/RandomGenerator.h>
#include <MUQ/Modelling/EmpiricalRandVar.h>
#include <MUQ/Modelling/ModGraphPiece.h>
#include <MUQ/Modelling/VectorPassthroughModel.h>

#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"
#include <MUQ/Approximation/Incremental/GPApproximator.h>

using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

REGISTER_MCMCKERNEL(LocalApproximationKernel);

LocalApproximationKernel::LocalApproximationKernel(std::shared_ptr<muq::Inference::AbstractSamplingProblem> density,
                                                   boost::property_tree::ptree                            & properties)
   : MCMCKernel(density, properties)
{
  string approximationCutStart = ReadAndLogParameter<string>(properties,
                                                             "MCMC.LocalApproximationKernel.approximationCutStart",
                                                             "forwardModel");
  string approximationCutEnd = ReadAndLogParameter<string>(properties,
                                                           "MCMC.LocalApproximationKernel.approximationCutEnd",
                                                           "forwardModel");
  string approximationModelName = ReadAndLogParameter<string>(properties,
                                                              "MCMC.LocalApproximationKernel.approximationModelName",
                                                              "inferenceApproximation");


    assert(approximationCutStart.compare(approximationCutEnd) != 0); //cannot start and end cut on the same node

  auto graph = density->GetGraph();

  bool endIsDensity = static_cast<bool>(dynamic_pointer_cast<Density>(graph->GetNodeModel(approximationCutEnd)));


  auto graphToApprox = graph->ReplaceNodeWithInput(approximationCutStart, approximationCutEnd);

  string nodeToApproxGraphviz = ReadAndLogParameter<string>(properties,
                                                            "MCMC.LocalApproximationKernel.graphToApproxOutput",
                                                            "");

  if (nodeToApproxGraphviz.compare("") != 0) {
    graphToApprox->writeGraphViz(nodeToApproxGraphviz);
  }

    assert(graphToApprox->NumInputs() == 1);

    assert(graphToApprox->NumOutputs() == 1);

  auto nodeToApprox = ModGraphPiece::Create(graphToApprox, approximationCutEnd);


  string modelType = ReadAndLogParameter<string>(properties,
                                                 "MCMC.LocalApproximationKernel.ApproximationType",
                                                 "polynomial");
  
  //construct the appropriate approximator, usingthe WithModes variants so the modpieces can return CV values
  if (modelType.compare("polynomial") == 0) {
    //since this property is required, make sure it is set
    properties.put("PolynomialRegressor.CrossValidate", 1);

    approximateModPiece = make_shared<PolynomialApproximatorWithModes>(nodeToApprox, properties);
  } else if (modelType.compare("gp") == 0) {
    approximateModPiece = make_shared<GPApproximatorWithModes>(nodeToApprox, properties);
  } else {
    cerr << "The provided approximator type is not available: " << modelType << endl;
    assert(false);
  }

  graph->AddNode(approximateModPiece, approximationModelName);

  graph->AddEdge(approximationCutStart, approximationModelName, 0);

  if (endIsDensity) {
    //if the end was a density, replace it with one
    //move through a temporary to avoid wiping out anything accidentally
    graph->AddNode(make_shared<VectorPassthroughDensity>(), approximationCutEnd + "_tmp");
    graph->MoveOutputEdges(approximationCutEnd, approximationCutEnd + "_tmp");
    graph->RemoveNode(approximationCutEnd);
    graph->AddNode(make_shared<VectorPassthroughDensity>(), approximationCutEnd);
    graph->MoveOutputEdges(approximationCutEnd + "_tmp", approximationCutEnd);

    //and pipe the approximation into it, so as to make sure it is effectively a density
    graph->AddEdge(approximationModelName, approximationCutEnd, 0);
  } else {
    //otherwise it's fine as it is, so just move the edges to this new node
    graph->MoveOutputEdges(approximationCutEnd, approximationModelName);
  }

  auto newGraph = graph->DependentCut("posterior");
  density->SetGraph(newGraph);

  string graphWithApproxGraphviz = ReadAndLogParameter<string>(properties,
                                                               "MCMC.LocalApproximationKernel.graphWithApproxOutput",
                                                               "");

  if (graphWithApproxGraphviz.compare("") != 0) {
    newGraph->writeGraphViz(graphWithApproxGraphviz);
  }

  //if needed, create a new gaussian fisher information metric and place it in the inference object
  bool useGaussianMetric = ReadAndLogParameter(properties, "MCMC.LocalApproximationKernel.CreateGaussianMetric", false);
  if (useGaussianMetric) {
    string priorName = ReadAndLogParameter<string>(properties,
                                                   "MCMC.LocalApproximationKernel.CreateGaussianMetric_prior",
                                                   "prior");
    string likelihoodName = ReadAndLogParameter<string>(properties,
                                                        "MCMC.LocalApproximationKernel.CreateGaussianMetric_likelihood",
                                                        "likelihood");
    string modelName = ReadAndLogParameter<string>(properties,
                                                   "MCMC.LocalApproximationKernel.CreateGaussianMetric_model",
                                                   "forwardModel");
    auto metric =
      make_shared<GaussianFisherInformationMetric>(dynamic_pointer_cast<GaussianDensity>(newGraph->GetNodeModel(
                                                                                      likelihoodName)),
                                                   newGraph->GetNodeModel(modelName),
                                                   dynamic_pointer_cast<GaussianDensity>(newGraph->GetNodeModel(priorName)));
    density->SetMetric(metric);
  }

  //read a bunch of parameters
  maxRefinementsPerEval = ReadAndLogParameter(properties,
                                              "MCMC.LocalApproximationKernel.maxRefinementsPerEval",
                                              maxRefinementsPerEval);
  baseBeta      = ReadAndLogParameter(properties, "MCMC.LocalApproximationKernel.baseBeta", baseBeta);
  baseGamma     = ReadAndLogParameter(properties, "MCMC.LocalApproximationKernel.baseGamma", baseGamma);
  betaExponent  = ReadAndLogParameter(properties, "MCMC.LocalApproximationKernel.betaExponent", betaExponent);
  gammaExponent = ReadAndLogParameter(properties, "MCMC.LocalApproximationKernel.gammaExponent", gammaExponent);
  referenceRun  = ReadAndLogParameter(properties, "MCMC.LocalApproximationKernel.referenceRun", referenceRun);

  //if it's a reference run, set the mode of the approximator accordingly
  if (referenceRun) {
    approximateModPiece->SetMode(PolynomialApproximator::trueDirect);
  }

  initialSampleRadius = ReadAndLogParameter(properties,
                                            "MCMC.LocalApproximationKernel.initialSampleRadius",
                                            initialSampleRadius);
  proposal = MCMCProposal::Create(density, properties);
}

void LocalApproximationKernel::SetupStartState(const Eigen::VectorXd& xStart)
{
  assert(approximateModPiece);
  //before performing the usual setup, ensure that there are enough points to get started

  if (!referenceRun) {
    //for as many samples as we need to make up, generate new samples near the start point
      LOG(INFO) << "Checking whether more cache evaluations are needed: " <<
      approximateModPiece->NumAvailableNearestNeighbors() << " of " << approximateModPiece->NumNearestNeighbors();
    while (approximateModPiece->NumAvailableNearestNeighbors() < approximateModPiece->NumNearestNeighbors()) {
      LOG(INFO) << "Initial population of approximate model, has " <<
        approximateModPiece->NumAvailableNearestNeighbors() << " of " << approximateModPiece->NumNearestNeighbors();
      //this is a hacky rule to guess where the points should go
      Eigen::VectorXd newSample;
      if (initialSampleRadius > 0) {
        newSample = xStart + RandomGenerator::GetNormalRandomVector(xStart.rows()) * initialSampleRadius;
      } else {
        newSample = xStart + RandomGenerator::GetNormalRandomVector(xStart.rows()) * xStart.norm() / 10.0;
      }      LOG(INFO) << "new sample: " << newSample.transpose();
      //generate the new samples by using the sampling problem graph
      approximateModPiece->AddToCache(newSample);
    }
  }
}

void LocalApproximationKernel::PrintStatus() const
{
  std::cout << "  LocalApproximationKernel: Acceptance rate = " << std::setw(5) << std::fixed << std::setprecision(1) <<
    100.0 * GetAccept() << "%\n";
}

double LocalApproximationKernel::GetAccept() const
{
  return static_cast<double>(numAccepts) / static_cast<double>(totalSteps);
}

std::shared_ptr<muq::Inference::MCMCState> LocalApproximationKernel::ConstructNextState(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  int const                                     iteration,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry)
{
  totalSteps++;

  //    LOG(INFO) << "iteration "<< iteration;
  assert(currentState);

  //reset to standard mode, just in case!
  if (!referenceRun) {
    approximateModPiece->SetMode(PolynomialApproximator::standard);
  }
  samplingProblem->InvalidateCaches(); //be sure to invalidate any caching in the sampling problem

  bool isAccepted;
  std::shared_ptr<MCMCState> proposedState;

  int betaRefines  = 0;
  int gammaRefines = 0;
  int badProposal  = 0;

  double gamma = baseGamma * pow(static_cast<double>(iteration), gammaExponent);
  double beta  = baseBeta * pow(static_cast<double>(iteration), betaExponent);

  //record whichever is worse
  logEntry->loggedData["beta"]  = Eigen::VectorXd::Constant(1, beta);
  logEntry->loggedData["gamma"] = Eigen::VectorXd::Constant(1, gamma);


  //might not get written, so make sure the value is sensible
  //   logEntry->loggedData["cvIndicator"] = Eigen::VectorXd::Constant(1, 0);

  if (referenceRun) { //if reference run, no approx happens, so just run the proposal
    proposedState = proposal->DrawProposal(currentState, logEntry);
  } else {            //else actually do everything
    //loop over the number of possible refinements, until we're happy

    //get the random draws for beta before we mess with the random generator
    Eigen::VectorXd betas     = RandomGenerator::GetUniformRandomVector(maxRefinementsPerEval);
    Eigen::VectorXd betaSides = RandomGenerator::GetUniformRandomVector(maxRefinementsPerEval);

    //grab randomness
    auto randomState = RandomGenerator::CopyGenerator();

    //loop over refinements
    for (int i = 0; i < maxRefinementsPerEval; ++i) {
      //                LOG(INFO) << "iter " << i;
      RandomGenerator::SetGenerator(randomState); //make sure they all use the same generator
      proposedState = proposal->DrawProposal(currentState, logEntry);

      if (!proposedState) {
        LOG(INFO) << "bad prior";
        badProposal++;
        break;
      }


      //next, check for beta refinement
      if (betas(i) < beta) {      //when needed,
        // LOG(INFO) <<"beta";
        betaRefines++;            //record that it happened
        if (betaSides(i) < 0.5) { //perform on either current or proposed state
          if (!approximateModPiece->RefineNear(currentState->state)) {
            badProposal++;
            break;
          }

          currentState = samplingProblem->ConstructState(currentState->state, iteration, logEntry);
          assert(currentState); //if we broke the approximation, something must be very wrong

          continue;             //go back to the top
        } else {
          if (!approximateModPiece->RefineNear(proposedState->state)) {
            badProposal++;
            break;
          }

          proposedState = samplingProblem->ConstructState(proposedState->state, iteration, logEntry);
          continue; //go back to the top
        }
      }

      //next check the cross validation
      double nominalProbability = ComputeAcceptanceProbability(currentState, proposedState, false, false);

      assert(approximateModPiece->GetNumberOfCrossValidationFits() > 0); //there must actually be some cross validation to do! If not, perhaps it was turned off?

      //error indicator on proposedState side
      double worstProposedIndicator = 0;
      for (int j = 0; j < approximateModPiece->GetNumberOfCrossValidationFits(); ++j) {
        approximateModPiece->SetCrossValidationIndex(j);
        samplingProblem->InvalidateCaches(); //be sure to invalidate any caching in the sampling problem
        double cvProbability = ComputeAcceptanceProbability(currentState, proposedState, false, true);
        //error indicator is forward and reverse versions of acceptance probability
        double errorIndicator =
          fabs(min(1.0,
                   nominalProbability) -
               min(1.0, cvProbability)) + fabs(min(1.0, 1.0 / nominalProbability) - min(1.0, 1.0 / cvProbability));
        worstProposedIndicator = max(worstProposedIndicator, errorIndicator / 2.0); //that was the doubled error
                                                                                    // indicator
      }

      //error indicator on currentState side
      double worstCurrentIndicator = 0;
      for (int j = 0; j < approximateModPiece->GetNumberOfCrossValidationFits(); ++j) {
        approximateModPiece->SetCrossValidationIndex(j);
        samplingProblem->InvalidateCaches(); //be sure to invalidate any caching in the sampling problem
        double cvProbability = ComputeAcceptanceProbability(currentState, proposedState, true, false);
        //error indicator is forward and reverse versions of acceptance probability

        double errorIndicator =
          fabs(min(1.0,
                   nominalProbability) -
               min(1.0, cvProbability)) + fabs(min(1.0, 1.0 / nominalProbability) - min(1.0, 1.0 / cvProbability));

        worstCurrentIndicator = max(worstCurrentIndicator, errorIndicator / 2.0); //that was the doubled error indicator
      }


      //reset to standard mode!
      approximateModPiece->SetMode(PolynomialApproximator::standard);
      samplingProblem->InvalidateCaches(); //be sure to invalidate any caching in the sampling problem


      //record whichever is worse
      if (i == 0) {
        logEntry->loggedData["cvIndicator"] =
          Eigen::VectorXd::Constant(1, max(worstProposedIndicator, worstCurrentIndicator));
      }
      // LOG(INFO) << "cv final " << worstCurrentIndicator << " " << worstProposedIndicator;
      //if either is worse than the threshold, trigger on the worse cv indicator
      if ((worstCurrentIndicator > gamma) && (worstCurrentIndicator > worstProposedIndicator)) {
        //                LOG(INFO) << "gamma1";
        gammaRefines++;

        if (!approximateModPiece->RefineNear(currentState->state)) {
          LOG(INFO) << "can't refine";
          badProposal++;
          break;
        }

        //refresh current state with updated value, which gets passed back, so this update sticks even if the proposal
        // fails.
        currentState = samplingProblem->ConstructState(currentState->state, iteration, logEntry);
        assert(currentState); //if we broke the approximation, something must be very wrong

        continue;
      } else if ((worstProposedIndicator > gamma) && (worstProposedIndicator >= worstCurrentIndicator)) {
        //                 LOG(INFO) << "gamma2";
        gammaRefines++;
        if (!approximateModPiece->RefineNear(proposedState->state)) {
          LOG(INFO) << "can't refine";
          badProposal++;
          break;
        }

        //Refresh proposed state with refinement, in case we're out of refinement iterations and we don't go back to the
        //top. This might get repeated at the top of the loop, but caching makes that not matter.
        proposedState = samplingProblem->ConstructState(proposedState->state, iteration, logEntry);

        continue; //go back to the top
      }

      break;      //no refinements needed, so proceed with accept/reject
    }
  }

  if (proposedState) {
    double gammaAccept = ComputeAcceptanceProbability(currentState, proposedState, false, false);

    logEntry->loggedData["acceptanceProbability"] = Eigen::VectorXd::Constant(1, gammaAccept);

    isAccepted = (RandomGenerator::GetUniform() < gammaAccept);
  } else {
    isAccepted = false;
  }

  //allow an adaptive algorithm to do it's thing
  if (isAccepted) {
    proposal->PostProposalProcessing(proposedState, MCMCProposal::accepted, iteration, logEntry);
  } else {
    proposal->PostProposalProcessing(currentState, MCMCProposal::rejected, iteration, logEntry);
  }


  logEntry->loggedData["isAccepted"] = Eigen::VectorXd::Constant(1, isAccepted);

  logEntry->loggedData["betaRefines"]      = Eigen::VectorXd::Constant(1, betaRefines);
  logEntry->loggedData["gammaRefines"]     = Eigen::VectorXd::Constant(1, gammaRefines);
  logEntry->loggedData["badProposal"]      = Eigen::VectorXd::Constant(1, badProposal);
  logEntry->loggedData["totalEvaluations"] = Eigen::VectorXd::Constant(1,
                                                                       approximateModPiece->NumAvailableNearestNeighbors());

  if (isAccepted) {
    return proposedState;
  } else {
    numAccepts++;

    return currentState;
  }
}

double LocalApproximationKernel::ComputeAcceptanceProbability(std::shared_ptr<muq::Inference::MCMCState> currentStateIn,
                                                              std::shared_ptr<muq::Inference::MCMCState> proposedStateIn,
                                                              bool                                       recomputeCurrent,
                                                              bool                                       recomputeProposed)
{
  std::shared_ptr<muq::Inference::MCMCState> currentState;
  std::shared_ptr<muq::Inference::MCMCState> proposedState;


  assert(currentStateIn); //must get a state

  //recompute the states as requested
  if (recomputeCurrent) {
    currentState = samplingProblem->ConstructState(currentStateIn->state, 0, nullptr);
  } else {
    currentState = currentStateIn;
  }

  assert(currentState); //must end up with a current state

  //do not accept if proposal doesn't exist, skip the rest
  if (proposedStateIn == nullptr) {
    return 0.0;
  }

  if (recomputeProposed) {
    proposedState = samplingProblem->ConstructState(proposedStateIn->state, 0, nullptr);
  } else {
    proposedState = proposedStateIn;
  }

  assert(currentState);
  // LOG(INFO) << "prop " <<  proposedState->GetDensity()<< " " <<  currentState->GetDensity();
  //this part is copied from MHKernel::ComputeAcceptanceProbability
  if (!proposedState) {
    return 0;
  } else {
    return exp(proposedState->GetDensity() - currentState->GetDensity() -
               proposal->ProposalDensity(currentState,
                                         proposedState) + proposal->ProposalDensity(proposedState, currentState));
  }
}

