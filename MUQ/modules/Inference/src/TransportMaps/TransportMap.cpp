
// self include
#include "MUQ/Inference/TransportMaps/TransportMap.h"


#include <Eigen/Eigenvalues>

using namespace muq::Inference;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace std;
                           
//bool PolynomialCheck(const std::vector<BasisSet>& basesIn)
//{
//  // check to see if any terms can be casted as an RBF
//  for (int d = 0; d < basesIn.size(); ++d) {
//    for (int term = 0; term < basesIn.at(d).size(); ++term) {
//      auto temp = std::dynamic_pointer_cast<PolynomialBasisBase>(basesIn.at(d).at(term));
//      if (!temp) {
//        return false;
//      }
//    }
//  }
//  return true;
//}

//TransportMap::TransportMap(int dimIn, int dimOut) : OneInputJacobianModPiece(dimIn,dimOut){};

TransportMap::TransportMap(int inputDim,
                           const vector<Eigen::VectorXd>&                         coeffsIn) : OneInputJacobianModPiece(inputDim,coeffsIn.size()),
                                                                                              coeffs(coeffsIn)
{
  assert(coeffs.size() == outputSize);
}


void TransportMap::SetCoeffs(std::vector<Eigen::VectorXd> const& coeffsIn){
  
  assert(coeffsIn.size()==coeffs.size());
  for(int i=0; i<coeffs.size(); ++i)
    assert(coeffs.at(i).size()==coeffsIn.at(i).size());
  
  coeffs = coeffsIn;
}
