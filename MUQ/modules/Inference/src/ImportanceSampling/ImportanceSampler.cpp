#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/AnalyticFunctions/ExpModel.h"

#include "MUQ/Utilities/VectorTranslater.h"

#include "MUQ/Inference/ImportanceSampling/ImportanceSampler.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Inference;

ImportanceSampler::ImportanceSampler(shared_ptr<ModGraph> const& graph, boost::property_tree::ptree const& para) :
  ModPiece(InputSizes(graph, para), OutputSize(graph, para), false, false, false, false, true), N(para.get("ImportanceSampler.N", 100)), normalize(para.get("ImportanceSampler.Normalize", true))
{
  // get the name of the parameter being marginalized 
  const string marginalizedNode = para.get<string>("ImportanceSampler.MarginalizedNode", "");
  assert(graph->NodeExists(marginalizedNode));

  // get the names of the output nodes 
  const string outputNames = para.get<string>("ImportanceSampler.OutputNames", "");

  // convert the comma-seperated list to a vector 
  const vector<string> names = ListToVector(outputNames);

  map<string, int> indexMap;
  indexMap.insert(pair<string, int>(marginalizedNode, -1));

  unsigned int numInputs = 0;

  vector<int> fIndices = InputIndices(graph, names[0], marginalizedNode, indexMap, numInputs);
  fPair =
    pair<shared_ptr<ModGraphPiece>, vector<int> >
      (ModGraph::ConstructModPiece(graph, names[0], graph->InputNames(names[0])), fIndices);

  fInd = -1;
  for (unsigned int i = 0; i < fPair.second.size(); ++i) {
    if (fPair.second[i] == -1) {
      fInd = i;
      break;
    }
  }

  vector<int> piIndices = InputIndices(graph, names[1], marginalizedNode, indexMap, numInputs);
  piPair =
    pair<shared_ptr<ModPieceDensity>,
         vector<int> >(ModGraph::ConstructDensity(graph, names[1], graph->InputNames(names[1])), piIndices);

  piInd = -1;
  for (unsigned int i = 0; i < piPair.second.size(); ++i) {
    if (piPair.second[i] == -1) {
      piInd = i;
      break;
    }
  }

  vector<int> qDensIndices = InputIndices(graph, names[2], marginalizedNode, indexMap, numInputs);
  qDensPair =
    pair<shared_ptr<ModPieceDensity>, vector<int> >
      (ModGraph::ConstructDensity(graph, names[2], graph->InputNames(names[2])), qDensIndices);

  qInd = -1;
  for (unsigned int i = 0; i < qDensPair.second.size(); ++i) {
    if (qDensPair.second[i] == -1) {
      qInd = i;
      break;
    }
  }

  vector<int> qRVIndices = InputIndices(graph, names[3], marginalizedNode, indexMap, numInputs);
  qRVPair =
    pair<shared_ptr<ModGraphPiece>, vector<int> >
      (ModGraph::ConstructModPiece(graph, names[3], graph->InputNames(names[3])), qRVIndices);
}

ImportanceSampler::ImportanceSampler(shared_ptr<ModGraph> const& graph,
                                     vector<string> const      & names,
                                     string const              & marginalizedNode,
                                     unsigned int const          N,
                                     bool const                  normalize) :
  ModPiece(InputSizes(graph, names,
                      marginalizedNode), graph->GetNodeModel(names[0])->outputSize, false, false, false, false,
           true), N(N), normalize(normalize)
{
  map<string, int> indexMap;
  indexMap.insert(pair<string, int>(marginalizedNode, -1));

  unsigned int numInputs = 0;

  vector<int> fIndices = InputIndices(graph, names[0], marginalizedNode, indexMap, numInputs);
  fPair =
    pair<shared_ptr<ModGraphPiece>, vector<int> >
      (ModGraph::ConstructModPiece(graph, names[0], graph->InputNames(names[0])), fIndices);

  fInd = -1;
  for (unsigned int i = 0; i < fPair.second.size(); ++i) {
    if (fPair.second[i] == -1) {
      fInd = i;
      break;
    }
  }

  vector<int> piIndices = InputIndices(graph, names[1], marginalizedNode, indexMap, numInputs);
  piPair =
    pair<shared_ptr<ModPieceDensity>,
         vector<int> >(ModGraph::ConstructDensity(graph, names[1], graph->InputNames(names[1])), piIndices);

  piInd = -1;
  for (unsigned int i = 0; i < piPair.second.size(); ++i) {
    if (piPair.second[i] == -1) {
      piInd = i;
      break;
    }
  }

  vector<int> qDensIndices = InputIndices(graph, names[2], marginalizedNode, indexMap, numInputs);
  qDensPair =
    pair<shared_ptr<ModPieceDensity>, vector<int> >
      (ModGraph::ConstructDensity(graph, names[2], graph->InputNames(names[2])), qDensIndices);

  qInd = -1;
  for (unsigned int i = 0; i < qDensPair.second.size(); ++i) {
    if (qDensPair.second[i] == -1) {
      qInd = i;
      break;
    }
  }

  vector<int> qRVIndices = InputIndices(graph, names[3], marginalizedNode, indexMap, numInputs);
  qRVPair =
    pair<shared_ptr<ModGraphPiece>, vector<int> >
      (ModGraph::ConstructModPiece(graph, names[3], graph->InputNames(names[3])), qRVIndices);
}

int ImportanceSampler::OutputSize(shared_ptr<ModGraph> const& graph, boost::property_tree::ptree const& para) {
  // get the names of the output nodes
  const string outputNames = para.get<string>("ImportanceSampler.OutputNames", "");

  // get the name of the function (we are trying to get the expectation of this function)
  const string func = ListToVector(outputNames)[0];
  assert(graph->NodeExists(func));

  return graph->GetNodeModel(func)->outputSize;
}

Eigen::VectorXi ImportanceSampler::InputSizes(shared_ptr<ModGraph> const& graph, boost::property_tree::ptree const& para) 
{
  // get the name of the parameter being marginalized 
  const string marginalizedNode = para.get<string>("ImportanceSampler.MarginalizedNode", "");
  assert(graph->NodeExists(marginalizedNode));

  // get the names of the output nodes 
  const string outputNames = para.get<string>("ImportanceSampler.OutputNames", "");

  // convert the comma-seperated list to a vector 
  const vector<string> names = ListToVector(outputNames);

  // a map from the name of each input to the number of inputs for that node 
  map<string, unsigned int> indexMap;

  // the number of inputs 
  unsigned int numInputs = 0;

  for( unsigned int i=0; i<names.size(); ++i ) {
    assert(graph->NodeExists(names[i]));

    // get the inputs for this output 
    const vector<string> inputNames = graph->InputNames(names[i]);

    // for input input 
    for( unsigned int j=0; j<inputNames.size(); ++j ) {
      // try to put that input into the map
      if( indexMap.insert(pair<string, unsigned int>(inputNames[j], numInputs)).second ) {
	++numInputs;
      }
    }
  }

  // the input sizes 
  vector<unsigned int> inputSizesVec;
  
  // loop through the input nodes 
  for( map<string, unsigned int>::iterator it=indexMap.begin(); it!=indexMap.end(); ++it ) {
    // if its not the marginalized node 
    if( marginalizedNode.compare(it->first)!=0 ) {
      // the names and sizes of the inputs
      const vector<string> localNames  = graph->InputNames(it->first);
      const Eigen::VectorXi localSizes = ModGraph::ConstructModPiece(graph, it->first, localNames)->inputSizes;
      
      for( unsigned int i=0; i<localSizes.size(); ++i ) {
	// if its not the marginalized node
        if (marginalizedNode.compare(localNames[i]) != 0) {
	  // add them to the input sizes 
	  inputSizesVec.push_back(localSizes(i));
	}
      }
    }
  }

  // translate to eigen and return
  Translater<vector<unsigned int>, Eigen::VectorXi> trans(inputSizesVec);
  return *trans.GetPtr();
}

Eigen::VectorXi ImportanceSampler::InputSizes(shared_ptr<muq::Modelling::ModGraph> const& graph,
                                              vector<string> const                      & names,
                                              string const                              & marginalizedNode)
{
  map<string, unsigned int> indexMap;

  unsigned int numInputs = 0;
  for (unsigned int i = 0; i < names.size(); ++i) {
      assert(graph->NodeExists(names[i]));

    const vector<string> inputNames = graph->InputNames(names[i]);

    for (unsigned int j = 0; j < inputNames.size(); ++j) {
      if (indexMap.insert(pair<string, unsigned int>(inputNames[j], numInputs)).second) {
        ++numInputs;
      }
    }
  }

  vector<unsigned int> inputSizesVec;
  for (map<string, unsigned int>::iterator it = indexMap.begin(); it != indexMap.end(); ++it) {
    if (marginalizedNode.compare(it->first) != 0) {
      const vector<string>  localNames = graph->InputNames(it->first);
      const Eigen::VectorXi localSizes = ModGraph::ConstructModPiece(graph, it->first, localNames)->inputSizes;
      for (unsigned int i = 0; i < localSizes.size(); ++i) {
        if (marginalizedNode.compare(localNames[i]) != 0) {
          inputSizesVec.push_back(localSizes(i));
        }
      }
    }
  }

  Translater<vector<unsigned int>, Eigen::VectorXi> trans(inputSizesVec);

  return *trans.GetPtr();
}

vector<string> ImportanceSampler::MarginalNames()
{
  vector<string> names(4);
  names[0] = "exp"; names[1] = "ones";
  names[2] = "qDens"; names[3] = "qRV";

  return names;
}

shared_ptr<ModGraph> ImportanceSampler::MarginalGraph(shared_ptr<Density> const& pi,
                                                      shared_ptr<Density> const& q,
                                                      shared_ptr<RandVar> const& rv,
                                                      unsigned int const         inputDimWrt)
{
      assert(q->inputSizes.size() == 1);
      assert(q->inputSizes(0) == pi->inputSizes(inputDimWrt));

  auto graph = make_shared<ModGraph>();

  graph->AddNode(pi, "logPi");
  graph->AddNode(q, "qDens");
  graph->AddNode(rv, "qRV");
  graph->AddNode(make_shared<ExpModel>(1), "exp");
  graph->AddNode(make_shared<VectorPassthroughModel>(pi->inputSizes(inputDimWrt)), "para");
  graph->AddNode(make_shared<ModParameter>(Eigen::VectorXd::Zero(1)), "ones");

  graph->AddEdge("para", "logPi", inputDimWrt);
  graph->AddEdge("logPi", "exp", 0);
  graph->AddEdge("para", "qDens", 0);

  return graph;
}

vector<int> ImportanceSampler::InputIndices(shared_ptr<ModGraph> const& graph,
                                            string const& name,
                                            string const& marginalizedNode,
                                            map<string, int>& indexMap,
                                            unsigned int& numInputs) const
{
      assert(graph->NodeExists(name));

  const vector<string> names = graph->InputNames(name);
  vector<int> indices(names.size());

  for (unsigned int j = 0; j < names.size(); ++j) {
    if (indexMap.insert(pair<string, int>(names[j], numInputs)).second) {
      ++numInputs;
    }

    indices[j] = indexMap[names[j]];
  }

  return indices;
}

Eigen::VectorXd ImportanceSampler::EvaluateImpl(vector<Eigen::VectorXd> const& inputs)
{
  // f inputs
  vector<Eigen::VectorXd> fIns(fPair.second.size());
  for (unsigned int i = 0; i < fPair.second.size(); ++i) {
    if (fPair.second[i] >= 0) {
      fIns[i] = inputs[fPair.second[i]];
    }
  }

  vector<Eigen::VectorXd> piIns(piPair.second.size());
  for (unsigned int i = 0; i < piPair.second.size(); ++i) {
    if (piPair.second[i] >= 0) {
      piIns[i] = inputs[piPair.second[i]];
    }
  }

  vector<Eigen::VectorXd> qDensIns(qDensPair.second.size());
  for (unsigned int i = 0; i < qDensPair.second.size(); ++i) {
    if (qDensPair.second[i] >= 0) {
      qDensIns[i] = inputs[qDensPair.second[i]];
    }
  }

  vector<Eigen::VectorXd> qRVIns(qRVPair.second.size());
  for (unsigned int i = 0; i < qRVPair.second.size(); ++i) {
    if (qRVPair.second[i] >= 0) {
      qRVIns[i] = inputs[qRVPair.second[i]];
    } else {
      cerr << endl << "Biasing sampler cannot depend on marginalized parameter." << endl;
      cerr << "\tImportanceSampler.cpp Line 145" << endl << endl;
      assert(false);
    }
  }

  Eigen::VectorXd weights = Eigen::VectorXd::Zero(N);
  Eigen::MatrixXd fruns   = Eigen::MatrixXd::Zero(outputSize, N);

  for (unsigned int i = 0; i < N; ++i) {
    // sample the rv
    const Eigen::VectorXd samp = qRVPair.first->Evaluate(qRVIns);

    if (fInd >= 0) {
      fIns[fInd] = samp;
    }

    if (piInd >= 0) {
      piIns[piInd] = samp;
    }

    if (qInd >= 0) {
      qDensIns[qInd] = samp;
    }

    weights(i)   = exp(piPair.first->LogDensity(piIns) - qDensPair.first->LogDensity(qDensIns)) / (double)N;
    fruns.col(i) = fPair.first->Evaluate(fIns);
  }

  if (normalize) {
    weights = weights / weights.sum();
  }

  return fruns * weights;
}

