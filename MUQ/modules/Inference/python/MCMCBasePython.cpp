
#include "MUQ/Inference/python/MCMCBasePython.h"

using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;

shared_ptr<MCMCBase> MCMCBasePython::ConstructMCMCFromDict(shared_ptr<AbstractSamplingProblem> density,
                                                           boost::python::dict const         & dict)
{
  boost::property_tree::ptree pt = PythonDictToPtree(dict);

  return MCMCBase::ConstructMCMC(density, pt);
}

shared_ptr<MCMCBase> MCMCBasePython::ConstructMCMCFromString(shared_ptr<AbstractSamplingProblem> density,
                                                             string                              xmlFile)
{
  return MCMCBase::ConstructMCMC(density, xmlFile);
}

void MCMCBasePython::WriteAttributes1(std::string const& groupName) const
{
  MCMCBase::WriteAttributes(groupName);
}

void muq::Inference::ExportMCMCBase()
{
  boost::python::class_<MCMCBasePython, std::shared_ptr<MCMCBasePython>, boost::noncopyable> exportMCMCBase(
    "MCMCBase",
    boost::
    python::no_init);

  // create constructors
  exportMCMCBase.def("__init__", boost::python::make_constructor(&MCMCBasePython::ConstructMCMCFromDict));
  exportMCMCBase.def("__init__", boost::python::make_constructor(&MCMCBasePython::ConstructMCMCFromString));

  // expose functions
  exportMCMCBase.def("Sample", &MCMCBase::PySample);
  exportMCMCBase.def("WriteAttributes",&MCMCBasePython::WriteAttributes1);

  // register parent pointer
  boost::python::register_ptr_to_python<std::shared_ptr<MCMCBase> >();

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<MCMCBasePython>, std::shared_ptr<MCMCBase> >();
}
