
#include <Eigen/Core>
#include "gtest/gtest.h"

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"
#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"
#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/GaussianDensity.h"

using namespace std;
using namespace Eigen;
using namespace muq::Modelling;
using namespace muq::Inference;
using namespace muq::Utilities;

class Mod23 : public ModPiece<> {
public:

  /** Constructor taking vector dimension and resizing the State.*/
  Mod23()
  {
    DimIn[0] = 3;
    DimOut   = 2;
    State.resize(DimIn[0]);
  }

  virtual void UpdateBase(const Eigen::VectorXd& VectorIn)
  {
    State(0) = cos(VectorIn(0)) + exp(VectorIn(1));
    State(1) = VectorIn(1) + 5.0 * VectorIn(2);
  }

  virtual void GradBase(const Eigen::VectorXd& VectorIn,
                        const Eigen::VectorXd& SensIn,
                        Eigen::VectorXd      & GradVec,
                        int                    dim)
  {
    GradVec(0) = sin(VectorIn(0)) * SensIn(0);
    GradVec(1) = exp(VectorIn(1)) * SensIn(0) + SensIn(1);
    GradVec(2) = 5.0 * SensIn(1);
  }
};

TEST(InferenceMetrics, Model2x3)
{
  // create the model
  Model TestMod = make_shared<Mod23>();
  Model param   = make_shared<ModParameter>(3);

  TestMod.SetInput(param);

  // test the model evaluation
  Eigen::VectorXd VecIn(3);
  VecIn << 1, 2, -.3;
  param.Update(VecIn);

  MatrixXd cov(2, 2);
  cov << 4, 2, 2, 6;
  auto density = make_shared<GaussianDensity>(VectorXd::Zero(2), cov);

  GaussianFisherInformationMetric metric(density, TestMod, param);


  Eigen::MatrixXd computedMetric = metric.ComputeMetric(VecIn);
  Eigen::MatrixXd correctMetric(3, 3);
  correctMetric << 0.21242202548207134, 1.7811557952296007, -0.42073549240394825, 1.7811557952296007,
  15.101633790157141, -2.6945280494653252, -0.42073549240394825, -2.6945280494653252,         5;
  EXPECT_PRED_FORMAT3(muq::Utilities::MatrixApproxEqual, correctMetric, computedMetric, 1e-14);
}

TEST(InferenceMetrics, EuclideanMetric)
{
  EuclideanMetric metric;

  EXPECT_PRED_FORMAT2(muq::Utilities::MatrixEqual,
                      MatrixXd::Identity(5, 5), metric.ComputeMetric(VectorXd::Zero(5)));
}
