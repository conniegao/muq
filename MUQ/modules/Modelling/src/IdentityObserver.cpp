
#include "MUQ/Modelling/IdentityObserver.h"

using namespace muq::Modelling;
using namespace std;

IdentityObserver::IdentityObserver(Eigen::VectorXi const& inputDims, int extractDimIn) : ModPiece(inputDims,
                                                                                                  inputDims(
                                                                                                    extractDimIn),                                                                        true, true, true, true,
                                                                                                  false), extractDim(
                                                                                           extractDimIn) {}

Eigen::VectorXd IdentityObserver::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  return inputs.at(extractDim);
}

Eigen::VectorXd IdentityObserver::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                               Eigen::VectorXd const             & sensitivity,
                                               int const                           inputDimWrt)
{
  if (inputDimWrt == extractDim) {
    return sensitivity;
  } else {
    return Eigen::VectorXd::Zero(inputSizes(inputDimWrt));
  }
}

Eigen::MatrixXd IdentityObserver::JacobianImpl(std::vector<Eigen::VectorXd> const& input,
                                               int const                           inputDimWrt)
{
  if (inputDimWrt == extractDim) {
    return Eigen::MatrixXd::Identity(outputSize, outputSize);
  } else {
    return Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));
  }
}

Eigen::VectorXd IdentityObserver::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & target,
                                                     int const                           inputDimWrt)
{
  if (inputDimWrt == extractDim) {
    return target;
  } else {
    return Eigen::VectorXd::Zero(outputSize);
  }
}

Eigen::MatrixXd IdentityObserver::HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                              Eigen::VectorXd const             & sensitvity,
                                              int const                           inputDimWrt)
{
  return Eigen::MatrixXd::Zero(inputSizes(inputDimWrt), inputSizes(inputDimWrt));
}
