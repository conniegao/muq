#include "MUQ/Modelling/GaussianDensity.h"

using namespace std;
using namespace muq::Modelling;

GaussianDensity::GaussianDensity(shared_ptr<GaussianSpecification> const& specification) :
  Density(InputSizes(specification), true, true, true,
          specification->HasInverseCovariance()), specification(specification), stateMod(MakeStateModel(specification))
{}

shared_ptr<ConcatenateModel> GaussianDensity::MakeStateModel(shared_ptr<GaussianSpecification> const& specification)
{
  return make_shared<ConcatenateModel>(specification->dim);
}

Eigen::VectorXi GaussianDensity::InputSizes(shared_ptr<GaussianSpecification> const& specification)
{
  Eigen::VectorXi inSizes(specification->inputSizes.size() + specification->dim.size());

  for (unsigned int i = 0; i < specification->dim.size(); ++i) {
    inSizes(i) = specification->dim(i);
  }

  inSizes.tail(specification->inputSizes.size()) = specification->inputSizes;

  return inSizes;
}

double GaussianDensity::LogDensityImpl(vector<Eigen::VectorXd> const& inputs)
{
    assert(specification);

  vector<Eigen::VectorXd> ins(inputs.begin() + specification->dim.size(), inputs.end());
  vector<Eigen::VectorXd> stateIns(inputs.begin(), inputs.begin() + specification->dim.size());

  Eigen::VectorXd mean  = specification->Mean(ins);
  Eigen::VectorXd delta = stateMod->Evaluate(stateIns) - mean;

  return -0.5 * (specification->LogDeterminant(ins) + inputSizes(0) * log(2.0*M_PI) + delta.dot(specification->ApplyInverseCovariance(delta, ins).col(0)));
}

Eigen::VectorXd GaussianDensity::GradientImpl(vector<Eigen::VectorXd> const& inputs,
                                              Eigen::VectorXd const        & sensitivity,
                                              int const                      inputDimWrt)
{
    assert(specification);

  vector<Eigen::VectorXd> ins(inputs.begin() + specification->dim.size(), inputs.end());
  vector<Eigen::VectorXd> stateIns(inputs.begin(), inputs.begin() + specification->dim.size());

  switch (inputDimWrt) {
  case 0: // derivative wrt the state
  {
    Eigen::VectorXd mean  = specification->Mean(ins);
    Eigen::VectorXd delta = stateMod->Evaluate(stateIns) - mean;

    return sensitivity[0] * (-1.0 * specification->ApplyInverseCovariance(delta, ins));
  }

  default: // derivative wrt hyperparameters
    assert(false);
    return Eigen::VectorXd();
  }
}

Eigen::MatrixXd GaussianDensity::JacobianImpl(vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
{
    assert(specification);

  vector<Eigen::VectorXd> ins(inputs.begin() + specification->dim.size(), inputs.end());
  vector<Eigen::VectorXd> stateIns(inputs.begin(), inputs.begin() + specification->dim.size());

  switch (inputDimWrt) {
  case 0: // derivative wrt the state
  {
    Eigen::VectorXd mean  = specification->Mean(ins);
    Eigen::VectorXd delta = stateMod->Evaluate(stateIns) - mean;

    return -1.0 * specification->ApplyInverseCovariance(delta, ins).transpose();
  }

  default: // derivative wrt hyperparameters
    assert(false);
    return Eigen::VectorXd();
  }
}

Eigen::VectorXd GaussianDensity::JacobianActionImpl(vector<Eigen::VectorXd> const& inputs,
                                                    Eigen::VectorXd const        & target,
                                                    int const                      inputDimWrt)
{
    assert(specification);

  vector<Eigen::VectorXd> ins(inputs.begin() + specification->dim.size(), inputs.end());
  vector<Eigen::VectorXd> stateIns(inputs.begin(), inputs.begin() + specification->dim.size());

  switch (inputDimWrt) {
  case 0: // derivative wrt the state
  {
    Eigen::VectorXd mean  = specification->Mean(ins);
    Eigen::VectorXd delta = stateMod->Evaluate(stateIns) - mean;

    return (-1.0 * specification->ApplyInverseCovariance(delta, ins).col(0)).dot(target) * Eigen::VectorXd::Ones(1);
  }

  default: // derivative wrt hyperparameters
    assert(false);
    return Eigen::VectorXd();
  }
}

Eigen::MatrixXd GaussianDensity::HessianImpl(vector<Eigen::VectorXd> const& inputs,
                                             Eigen::VectorXd const        & sensitivity,
                                             int const                      inputDimWrt)
{
    assert(specification);

  switch (inputDimWrt) {
  case 0: // derivative wrt the state
  {
    assert(sensitivity.size() == 1);
    return -sensitivity(0) * specification->GetPrecisionMatrix(inputs);
  }

  default: // derivative wrt hyperparameters
    assert(false);
    return Eigen::MatrixXd();
  }
}

Eigen::VectorXd GaussianDensity::Mean(vector<Eigen::VectorXd> const& ins) const
{
  return specification->Mean(ins);
}

