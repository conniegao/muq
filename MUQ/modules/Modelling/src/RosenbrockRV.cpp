#include "MUQ/Modelling/RosenbrockRV.h"

using namespace std;
using namespace muq::Modelling;

RosenbrockRV::RosenbrockRV(std::shared_ptr<RosenbrockSpecification> const& specification) :
  RandVar(Eigen::VectorXi(), 2, false, false, false, false), specification(specification) {}

Eigen::VectorXd RosenbrockRV::EvaluateImpl(vector<Eigen::VectorXd> const& input) 
{
  return Sample(input);
}

Eigen::MatrixXd RosenbrockRV::Sample(int numSamps)
{
  assert(inputSizes.size() == 0);
  return Sample(vector<Eigen::VectorXd>(), numSamps);
}

Eigen::MatrixXd RosenbrockRV::Sample(std::vector<Eigen::VectorXd> const& input, int numSamps) 
{
  assert(specification);
  assert(gaussian);

  // compute Rosenbrock parameters
  const double a = specification->a(input);
  const double b = specification->b(input);

  // sample from the standard Gaussian
  const Eigen::MatrixXd normalSamp = gaussian->Sample(numSamps);
  
  // transrom the sample
  Eigen::MatrixXd samp(2, normalSamp.cols());
  samp.row(0) = a * normalSamp.row(0);
  samp.row(1) = normalSamp.row(1) / a + 
    b * ((samp.row(0).array() * samp.row(0).array()).matrix() + pow(a, 2.0) * Eigen::VectorXd::Ones(normalSamp.cols()).transpose());

  return samp;
}
