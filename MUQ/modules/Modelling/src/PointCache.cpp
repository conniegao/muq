#include "MUQ/Modelling/PointCache.h"

using namespace std;
using namespace muq::Modelling;

PointCache::PointCache(Eigen::MatrixXd const& inputs, Eigen::MatrixXd const& outputs) : 
  CachedModPiece(inputs.rows() * Eigen::VectorXi::Ones(1), outputs.rows()) 
{
  // make sure the number of points match 
  assert(inputs.cols() == outputs.cols());

  // add the input points to the cache
  for( unsigned int i=0; i<inputs.cols(); ++i ) {
    auto cachedResult = FetchCachedResult(inputs.col(i));
    cachedResult->SetCachedEvaluate(outputs.col(i));
  }
}

Eigen::VectorXd PointCache::EvaluateImpl(vector<Eigen::VectorXd> const& input)  
{
  // can only return cached entries
  assert(IsEntryInCache(input));

  auto cachedResult = FetchCachedResult(input);
  
  return *(cachedResult->GetCachedEvaluate());
}

Eigen::VectorXd PointCache::GradientImpl(vector<Eigen::VectorXd> const& input,
					 Eigen::VectorXd const             & sensitivity,
					 int const                           inputDimWrt) 
{
  cerr << endl << "NO GRADIENT FOR PointCache" << endl << endl;
  assert(false);
  
  return Eigen::VectorXd();
}

Eigen::MatrixXd PointCache::JacobianImpl(vector<Eigen::VectorXd> const& input, int const inputDimWrt) 
{
  cerr << endl << "NO JACOBIAN FOR PointCache" << endl << endl;
  assert(false);
  
  return Eigen::MatrixXd();
}


Eigen::VectorXd PointCache::JacobianActionImpl(vector<Eigen::VectorXd> const& input,
					       Eigen::VectorXd const             & target,
					       int const                           inputDimWrt) 
{
  cerr << endl << "NO JACOBIAN ACTION FOR  PointCache" << endl << endl;
  assert(false);
  
  return Eigen::VectorXd();
}


Eigen::MatrixXd PointCache::HessianImpl(vector<Eigen::VectorXd> const& input,
					Eigen::VectorXd const             & sensitivity,
					int const                           inputDimWrt) 
{
  cerr << endl << "NO HESSIAN FOR PointCache" << endl << endl;
  assert(false);
  
  return Eigen::MatrixXd();
}

