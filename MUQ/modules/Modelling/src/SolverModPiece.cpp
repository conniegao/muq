#include "MUQ/Modelling/SolverModPiece.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/timer.hpp>

#include <kinsol/kinsol.h>           /* main nonlinear solver header file */
#include <kinsol/kinsol_spgmr.h>     /* prototypes & constants for CVSPGMR solver */
#include <kinsol/kinsol_spbcgs.h>    /* prototypes & constants for CVSPBCG solver */
#include <kinsol/kinsol_sptfqmr.h>   /* prototypes & constants for SPTFQMR solver */
#include <kinsol/kinsol_dense.h>


#include <sundials/sundials_spgmr.h>     /* prototypes & constants for CVSPGMR solver */
#include <sundials/sundials_spbcgs.h>    /* prototypes & constants for CVSPBCG solver */
#include <sundials/sundials_sptfqmr.h>   /* prototypes & constants for SPTFQMR solver */

#include <nvector/nvector_serial.h>  /* serial N_Vector types, fct. and macros */
#include <sundials/sundials_dense.h> /* use generic DENSE solver in preconditioning */
#include <sundials/sundials_types.h> /* definition of realtype */
#include <sundials/sundials_math.h>  /* contains the macros ABS, SQR, and EXP */

#include <Eigen/Dense>

#include "MUQ/Modelling/IdentityObserver.h"


using namespace muq::Modelling;
using namespace std;


std::ostream& muq::Modelling::operator<<(std::ostream& os, const SolverStatistics& stats)
{
	std::cout << "Nonlinear solve statistics:\n";
	std::cout << "  Linear solver type =    " << stats.linSolver << std::endl;
	std::cout << "  Nonlinear iterations =  " << stats.nonlinearIterations << std::endl;
	std::cout << "  Solution time =         " << stats.solveTime << std::endl;
	std::cout << "  Residual evalautions =  " << stats.numResEvals << std::endl;
	std::cout << "  Jacobian evaluations =  " << stats.numJacEvals << std::endl;
	std::cout << "  Jacobian applications = " << stats.numJacActs << std::endl;
	
  return os;
}



struct RhsData {
  std::shared_ptr<ModPiece>    f, g;
  std::vector<Eigen::VectorXd> inputs;
  int                          stateSize;
  int                          inputDimWrt;
  int                          paramSize;
};

/* Functions Called by the KINSOL Solver */
static int sundialsFunc(N_Vector state, N_Vector deriv, void *user_data)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvDerivMap(NV_DATA_S(deriv), fgdata->stateSize);

  
  fgdata->inputs.at(0) = nvStateMap;
  nvDerivMap = fgdata->f->Evaluate(fgdata->inputs);
  return 0;
}

static int sundialsJac(long int N,
                       N_Vector state,
                       N_Vector rhs,
                       DlsMat   jac,
                       void    *user_data,
                       N_Vector tmp1,
                       N_Vector tmp2)
{
  RhsData *fgdata = (RhsData *)user_data;

  assert(N == fgdata->stateSize);      
  assert(jac->M == fgdata->stateSize);

  Eigen::Map<Eigen::VectorXd> stateMap(NV_DATA_S(state), fgdata->stateSize);

  Eigen::Map < Eigen::MatrixXd, 0, Eigen::OuterStride < Eigen::Dynamic >> jacMap(jac->data,
                                                                                 jac->M,
                                                                                 jac->N,
                                                                                 Eigen::OuterStride<Eigen::Dynamic>(jac->ldim));

  fgdata->inputs.at(0) = stateMap;
  jacMap = fgdata->f->Jacobian(fgdata->inputs, 0);
  return 0;
}

static int sundialsJacApply(N_Vector v,
                            N_Vector Jv,
                            N_Vector y,
                            int* fy,
                            void    *user_data)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> vMap(NV_DATA_S(v), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> JvMap(NV_DATA_S(Jv), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> yMap(NV_DATA_S(y), fgdata->stateSize);

  fgdata->inputs.at(0) = yMap;

  fgdata->f->Evaluate(fgdata->inputs);
  JvMap = fgdata->f->JacobianAction(fgdata->inputs, vMap, 0);

  return 0;
}


static void check_sundials_flag(void *flagvalue, string funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if ((opt == 0) && (flagvalue == NULL)) {
    std::cerr << "\nSUNDIALS_ERROR: " << funcname << " failed - returned NULL pointer\n\n";
    //assert(false);
  }
  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *)flagvalue;
    if (*errflag < 0) {
      std::cerr << "\nSUNDIALS_ERROR: " << funcname << " failed with flag = " << *errflag << "\n\n";
      //assert(false);
    }
  }
  /* Check if function returned NULL pointer - no memory allocated */
  else if ((opt == 2) && (flagvalue == NULL)) {
    std::cerr << "\nMEMORY_ERROR: " << funcname << " failed - returned NULL pointer\n\n";
    //assert(false);
  }
}

SolverModPiece::SolverModPiece(std::shared_ptr<ModPiece>    fIn,
                         	   std::shared_ptr<ModPiece>    gIn,
							   boost::property_tree::ptree& properties) : ModPiece(fIn->inputSizes,
                                                                             gIn->outputSize,
																			 (gIn->hasDirectGradient)&&(fIn->hasDirectGradient)&&(fIn->hasDirectJacobian||fIn->hasDirectJacobianAction),
																			 (gIn->hasDirectJacobian)&&(fIn->hasDirectJacobian),
																			 (gIn->hasDirectJacobian)&&(fIn->hasDirectJacobian), 
																			 false,
																			 (gIn->isRandom)),
																    f(fIn), g(gIn),
                                                                    linSolver(properties.get("KINSOL.LinearSolver", "Dense")),
																    solveMethod(properties.get("KINSOL.SolveMethod", "Newton")),
												      verboseLevel(properties.get("KINSOL.Verbose", 0)),
												      maxNewtonStep(properties.get<double>("KINSOL.MaxNewtonStep",0.0)),
													  maxNewtonIters(properties.get<int>("KINSOL.MaxNewtonIters",200))
{
	assert(!fIn->isRandom);
	
  // check sizes of f and g
      assert(fIn->outputSize == fIn->inputSizes(0));
      assert(gIn->inputSizes.size() == fIn->inputSizes.size());
  for (int inDim = 0; inDim < inputSizes.size(); ++inDim) {
      assert(gIn->inputSizes(inDim) == fIn->inputSizes(inDim));
  }

}

SolverModPiece::SolverModPiece(std::shared_ptr<ModPiece>    fIn,
                               boost::property_tree::ptree& properties) : SolverModPiece(fIn,make_shared<IdentityObserver>(fIn->inputSizes,0),properties){};



Eigen::VectorXd SolverModPiece::Solve(std::vector<Eigen::VectorXd> const& inputs){
	boost::timer Time;
	Time.restart();
	
	
    N_Vector state, scale;
    RhsData *fgdata;
    void    *kin_mem;
    int iout, flag;

    state     = NULL;
    fgdata    = NULL;
    kin_mem = NULL;
	
	
    int stateSize = inputSizes(0);

    /* Allocate memory, and set problem data, initial values, tolerances */
    state = N_VNew_Serial(stateSize);
	
    check_sundials_flag((void *)state,  "N_VNew_Serial", 0);

    fgdata = new RhsData;
    check_sundials_flag((void *)fgdata, "AllocUserData", 2);

    // set the rhs and observation modpieces
    fgdata->stateSize = stateSize;
    fgdata->f         = f;
    fgdata->g         = g;
    fgdata->inputs.insert(fgdata->inputs.end(), inputs.begin(), inputs.end());
	
    // copy the initial state
    Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), stateSize);
    nvStateMap = inputs.at(0);
	
	// create the kinsol solver
	kin_mem = KINCreate();

	flag = KINSetMaxNewtonStep(kin_mem,maxNewtonStep);
	check_sundials_flag(&flag, "KINSetMaxNewtonStep", 1);
	
	flag = KINSetNumMaxIters(kin_mem,maxNewtonIters);
	check_sundials_flag(&flag, "KINSetMaxNewtonStep", 1);

	flag = KINSetPrintLevel(kin_mem,verboseLevel);
	check_sundials_flag(&flag, "KINSetPrintLevel", 1);
	
	flag = KINSetUserData(kin_mem,fgdata);
	check_sundials_flag(&flag, "KINSetUserData", 1);
	
	flag = KINInit(kin_mem,sundialsFunc,state);
	check_sundials_flag(&flag, "KINInit", 1);
	
	
    if (!linSolver.compare("Dense")) {
      flag = KINDense(kin_mem, stateSize);
      check_sundials_flag(&flag, "KINDense", 1);

      flag = KINDlsSetDenseJacFn(kin_mem, sundialsJac);
      check_sundials_flag(&flag, "KINDlsSetDenseJacFn", 1);
	  
    } else {
      if (!linSolver.compare("SPGMR")) {
        flag = KINSpgmr(kin_mem, 0);
        check_sundials_flag(&flag, "KINSpgmr",   1);
      } else if (!linSolver.compare("SPBCG")) {
        flag = KINSpbcg(kin_mem, 0);
        check_sundials_flag(&flag, "KINSpbcg",   1);
      } else if (!linSolver.compare("SPTFQMR")) {
        flag = KINSptfqmr(kin_mem, 0);
        check_sundials_flag(&flag, "KINSptfqmr", 1);
      } else {
        std::cerr << "\nInvalid KINSOL linear solver type.  Options are Dense, SPGMR, SPBCG, or SPTFQMR\n\n";
        assert(false);
      }
      flag = KINSpilsSetJacTimesVecFn(kin_mem, sundialsJacApply);
      check_sundials_flag(&flag, "KINSpilsSetJacTimesVecFn", 1);
  }
  
  // call the solver
  scale = N_VNew_Serial(stateSize);
  N_VConst_Serial(1.0,scale);
  
  flag = KINSol(kin_mem, state, KIN_LINESEARCH, scale,scale);
  check_sundials_flag(&flag, "KINSol", 1);
  
  Eigen::VectorXd output = nvStateMap;
  
  // store some statistics about the solve
  KINGetNumNonlinSolvIters(kin_mem,&stats.nonlinearIterations);
  stats.solveTime = Time.elapsed();
  stats.linSolver = linSolver;
  if(!linSolver.compare("Dense")){
	  KINDlsGetNumFuncEvals(kin_mem,&stats.numResEvals);	
	  KINDlsGetNumJacEvals(kin_mem,&stats.numJacEvals);	  
  }else{
	  KINSpilsGetNumFuncEvals(kin_mem,&stats.numResEvals);
	  KINSpilsGetNumJtimesEvals(kin_mem,&stats.numJacActs);	
  }
  
  
  
  N_VDestroy_Serial(state);
  N_VDestroy_Serial(scale);

  delete fgdata;
  KINFree(&kin_mem);
  
  return output;
}
	
Eigen::VectorXd SolverModPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{

	Eigen::VectorXd x = Solve(inputs);
	std::vector<Eigen::VectorXd> gInputs(inputs);
	gInputs.at(0) = x;
  	return g->Evaluate(gInputs);
}




struct LinSolverData{
	std::shared_ptr<ModPiece> f;
	std::vector<Eigen::VectorXd> *inputPtr;
	int dim;
};

static int linSolverJacApply(void *user_data, N_Vector v, N_Vector z)
{
  LinSolverData *data = (LinSolverData*)user_data;

  Eigen::Map<Eigen::VectorXd> vMap(NV_DATA_S(v), data->f->inputSizes(data->dim));
  Eigen::Map<Eigen::VectorXd> zMap(NV_DATA_S(z), data->f->inputSizes(data->dim));


  zMap = data->f->JacobianAction(*data->inputPtr,vMap,data->dim);

  return 0;
}


Eigen::VectorXd SolverModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       		 Eigen::VectorXd const             & sensitivity,
                                             int const                           inputDimWrt){
	
	if(inputDimWrt==0){
		return Eigen::VectorXd::Zero(inputSizes(0));	
	}else{
		std::vector<Eigen::VectorXd> solInputs(input);
		solInputs.at(0) = Solve(input);
		Eigen::VectorXd lambda;
			
	    if (!linSolver.compare("Dense")) {
	      lambda = f->Jacobian(solInputs,0).lu().solve(g->Gradient(solInputs,sensitivity,0));
	    } else {
			
		  
		  int stateSize = inputSizes(0);
		  
		  LinSolverData *data;
		  data = new LinSolverData;
		  data->f = f;
		  data->dim = 0;
		  data->inputPtr = &solInputs; 
		  
		  int flag;
		  N_Vector rhs, sol;
		  rhs = N_VNew_Serial(stateSize);
		  sol = N_VNew_Serial(stateSize);

		  Eigen::Map<Eigen::VectorXd> rhsMap(NV_DATA_S(rhs), stateSize);
		  rhsMap = g->Gradient(solInputs,sensitivity,0);

		  Eigen::Map<Eigen::VectorXd> solMap(NV_DATA_S(sol), stateSize);
		  //solMap = input.at(inputDimWrt);
		  solMap = Eigen::VectorXd::Zero(stateSize);

		  const double residTol = 1e-9;
	  	  double resNorm;
		  
	      if (!linSolver.compare("SPGMR")) {

			  SpgmrMem lin_mem;
			  lin_mem = SpgmrMalloc(stateSize, rhs);
			  check_sundials_flag(lin_mem, "SpgmrMalloc", 0);

			  int linIts;
			  int pCalls;
			  
			  flag = SpgmrSolve(lin_mem, (void*) data, sol, rhs,
			  			       PREC_NONE, MODIFIED_GS, residTol, 
			  			       20, NULL, NULL, 
			  			       NULL, linSolverJacApply, NULL, 
			  			       &resNorm, &linIts, &pCalls);

			  check_sundials_flag(&flag, "SpgmrSolve", 1);

							   
	          SpgmrFree(lin_mem);
			
			
	      } else if (!linSolver.compare("SPBCG")) {
			  
			  SpbcgMem lin_mem;
			  lin_mem = SpbcgMalloc(stateSize, rhs);
			  check_sundials_flag(lin_mem, "SpgmrMalloc", 0);
			  
			  int linIts;
			  int pCalls;
			  
			  flag = SpbcgSolve(lin_mem, (void*) data, sol, rhs,
			  			       PREC_NONE, residTol, 
			  			       NULL, NULL, 
			  			       NULL, linSolverJacApply, NULL, 
			  			       &resNorm, &linIts, &pCalls);
			  check_sundials_flag(&flag, "SpgmrSolve", 1);
			  				   
							   
	          SpbcgFree(lin_mem);
			  
			  
	      } else if (!linSolver.compare("SPTFQMR")) {
			  
			  SptfqmrMem lin_mem;
			  lin_mem = SptfqmrMalloc(stateSize, rhs);
			  check_sundials_flag(lin_mem, "SpgmrMalloc", 0);
			  
			  int linIts;
			  int pCalls;
			  
			  flag = SptfqmrSolve(lin_mem, (void*) data, sol, rhs,
			  			       PREC_NONE, residTol, 
			  			       NULL, NULL, 
			  			       NULL, linSolverJacApply, NULL, 
			  			       &resNorm, &linIts, &pCalls);
			  check_sundials_flag(&flag, "SpgmrSolve", 1);			   
			
	          SptfqmrFree(lin_mem);
			  
			  
	      } else {
	        std::cerr << "\nInvalid KINSOL linear solver type.  Options are Dense, SPGMR, SPBCG, or SPTFQMR\n\n";
			delete data;
 		    N_VDestroy_Serial(sol);
 		    N_VDestroy_Serial(rhs);
	        assert(false);
	      }
		    	
		  lambda = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(sol),stateSize);
		   N_VDestroy_Serial(sol);
		   N_VDestroy_Serial(rhs);
		   
		  delete data;
		  
		  if(resNorm>residTol){
			  std::cerr << "Linear solve failed in SolverModPiece::GradientImpl.\n";
			  assert(resNorm<=residTol);
		  }
	    }
		return g->Gradient(solInputs,sensitivity,inputDimWrt) - f->Gradient(solInputs,lambda,inputDimWrt);
	}
}


Eigen::MatrixXd SolverModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt){
	
	if(inputDimWrt==0){
		return Eigen::MatrixXd::Zero(outputSize,inputSizes(0));
	}else{
		std::vector<Eigen::VectorXd> solInputs(input);
		solInputs.at(0) = Solve(input);
		
		Eigen::MatrixXd lambda = f->Jacobian(solInputs,0).lu().solve(g->Jacobian(solInputs,0).transpose());
		return g->Jacobian(solInputs,inputDimWrt) - lambda.transpose()*f->Jacobian(solInputs,inputDimWrt);
	}
	
}

Eigen::VectorXd SolverModPiece::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                   Eigen::VectorXd const             & target,
                                                   int const                           inputDimWrt){
                                   	
	if(inputDimWrt==0){
		return Eigen::VectorXd::Zero(outputSize);
	}else{
		std::vector<Eigen::VectorXd> solInputs(input);
		solInputs.at(0) = Solve(input);
		
		Eigen::MatrixXd lambda = f->Jacobian(solInputs,0).lu().solve(g->Jacobian(solInputs,0).transpose());
		return g->JacobianAction(solInputs,target,inputDimWrt) - lambda.transpose()*f->JacobianAction(solInputs,target,inputDimWrt);
	}
}
