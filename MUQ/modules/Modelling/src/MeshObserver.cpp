
#include "MUQ/Modelling/MeshObserver.h"

// mesh includes
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Utilities/mesh/Mesh.h"
#include "MUQ/Utilities/mesh/UnstructuredSimplicialMesh.h"


using namespace muq::Modelling;
using namespace muq::utilities;


/** Construct the observer using a perscribed polynomial order and pescribed mesh.  */
template<unsigned int dim>
MeshObserver<dim>::MeshObserver(unsigned int porder,
                                const std::shared_ptr < Mesh < dim >>& MeshPtr,
                                const Eigen::Matrix<double, Eigen::Dynamic, dim>& ObsLocs)
{
  // grab how many observations we have
  unsigned int Nobs = ObsLocs.rows();

  // elementwise-constant observation, find the element containing each observation location and set the weights to one.
  if (porder == 0) {
    // resize the weights and nodes
    Nodes.resize(Nobs, std::vector<unsigned int>(1, 0));
    Weights.resize(Nobs);

    // loop over the observations and find the elements we want to extract
    for (unsigned int i = 0; i < Nobs; ++i) {
      Nodes[i][0] = MeshPtr->GetEle(ObsLocs.row(i).transpose());
      Weights[i]  = Eigen::VectorXd::Ones(1);
    }

    // piecewise-linear observation, get the nodes surrouding the element containing each observation and use linear
    // interpolation to set the weights
  } else if (porder == 1) {
    // resize the weights and nodes
    Nodes.resize(Nobs, std::vector<unsigned int>(1, 0));
    Weights.resize(Nobs);

    // loop over the observations and find the nodes we need
    for (unsigned int i = 0; i < Nobs; ++i) {
      unsigned int ele = MeshPtr->GetEle(ObsLocs.row(i).transpose());

      Eigen::Matrix<unsigned int, Eigen::Dynamic, Eigen::Dynamic> elenodes = MeshPtr->GetNodes(ele);

      // resize and copy to this classes private vector
      Nodes[i].resize(elenodes.size());
      std::copy(elenodes.data(), elenodes.data() + elenodes.size(), &Nodes[i][0]);


      // now figure out the weigths based on a linear interpolation
      Eigen::VectorXd CurrWeight = Eigen::VectorXd::Zero(elenodes.size());

      // simple linear interpolation is the dimension is 1
      if (dim == 1) {
        double x0   = (MeshPtr->GetNodePos(elenodes(0)))(0);
        double x1   = (MeshPtr->GetNodePos(elenodes(1)))(0);
        double temp = (ObsLocs(0, i) - x0) / (x1 - x0);
        CurrWeight[0] = 1.0 - temp;
        CurrWeight[1] = temp;
      } else if (dim == 2) {
        // first, define coefficients of basis function
        Eigen::Matrix<double, 6, 1>   coeff;
        Eigen::Matrix<double, 6, 6>   A;
        Eigen::Matrix<double, dim, 1> P1 = MeshPtr->GetNodePos(elenodes(0));
        Eigen::Matrix<double, dim, 1> P2 = MeshPtr->GetNodePos(elenodes(1));
        Eigen::Matrix<double, dim, 1> P3 = MeshPtr->GetNodePos(elenodes(2));

        A <<    1, P1[0], P1[1], 0, 0, 0, 0, 0, 0, 1, P1[0], P1[1], 1, P2[0], P2[1], 0, 0, 0, 0, 0, 0, 1, P2[0], P2[1],
        1, P3[0], P3[1], 0, 0, 0, 0, 0, 0, 1, P3[0], P3[1];

        Eigen::Matrix<double, 6, 1> rhs;
        rhs << 0, 0, 1, 0, 0, 1;

        coeff = A.lu().solve(rhs);

        // map the point to the reference triangle
        double x = coeff[0] + coeff[1] * ObsLocs(i, 0) + coeff[2] * ObsLocs(i, 1);
        double y = coeff[3] + coeff[4] * ObsLocs(i, 0) + coeff[5] * ObsLocs(i, 1);

        CurrWeight[0] = 1 - x - y;
        CurrWeight[1] = x;
        CurrWeight[2] = y;
      } else if (dim == 3) {
        // first, define coefficients of basis function
        Eigen::Matrix<double, 12, 1>  coeff;
        Eigen::Matrix<double, 12, 12> A;
        Eigen::Matrix<double, dim, 1> P1 = MeshPtr->GetNodePos(elenodes(0));
        Eigen::Matrix<double, dim, 1> P2 = MeshPtr->GetNodePos(elenodes(1));
        Eigen::Matrix<double, dim, 1> P3 = MeshPtr->GetNodePos(elenodes(2));
        Eigen::Matrix<double, dim, 1> P4 = MeshPtr->GetNodePos(elenodes(3));

        A <<  1, P1[0], P1[1], P1[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, P1[0], P1[1], P1[2], 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 1, P1[0], P1[1], P1[2], 1, P2[0], P2[1], P2[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, P2[0],
        P2[1], P2[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, P2[0], P2[1], P2[2], 1, P3[0], P3[1], P3[2], 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 1, P3[0], P3[1], P3[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, P3[0], P3[1], P3[2], 1,
        P4[0], P4[1], P4[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, P4[0], P4[1], P4[2], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, P4[0], P4[1], P4[2];


        Eigen::Matrix<double, 12, 1> rhs;
        rhs << 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1;
        coeff = A.lu().solve(rhs);

        // map the point to the reference triangle
        double x = coeff[0] + coeff[1] * ObsLocs(0, i) + coeff[2] * ObsLocs(1, i) + coeff[3] * ObsLocs(2, i);
        double y = coeff[4] + coeff[5] * ObsLocs(0, i) + coeff[6] * ObsLocs(1, i) + coeff[7] * ObsLocs(2, i);
        double z = coeff[8] + coeff[9] * ObsLocs(0, i) + coeff[10] * ObsLocs(1, i) + coeff[11] * ObsLocs(2, i);

        CurrWeight[0] = 1 - x - y - z;
        CurrWeight[1] = x;
        CurrWeight[2] = y;
        CurrWeight[3] = z;
      }

      // save the weights for this observation
      Weights[i] = CurrWeight;
    }
  } else {
    std::cerr <<
    "ERROR: In MeshObserver, currently, only porder=0 and porder=1 are supported.\n\t If you have a need for a more general observer, such as an unstructured simplicial mesh with element geometries described by shape functions, contact mparno@mit.edu to express your interest.";
    throw("Polynomial order error.");
  }

  State.resize(Nobs);
  DimOut = Nobs;
  if (porder == 0) {
    DimIn[0] = MeshPtr->NumEles();
  } else if (porder == 1) {
    DimIn[0] = MeshPtr->NumNodes();
  }
}

/** Update the state by extracting and possibly weighting the entries of VectorIn */
template<unsigned int dim>
void MeshObserver<dim>::UpdateBase(const Eigen::VectorXd& VectorIn)
{
  assert(VectorIn.size() == DimIn[0]);

  // loop over the observation locations
  for (unsigned int i = 0; i < Nodes.size(); ++i) {
    State[i] = 0;
    Eigen::VectorXd CurrWeight = Weights[i];
    for (unsigned j = 0; j < CurrWeight.size(); ++j) {
      State[i] += CurrWeight(j) * VectorIn(Nodes[i][j]);
    }
  }
}

/** Evaluate the gradient of the "observed" values to the entries in the input matrix. */
template<unsigned int dim>
void MeshObserver<dim>::GradBase(const Eigen::VectorXd& VectorIn,
                                 const Eigen::VectorXd& SensIn,
                                 Eigen::VectorXd      & GradVec,
                                 int                    dimIn)
{
  assert(VectorIn.size() == DimIn[0]);
  assert(GradVec.size() == DimIn[0]);
  assert(SensIn.size() == DimOut);

  // loop over the observation locations
  for (unsigned int i = 0; i < Nodes.size(); ++i) {
    for (unsigned j = 0; j < Nodes[i].size(); ++j) {
      GradVec[Nodes[i][j]] += SensIn[i] * (Weights[i])(j);
    }
  }
}

// make instances of this class for each dimension
template class MeshObserver<1>;
template class MeshObserver<2>;
template class MeshObserver<3>;
