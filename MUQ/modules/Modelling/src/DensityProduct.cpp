
#include "MUQ/Modelling/DensityProduct.h"
#include <iostream>
#include <numeric>

using namespace muq::Modelling;
using namespace std;

/** Create a density product from smart pointers to DensityBase instances */
DensityProduct::DensityProduct(int const numInputs) : Density(Eigen::VectorXi::Constant(numInputs,
                                                                                        1), true, true, true, false)
{
  assert(numInputs > 1); //should have at least one input
}

double DensityProduct::LogDensityImpl(std::vector<Eigen::VectorXd> const& input)
{
  Eigen::VectorXd zero = Eigen::VectorXd::Zero(1);

  return accumulate(input.begin(), input.end(), zero) (0);
}

Eigen::VectorXd DensityProduct::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & sensitivity,
                                             int const                           inputDimWrt)
{
  return sensitivity;
}

///The jacobian is outputDim x inputDim
Eigen::MatrixXd DensityProduct::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  return Eigen::MatrixXd::Constant(1, 1, 1.0);
}

Eigen::VectorXd DensityProduct::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                   Eigen::VectorXd const             & target,
                                                   int const                           inputDimWrt)
{
  return target;
}

