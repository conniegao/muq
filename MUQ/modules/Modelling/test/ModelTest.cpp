
#include <iostream>
#include <math.h>
#include <vector>

#include <boost/algorithm/string/predicate.hpp>

// google testing library
#include "gtest/gtest.h"

// muq related includes
#include "MUQ/Utilities/EigenTestUtils.h"

// #include "MUQ/Modelling/Model.h"
#include "MUQ/Modelling/ModPieceTemplates.h"

// #include "MUQ/Modelling/ModParameter.h"
// #include "MUQ/Modelling/AnalyticFunctions/SymbolicHeaders.h"

using namespace muq::Modelling;
using namespace std;
using namespace Eigen;
using namespace muq::Utilities;

class MultiTestMod : public OneInputNoDerivModPiece
{
  
public:
  MultiTestMod(int dim) : OneInputNoDerivModPiece(dim,dim){};
  virtual ~MultiTestMod() = default;
  
private:
  virtual Eigen::MatrixXd EvaluateMultiImpl(Eigen::MatrixXd const& input) override
  {
    return sin(input.array());
  };

  // NOTICE: In practice EvaluateImpl and Evaluate should return the same thing.  They are different here only for testing purposes.
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    return cos(input.array());
  };
  
};

// define a simple single input forward model
class SinMod : public OneInputAdjointModPiece {
public:

  /** Constructor taking vector dimension and resizing the State.*/
  SinMod(int dim) : OneInputAdjointModPiece(dim, dim) {}

  virtual ~SinMod() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override
  {
    return sin(input.array());
  }

  virtual Eigen::VectorXd GradientImpl(Eigen::VectorXd const& input, Eigen::VectorXd const& sensitivity) override
  {
    return sensitivity.array() * cos(input.array());
  }
};

// define a simple two input forward model
class SumMod : public ModPiece {
public:

  /** Constructor taking vector dimension and resizing the State.*/
  SumMod(int dim) : ModPiece(CreateInputSize(dim), dim, true, false, false, false, false) {}

  virtual ~SumMod() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    return input.at(0) + input.at(1);
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    return sensitivity;
  }

  static VectorXi CreateInputSize(int dim)
  {
    VectorXi size(2);

    size << dim, dim;
    return size;
  }
};

// define a simple single input model with different input/output dimensions
class FooMod : public ModPiece {
public:

  /** Constructor taking vector dimensions */
  FooMod(int dimIn, int dimOut) : ModPiece(dimIn * Eigen::VectorXi::Ones(1), dimOut, true, false, false, true, false, "RandomModPieceName") {}

  virtual ~FooMod() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    Eigen::ArrayXd shifted(inputSizes[0]);

    for (int i = 0; i < inputSizes[0]; ++i) {
      shifted(i) = input[0]((i + 1) % inputSizes[0]);
    }

    Eigen::VectorXd output(outputSize);
    for (int i = 0; i < outputSize; ++i) {
      output[i] = (input[0].array() * shifted).sum() + (input[0].array() * input[0].array() * input[0].array()).sum();
    }
    return output;
  }

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override
  {
    Eigen::ArrayXd shifted(inputSizes[0]);

    for (int i = 0; i < inputSizes[0]; ++i) {
      shifted(i) = input[0]((i + 1) % inputSizes[0]);
    }

    Eigen::VectorXd gradient = Eigen::VectorXd::Zero(inputSizes[0]);
    for (int i = 0; i < inputSizes[0]; ++i) {
      int j = ((i - 1) < 0) ? inputSizes[0] - 1 : i - 1;
      gradient[i] = sensitivity.dot((input[0](j) + shifted(i) + 3.0 * input[0](i) * input[0](
                                       i)) * Eigen::VectorXd::Ones(outputSize));
    }
    return gradient;
  }

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
                                      Eigen::VectorXd const             & sensitivity,
                                      int const                           inputDimWrt) override
  {
    Eigen::MatrixXd Hessian = Eigen::MatrixXd::Zero(inputSizes[0], inputSizes[0]);

    for (int out = 0; out < outputSize; ++out) {
      for (int i = 0; i < inputSizes[0]; ++i) {
        Hessian(i, i) += 6.0 * input[0](i) * sensitivity(out);

        int j1 = ((i - 1) < 0) ? inputSizes[0] - 1 : i - 1;
        Hessian(i, j1) += 1.0 * sensitivity(out);

        int j2 = ((i + 1) == inputSizes[0]) ? 0 : i + 1;
        Hessian(i, j2) += 1.0 * sensitivity(out);
      }
    }

    return Hessian;
  }
};

// // sum up the input vector
// class ReduceMod : public ModPiece<>{
// public:
//     ReduceMod(int dim){
//         DimIn[0] = dim;
//         DimOut = 1;
//         State.resize(DimOut);
//     };
//
//
//     virtual void UpdateBase(const Eigen::VectorXd &VectorIn0){
//         State[0] = VectorIn0.sum();
//     };
//
//     virtual void GradBase(const Eigen::VectorXd &VectorIn, const Eigen::VectorXd &SensIn, Eigen::VectorXd &GradVec,
// int dim){
//         GradVec = SensIn[0]*Eigen::VectorXd::Ones(DimIn[dim]);
//     };
//
//     // this is just a random function that we may want to call -- it will just add 1 to the state
//     virtual void AddOne(){
//         State += Eigen::VectorXd::Ones(DimOut);
//     };
//
// };

TEST(ModellingModelTest, NamingTest)
{
  // set the vector size
  int dim = 10;

  // create the models
  auto TestSinMod = make_shared<SinMod>(dim);
  auto TestFooMod = make_shared<FooMod>(dim, dim);

  EXPECT_TRUE(boost::algorithm::starts_with(TestSinMod->GetName(), "SinMod"));
  EXPECT_TRUE(boost::algorithm::starts_with(TestFooMod->GetName(), "RandomModPieceName"));
  
  TestSinMod->SetName("RenamedSinMod");
  
  EXPECT_TRUE(TestSinMod->GetName().compare("RenamedSinMod") == 0);
}

TEST(ModellingModelTest, UniqueNames)
{
  int dim = 10;

  // create the forward model object
  auto mod1 = make_shared<SinMod>(dim);
  auto mod2 = make_shared<SinMod>(dim);

  //We just want to make sure that even though we didn't provide names, and their types are identical,
  //the default names they are given are different.
  EXPECT_NE(0, mod1->GetName().compare(mod2->GetName()));
}

TEST(ModellingModelTest, FiniteDifference)
{
  // set the vector size
  int dim = 10;

  // test the model evaluation
  Eigen::VectorXd VecIn  = Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd SensIn = Eigen::VectorXd::Ones(dim);

  // create the model
  auto TestSinMod = make_shared<SinMod>(dim);

  Eigen::VectorXd GradVec  = TestSinMod->GradientByFD(std::vector<Eigen::VectorXd>(1, VecIn), SensIn, 0);
  Eigen::VectorXd TrueGrad = VecIn.array().cos();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, GradVec, TrueGrad, 1e-3);

  int numPrevEvals = TestSinMod->GetNumCalls("Evaluate");
  
  Eigen::MatrixXd Jac  = TestSinMod->JacobianByFD(std::vector<Eigen::VectorXd>(1, VecIn), 0);
  
  Eigen::MatrixXd TrueJac = TestSinMod->Jacobian(std::vector<Eigen::VectorXd>(1, VecIn), 0);
  int numJacEvals = TestSinMod->GetNumCalls("Evaluate");;
  EXPECT_EQ(dim+1,numJacEvals - numPrevEvals);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, Jac, TrueJac, 1e-3);
  
  {
    Eigen::VectorXd targetVec = 0.8*Eigen::VectorXd::Ones(dim);
    Eigen::VectorXd JacX = TestSinMod->JacobianActionByFD(std::vector<Eigen::VectorXd>(1, VecIn), targetVec, 0);
    EXPECT_EQ(TestSinMod->GetNumCalls("Evaluate")-numJacEvals,2);
  
    Eigen::VectorXd TrueJacX = TestSinMod->JacobianAction(std::vector<Eigen::VectorXd>(1, VecIn), targetVec, 0);
    EXPECT_PRED_FORMAT3(MatrixApproxEqual, Jac, TrueJac, 1e-3);
  }
  // create the model
  {
    auto TestFooMod         = make_shared<FooMod>(dim, dim);
    Eigen::MatrixXd HessMat = TestFooMod->HessianByFD(std::vector<Eigen::VectorXd>(1, VecIn), SensIn / dim, 0);
    Eigen::MatrixXd TrueHess = TestFooMod->Hessian(std::vector<Eigen::VectorXd>(1, VecIn), SensIn / dim, 0);

    EXPECT_PRED_FORMAT3(MatrixApproxEqual, HessMat, TrueHess, 1e-3);
  }
}

TEST(ModellingModelTest, MultiEval)
{
  // set the vector size
  int dim = 10;
  
  // create the model
  auto TestMod = make_shared<SinMod>(dim);
  auto TestMod2 = make_shared<MultiTestMod>(dim);
  
  // test the model evaluation
  const int numCopies = 5;
  Eigen::MatrixXd VecIn = Eigen::MatrixXd::Ones(dim,numCopies);
  for(int i=0; i<numCopies; ++i)
    VecIn.col(i) *= i;
  
  
  Eigen::MatrixXd TrueOutput = VecIn.array().sin();
  
  Eigen::MatrixXd VecOut = TestMod->EvaluateMulti(VecIn);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueOutput, VecOut, 1e-14);
  
  Eigen::MatrixXd VecOut2 = TestMod2->EvaluateMulti(VecIn);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueOutput, VecOut2, 1e-14);
}


TEST(ModellingModelTest, SingleEval)
{
  // set the vector size
  int dim = 10;

  // create the model
  auto TestMod = make_shared<SinMod>(dim);

  // test the model evaluation
  Eigen::VectorXd VecIn = Eigen::VectorXd::Ones(dim);


  Eigen::VectorXd VecOut     = TestMod->Evaluate(VecIn);
  Eigen::VectorXd TrueOutput = VecIn.array().sin();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueOutput, VecOut, 1e-14);


  //try it again
  VecOut = TestMod->Evaluate(VecIn);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueOutput, VecOut, 1e-14);

  //and now at a different point
  VecIn      = Eigen::VectorXd::Ones(dim) * 2;
  VecOut     = TestMod->Evaluate(VecIn);
  TrueOutput = VecIn.array().sin();
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueOutput, VecOut, 1e-14);
}

  TEST(ModellingModelTest, SingleGrad)
{
  // set the vector size
  int dim = 10;

  // create the model
  auto TestMod = make_shared<SinMod>(dim);

  // test the model evaluation
  Eigen::VectorXd VecIn = Eigen::VectorXd::Ones(dim);

  Eigen::VectorXd SensIn   = Eigen::VectorXd::Ones(dim);
  Eigen::VectorXd GradVec  = TestMod->Gradient(VecIn, SensIn);
  Eigen::VectorXd TrueGrad = VecIn.array().cos();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueGrad, GradVec, 1e-14);

  //try it again
  GradVec = TestMod->Gradient(VecIn, SensIn);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueGrad, GradVec, 1e-14);

  //and now at a different point
  VecIn    = Eigen::VectorXd::Ones(dim) * 2;
  GradVec  = TestMod->Gradient(VecIn, SensIn);
  TrueGrad = VecIn.array().cos();
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, TrueGrad, GradVec, 1e-14);
}

  TEST(ModellingModelTest, SingleJacobian)
{
  // set the vector size
  int dim = 3;

  // create the model
  auto TestMod = make_shared<SinMod>(dim);

  // test the model evaluation
  Eigen::VectorXd VecIn(3);

  VecIn << 1, 2, .3;

  Eigen::MatrixXd computedJacobian = TestMod->Jacobian(VecIn);
  Eigen::MatrixXd correctJacobian(3, 3);
  correctJacobian << 0.540302305868140, 0, 0, 0,  -0.416146836547142, 0, 0, 0, 0.955336489125606;

  EXPECT_PRED_FORMAT3(muq::Utilities::MatrixApproxEqual, correctJacobian, computedJacobian, 1e-14);

  //do it again
  computedJacobian = TestMod->Jacobian(VecIn);
  EXPECT_PRED_FORMAT3(muq::Utilities::MatrixApproxEqual, correctJacobian, computedJacobian, 1e-14);
}

  TEST(ModellingModelTest, SingleHessian)
{
  const int dimIn  = 5;
  const int dimOut = 2;

  // create the model
  auto TestMod = make_shared<FooMod>(dimIn, dimOut);

  // test the model evaluation
  Eigen::VectorXd VecIn   = Eigen::VectorXd::Ones(dimIn);
  Eigen::VectorXd sensOut = Eigen::VectorXd::Ones(dimOut) / (double)dimOut;

  // compute the Hessian with MUQ
  Eigen::MatrixXd computedHessian = TestMod->Hessian(VecIn, sensOut);

  // compute the correct Hessian
  Eigen::MatrixXd correctHessian = Eigen::MatrixXd::Zero(dimIn, dimIn);

  for (int i = 0; i < dimIn; ++i) {
    correctHessian(i, i) = 6.0 * VecIn(i);

    int j1 = ((i - 1) < 0) ? dimIn - 1 : i - 1;
    correctHessian(i, j1) = 1.0;

    int j2 = ((i + 1) == dimIn) ? 0 : i + 1;
    correctHessian(i, j2) = 1.0;
  }

    EXPECT_PRED_FORMAT3(muq::Utilities::MatrixApproxEqual, correctHessian, computedHessian, 1e-14);

  // do it again
  computedHessian = TestMod->Hessian(VecIn, sensOut);
    EXPECT_PRED_FORMAT3(muq::Utilities::MatrixApproxEqual, correctHessian, computedHessian, 1e-14);
}


// TEST(ModellingModelTest, RepeatEval){
//
//     // set the vector size
//     int dim = 3;
//
//     // create the model
//     Model TestMod = make_shared<SinMod>(dim);
//     Model TestMod2 = make_shared<SinMod>(dim);
//
//     TestMod2.SetInput(TestMod);
//
//     // test the model evaluation
//     Eigen::VectorXd VecIn = Eigen::VectorXd::Ones(dim);
//     TestMod.Update(VecIn);
//
//     Model TestMod3 = make_shared<ReduceMod>(dim);
//     TestMod3.SetInput(TestMod2);
//
//
//     Eigen::VectorXd VecOut(1);
//     TestMod3.Eval(VecOut);
//
//     EXPECT_DOUBLE_EQ(sin(sin(1.0))+sin(sin(1.0)) + sin(sin(1.0)),VecOut[0]);
//
// }
//
//
// TEST(ModellingModelTest, RepeatStdEval){
//
//     // set the vector size
//     int dim = 3;
//
//     // create the model
//     Model TestMod = make_shared<SinMod>(dim);
//     Model TestMod2 = make_shared<SinMod>(dim);
//
//     TestMod2.SetInput(TestMod);
//
//     // test the model evaluation
//     std::vector<double> VecIn(dim,1);
//     TestMod.Update(VecIn);
//
//     Model TestMod3 = make_shared<ReduceMod>(dim);
//     TestMod3.SetInput(TestMod2);
//
//
//     std::vector<double> VecOut(1);
//     TestMod3.Eval(VecOut);
//
//     EXPECT_DOUBLE_EQ(sin(sin(1.0))+sin(sin(1.0)) + sin(sin(1.0)),VecOut[0]);
//
// }
//
// TEST(ModellingModelTest, RepeatDblPtrEval){
//
//     // set the vector size
//     int dim = 3;
//
//     // create the model
//     Model TestMod = make_shared<SinMod>(dim);
//     Model TestMod2 = make_shared<SinMod>(dim);
//
//     TestMod2.SetInput(TestMod);
//
//     // test the model evaluation
//     double* VecIn = new double[dim];
//     for(int i=0; i<dim; ++i)
//         VecIn[i] = 1;
//
//     TestMod.Update(VecIn);
//
//     Model TestMod3 = make_shared<ReduceMod>(dim);
//     TestMod3.SetInput(TestMod2);
//
//
//     double* VecOut = new double[1];
//     TestMod3.Eval(VecOut);
//
//     EXPECT_DOUBLE_EQ(sin(sin(1.0))+sin(sin(1.0)) + sin(sin(1.0)),VecOut[0]);
//
//
//     delete[] VecIn;
//     delete[] VecOut;
//
// }
//
//
// TEST(ModellingModelTest, RepeatGrad){
//
//     // set the vector size
//     int dim = 3;
//
//     // create the model
//     Model TestMod = make_shared<SinMod>(dim);
//     Model TestMod2 = make_shared<SinMod>(dim);
//
//     TestMod2.SetInput(TestMod);
//
//     // test the model evaluation
//     Eigen::VectorXd VecIn = Eigen::VectorXd::Ones(dim);
//     TestMod.Update(VecIn);
//
//     Model TestMod3 = make_shared<ReduceMod>(dim);
//     TestMod3.SetInput(TestMod2);
//
//     Eigen::VectorXd GradOut = Eigen::VectorXd::Zero(dim);
//     Eigen::VectorXd SensIn = Eigen::VectorXd::Ones(1);
//     TestMod3.Grad(TestMod,SensIn,GradOut);
//
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[0])),GradOut[0]);
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[1])),GradOut[1]);
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[2])),GradOut[2]);
//
//     // reset the gradient
//     GradOut = Eigen::VectorXd::Zero(dim);
//
//     // get the gradient wrt the middle variable
//     TestMod3.Grad(TestMod2,SensIn,GradOut);
//     EXPECT_DOUBLE_EQ(1,GradOut[0]);
//     EXPECT_DOUBLE_EQ(1,GradOut[1]);
//     EXPECT_DOUBLE_EQ(1,GradOut[2]);
//
// }
//
//
// TEST(ModellingModelTest, RepeatStdGrad){
//
//     // set the vector size
//     int dim = 3;
//
//     // create the model
//     Model TestMod = make_shared<SinMod>(dim);
//     Model TestMod2 = make_shared<SinMod>(dim);
//
//     TestMod2.SetInput(TestMod);
//
//     // test the model evaluation
//     std::vector<double> VecIn(dim,1);
//     TestMod.Update(VecIn);
//
//     Model TestMod3 = make_shared<ReduceMod>(dim);
//     TestMod3.SetInput(TestMod2);
//
//     std::vector<double> GradOut(dim,0);
//     std::vector<double> SensIn(1,1);
//     TestMod3.Grad(TestMod,SensIn,GradOut);
//
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[0])),GradOut[0]);
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[1])),GradOut[1]);
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[2])),GradOut[2]);
//
//     // reset the gradient
//     GradOut.clear();
//     GradOut.resize(dim,0);
//
//     // get the gradient wrt the middle variable
//     TestMod3.Grad(TestMod2,SensIn,GradOut);
//     EXPECT_DOUBLE_EQ(1,GradOut[0]);
//     EXPECT_DOUBLE_EQ(1,GradOut[1]);
//     EXPECT_DOUBLE_EQ(1,GradOut[2]);
//
// }
//
//
// TEST(ModellingModelTest, RepeatDblPtrGrad){
//
//     // set the vector size
//     int dim = 3;
//
//     // create the model
//     Model TestMod = make_shared<SinMod>(dim);
//     Model TestMod2 = make_shared<SinMod>(dim);
//
//     TestMod2.SetInput(TestMod);
//
//     // test the model evaluation
//     double* VecIn = new double[dim];
//     for(int i=0; i<dim; ++i)
//         VecIn[i] = 1.0;
//
//     TestMod.Update(VecIn);
//
//     Model TestMod3 = make_shared<ReduceMod>(dim);
//     TestMod3.SetInput(TestMod2);
//
//     double* GradOut = new double[dim];
//     for(int i=0; i<dim; ++i)
//         GradOut[i] = 0.0;
//
//     double* SensIn = new double[1];
//     SensIn[0] = 1;
//
//     TestMod3.Grad(TestMod,SensIn,GradOut);
//
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[0])),GradOut[0]);
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[1])),GradOut[1]);
//     EXPECT_DOUBLE_EQ(cos(sin(VecIn[2])),GradOut[2]);
//
//     // reset the gradient
//     for(int i=0; i<dim; ++i)
//         GradOut[i] = 0.0;
//
//     // get the gradient wrt the middle variable
//     TestMod3.Grad(TestMod2,SensIn,GradOut);
//     EXPECT_DOUBLE_EQ(1,GradOut[0]);
//     EXPECT_DOUBLE_EQ(1,GradOut[1]);
//     EXPECT_DOUBLE_EQ(1,GradOut[2]);
//
//     delete[] VecIn;
//     delete[] GradOut;
//     delete[] SensIn;
//
// }
//
//
// TEST(ModellingModelTest, MidUpdateTest){
//
//     // run Eval(std::vector*, std::vector*)
//     Model Internal = make_shared<SinMod>(3);
//     Model CompTest = make_shared<ReduceMod>(3);
//     Model Internal2 = make_shared<SinMod>(3);
//
//     CompTest.SetInput(Internal2);
//     Internal2.SetInput(Internal);
//
//     Eigen::VectorXd VecIn = Eigen::VectorXd::Ones(3);
//     Internal.Update( VecIn );
//
//     Eigen::VectorXd VecOut(1);
//     CompTest.Eval(VecOut);
//
//     // check output for correctness
//     EXPECT_DOUBLE_EQ(sin(sin(VecIn[0])) + sin(sin(VecIn[1])) + sin(sin(VecIn[2])),VecOut[0]);
//
//     Internal2.Update(VecIn);
//     CompTest.Eval(VecOut);
//     EXPECT_DOUBLE_EQ(sin(VecIn[0]) + sin(VecIn[1]) + sin(VecIn[2]),VecOut[0]);
//
// }
//
//
//
// // A tricky example, that tries to break the evaluation with a goofy dependency structure
// TEST(ModellingModelTest, DependecyStressTest){
//
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model SingleInput2 = make_shared<SinMod>(3);
//
//     // create a two input, three dimensional expression
//     Model TwoInputExpr = make_shared<SumMod>(3);
//
//     // create an odd dependence structure, the sum will depend on both of the other inputs, and one of the single
// inputs will depend on the other
//     TwoInputExpr.SetInput(SingleInput1,0);
//     TwoInputExpr.SetInput(SingleInput2,1);
//
//     SingleInput2.SetInput(SingleInput1,0);
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//     SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd output(3);
//     TwoInputExpr.Eval(output);
//
//     // compare the output to what we should get
//     EXPECT_DOUBLE_EQ(sin(sin(input[0]))+sin(input[0]),output[0]);
//     EXPECT_DOUBLE_EQ(sin(sin(input[1]))+sin(input[1]),output[1]);
//     EXPECT_DOUBLE_EQ(sin(sin(input[2]))+sin(input[2]),output[2]);
//
// }
//
//
// // A tricky example, that tries to break the evaluation with a goofy dependency structure
// TEST(ModellingModelTest, DependecyStressGradTest){
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model SingleInput2 = make_shared<SinMod>(3);
//
//     // create a two input, three dimensional expression
//     Model TwoInputExpr = make_shared<SumMod>(3);
//
//     // create an odd dependence structure, the sum will depend on both of the other inputs, and one of the single
// inputs will depend on the other
//     TwoInputExpr.SetInput(SingleInput1,0);
//     TwoInputExpr.SetInput(SingleInput2,1);
//
//     SingleInput2.SetInput(SingleInput1,0);
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//     SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd SensIn = Eigen::VectorXd::Ones(3);
//     Eigen::VectorXd GradOut = Eigen::VectorXd::Zero(3);
//
//     // compute the gradient with respect to the tricky variable
//     TwoInputExpr.Grad(SingleInput1,SensIn,GradOut);
//
//     // compare the output to what we should get
//     EXPECT_DOUBLE_EQ(cos(sin(input[0]))+1,GradOut[0]);
//     EXPECT_DOUBLE_EQ(cos(sin(input[1]))+1,GradOut[1]);
//     EXPECT_DOUBLE_EQ(cos(sin(input[2]))+1,GradOut[2]);
//
// }
//
//
// // A tricky example, that tries to break the evaluation with a goofy dependency structure
// TEST(ModellingModelTest, ParallelEvalTest){
//
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model MidRange1 = make_shared<SinMod>(3);
//     Model MidRange2 = make_shared<SinMod>(3);
//     Model MidRange3 = make_shared<SinMod>(3);
//     Model MidRange4 = make_shared<SinMod>(3);
//
//     MidRange1.SetInput(SingleInput1);
//     MidRange2.SetInput(SingleInput1);
//     MidRange3.SetInput(SingleInput1);
//     MidRange4.SetInput(SingleInput1);
//
//     // create a two input, three dimensional expression
//     Model TwoInputExpr = make_shared<SumMod>(3);
//     TwoInputExpr.SetInput(MidRange1,0);
//     TwoInputExpr.SetInput(MidRange2,1);
//
//     Model TwoInputExpr2 = make_shared<SumMod>(3);
//     TwoInputExpr2.SetInput(MidRange3,0);
//     TwoInputExpr2.SetInput(MidRange4,1);
//
//     Model TwoInputExpr3 = make_shared<SumMod>(3);
//     TwoInputExpr3.SetInput(TwoInputExpr,0);
//     TwoInputExpr3.SetInput(TwoInputExpr2,1);
//
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//     SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd output(3);
//     TwoInputExpr3.Eval(output);
//
//     // compare the output to what we should get
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[0])),output[0]);
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[1])),output[1]);
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[2])),output[2]);
//
// }
//
//
//
// // A tricky example, that tries to break the evaluation with a goofy dependency structure
// TEST(ModellingModelTest, ParallelCompositionTest){
//
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model MidRange1 = make_shared<SinMod>(3);
//     Model MidRange2 = make_shared<SinMod>(3);
//     Model MidRange3 = make_shared<SinMod>(3);
//     Model MidRange4 = make_shared<SinMod>(3);
//
//     Model TwoInputExpr = make_shared<SumMod>(3);
//     Model TwoInputExpr2 = make_shared<SumMod>(3);
//     Model TwoInputExpr3 = make_shared<SumMod>(3);
//
//     // test the composition operator for setting all the inputs
//     TwoInputExpr3(TwoInputExpr(MidRange1(SingleInput1),MidRange2(SingleInput1)),
//  TwoInputExpr2(MidRange3(SingleInput1),MidRange4(SingleInput1)));
//
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//     SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd output(3);
//     TwoInputExpr3.Eval(output);
//
//     // compare the output to what we should get
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[0])),output[0]);
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[1])),output[1]);
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[2])),output[2]);
//
// }
//
// // A tricky example, that tries to break the evaluation with a goofy dependency structure
// TEST(ModellingModelTest, CacheRunOrderTest){
//
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model MidRange1 = make_shared<SinMod>(3);
//     Model MidRange2 = make_shared<SinMod>(3);
//     Model MidRange3 = make_shared<SinMod>(3);
//     Model MidRange4 = make_shared<SinMod>(3);
//
//     Model TwoInputExpr = make_shared<SumMod>(3);
//     Model TwoInputExpr2 = make_shared<SumMod>(3);
//     Model TwoInputExpr3 = make_shared<SumMod>(3);
//
//     // test the composition operator for setting all the inputs
//     TwoInputExpr3(TwoInputExpr(MidRange1(SingleInput1),MidRange2(SingleInput1)),
//  TwoInputExpr2(MidRange3(SingleInput1),MidRange4(SingleInput1)));
//
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//
//     for(int i=0; i<10; ++i)
//         SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd output(3);
//     TwoInputExpr3.Eval(output);
//
//     // compare the output to what we should get
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[0])),output[0]);
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[1])),output[1]);
//     EXPECT_DOUBLE_EQ(4*sin(sin(input[2])),output[2]);
//
// }
//
//
//
// // test the parallel gradient evaluation
// TEST(ModellingModelTest, ParallelGradTest){
//
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model MidRange1 = make_shared<SinMod>(3);
//     Model MidRange2 = make_shared<SinMod>(3);
//     Model MidRange3 = make_shared<SinMod>(3);
//     Model MidRange4 = make_shared<SinMod>(3);
//
//     MidRange1.SetInput(SingleInput1);
//     MidRange2.SetInput(SingleInput1);
//     MidRange3.SetInput(SingleInput1);
//     MidRange4.SetInput(SingleInput1);
//
//     // create a two input, three dimensional expression
//     Model TwoInputExpr = make_shared<SumMod>(3);
//     TwoInputExpr.SetInput(MidRange1,0);
//     TwoInputExpr.SetInput(MidRange2,1);
//
//     Model TwoInputExpr2 = make_shared<SumMod>(3);
//     TwoInputExpr2.SetInput(MidRange3,0);
//     TwoInputExpr2.SetInput(MidRange4,1);
//
//     Model TwoInputExpr3 = make_shared<SumMod>(3);
//     TwoInputExpr3.SetInput(TwoInputExpr,0);
//     TwoInputExpr3.SetInput(TwoInputExpr2,1);
//
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//     SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd GradOut = Eigen::VectorXd::Zero(3);
//     Eigen::VectorXd SensIn = Eigen::VectorXd::Ones(3);
//     TwoInputExpr3.Grad(SingleInput1,SensIn,GradOut);
//
//     // compare the output to what we should get
//     EXPECT_DOUBLE_EQ(4*cos(sin(input[0])),GradOut[0]);
//     EXPECT_DOUBLE_EQ(4*cos(sin(input[1])),GradOut[1]);
//     EXPECT_DOUBLE_EQ(4*cos(sin(input[2])),GradOut[2]);
//
// }
//
// // test the parallel gradient evaluation
// TEST(ModellingModelTest, ParallelFDGradTest){
//
//
//     // now, create two other single input expressions
//     Model SingleInput1 = make_shared<SinMod>(3);
//     Model MidRange1 = make_shared<SinMod>(3);
//     Model MidRange2 = make_shared<SinMod>(3);
//     Model MidRange3 = make_shared<SinMod>(3);
//     Model MidRange4 = make_shared<SinMod>(3);
//
//     MidRange1.SetInput(SingleInput1);
//     MidRange2.SetInput(SingleInput1);
//     MidRange3.SetInput(SingleInput1);
//     MidRange4.SetInput(SingleInput1);
//
//     // create a two input, three dimensional expression
//     Model TwoInputExpr = make_shared<SumMod>(3);
//     TwoInputExpr.SetInput(MidRange1,0);
//     TwoInputExpr.SetInput(MidRange2,1);
//
//     Model TwoInputExpr2 = make_shared<SumMod>(3);
//     TwoInputExpr2.SetInput(MidRange3,0);
//     TwoInputExpr2.SetInput(MidRange4,1);
//
//     Model TwoInputExpr3 = make_shared<SumMod>(3);
//     TwoInputExpr3.SetInput(TwoInputExpr,0);
//     TwoInputExpr3.SetInput(TwoInputExpr2,1);
//
//
//     // now, update SingleInput2, which drives this whole system
//     Eigen::VectorXd input = Eigen::VectorXd::Ones(3);
//     SingleInput1.Update(input);
//
//
//     // evaluate the output and see what we get
//     Eigen::VectorXd GradOut = Eigen::VectorXd::Zero(3);
//     Eigen::VectorXd SensIn = Eigen::VectorXd::Ones(3);
//     TwoInputExpr3.Grad(SingleInput1,SensIn,GradOut,1e-5,1e-10);
//
//     // compare the output to what we should get
//     EXPECT_NEAR(4*cos(sin(input[0])),GradOut[0],1e-4);
//     EXPECT_NEAR(4*cos(sin(input[1])),GradOut[1],1e-4);
//     EXPECT_NEAR(4*cos(sin(input[2])),GradOut[2],1e-4);
//
// }
//
// TEST(ModellingModelTest, ScopeTest){
//
//     Model Input1 = make_shared<ModParameter>(3,-1.0);
//     Model Output;
//     {
//     Model Middle = sin(Input1);
//     Output = exp(Middle);
//     }
//
//     Eigen::VectorXd OutVec(3);
//     Output.Eval(OutVec);
//
//     for(int i=0; i<3; ++i)
//       EXPECT_DOUBLE_EQ(exp(sin(-1.0)),OutVec[i]);
//
//     Eigen::VectorXd InVec = Eigen::VectorXd::Ones(3);
//     Input1.Update(InVec);
//
//     Output.Eval(OutVec);
//     for(int i=0; i<3; ++i)
//       EXPECT_DOUBLE_EQ(exp(sin(1.0)),OutVec[i]);
//
//
// }
//
// TEST(ModellingModelTest, GetPtrTest){
//
//     // set up a few different models
//     Model Internal = make_shared<SinMod>(3);
//     Model CompTest = make_shared<ReduceMod>(3);
//     Model Internal2 = make_shared<SinMod>(3);
//
//     // set the inputs
//     CompTest.SetInput(Internal2);
//     Internal2.SetInput(Internal);
//
//     // update the Model furthest "upstream"
//     Eigen::VectorXd VecIn = Eigen::VectorXd::Ones(3);
//     Internal.Update( VecIn );
//
//     // evaluate the model furthest "downstream"
//     Eigen::VectorXd VecOut(1);
//     CompTest.Eval(VecOut);
//
//     // get a ReducedMod smart pointer to CompTest base
//     std::shared_ptr<ReduceMod> BasePtr = CompTest.GetOutPtr<ReduceMod>();
//
//     // call a function of ReducedMod that is not available in Model
//     BasePtr->AddOne();
//
//     // Get the state again
//     Eigen::VectorXd VecOut2(1);
//     CompTest.Eval(VecOut2);
//
//     // make sure everything worked as expected
//     EXPECT_DOUBLE_EQ(VecOut[0]+1,VecOut2[0]);
//
// }
