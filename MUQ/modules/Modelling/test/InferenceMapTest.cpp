
#include "gtest/gtest.h"

#include <iostream>
#include <fstream>

#include "MUQ/Modelling/OptimalMaps/InferenceMap.h"
#include "MUQ/Modelling/OptimalMaps/InferenceMap2.h"
#include "MUQ/Modelling/Model.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include <Eigen/Dense>
#include "MUQ/Modelling/GaussianRV.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/ComposedRandVar.h"
#include "MUQ/Modelling/Auctioneer.h"

using namespace muq::Modelling;
using namespace muq::utilities;
using namespace std;

class ModellingCubicInferenceTest : public::testing::Test {
protected:

  ModellingCubicInferenceTest()
  {
    // how many points to use in the regression
    Npts   = 5000;
    porder = 5;

    // observation in cubic model
    b = 1.0;

    // Define the prior
    Eigen::MatrixXd PriorCov(1, 1);
    PriorCov << 1.0;

    Eigen::MatrixXd PriorMu = Eigen::VectorXd::Zero(1);

    Prior = make_shared<GaussianRV>(PriorMu, PriorCov);

    // generate the prior samples from a Latin Hypercube
    priorSamps = Prior->LhsSample(Npts);

    // generate reference samples
    dRefSamps.resize(1, Npts);
    xRefSamps.resize(1, Npts);
    for (unsigned int i = 0; i < Npts; ++i) {
      dRefSamps(0, i) = RandomGenerator::GetNormal();
      xRefSamps(0, i) = RandomGenerator::GetNormal();
    }

    // define the additive error model
    Eigen::VectorXd dat = 0.0 * Eigen::VectorXd::Ones(1);
    Data = make_shared<GaussianRV>(dat, 1.0);

    RefPrior = make_shared<GaussianRV>(1);
  }

  double b;
  unsigned int Npts, porder;
  std::shared_ptr<GaussianRV> Data;
  std::shared_ptr<GaussianRV> Prior;

  Eigen::MatrixXd priorSamps;

  Eigen::MatrixXd dRefSamps;
  Eigen::MatrixXd xRefSamps;

  RandVar RefPrior;
};


//compare L2 assignment vs weighted "t" triangular distance
TEST(ModellingInferenceMapTest, AssignCompTest)
{
  unsigned int Nsamps = 30;
  Eigen::MatrixXd JointSamps(3, Nsamps);

  JointSamps.setRandom(); // fill with uniform random variables


  // compute the fully enforced distance matrix for the error term alone
  Eigen::MatrixXd FullDist(Nsamps, Nsamps);
  for (unsigned int i = 0; i < Nsamps; ++i) {
    for (unsigned int j = 0; j < Nsamps; ++j) {
      FullDist(i, j) = -1.0 * pow(JointSamps(0, i) - JointSamps(0, j), 2.0);
    }
  }

  // compute the assignment based on the full dist
  Auctioneer<float> Solver1(FullDist);
  Solver1.solve();
  std::vector<unsigned int> Ass1 = Solver1.GetSolution();

  // create the t-weighted distance matrix
  Eigen::MatrixXd TDist(Nsamps, Nsamps);
  Eigen::VectorXd ws(3);
  ws << 1.0, 1.0e-2, 1.0e-2;
  for (unsigned int i = 0; i < Nsamps; ++i) {
    for (unsigned int j = 0; j < Nsamps; ++j) {
      TDist(i,
            j) = -1.0 *
                 (JointSamps.col(i) -
                  JointSamps.col(j)).transpose() * ws.asDiagonal() * (JointSamps.col(i) - JointSamps.col(j));
    }
  }


  // compute the assignment for the t-weighted distance
  Auctioneer<float> Solver2(FullDist);
  Solver2.solve();
  std::vector<unsigned int> Ass2 = Solver2.GetSolution();
  for (unsigned int i = 0; i < Nsamps; ++i) {
    EXPECT_EQ(Ass1[i], Ass2[i]);
  }
}


TEST(ModellingInferenceMap2Test, BasicTest)
{
  unsigned int Npts = 1000;

  // first, build the prior
  Eigen::MatrixXd PriorCov(2, 2);

  PriorCov << 1.0, 0.6, 0.6, 1.0;


  Eigen::MatrixXd PriorMu = Eigen::VectorXd::Zero(2);


  Eigen::MatrixXd PriorSamps(2, Npts);
  Eigen::MatrixXd DataSamps(1, Npts);

  // create a random variable for the additive error model
  Eigen::VectorXd dat = 0.0 * Eigen::VectorXd::Ones(1);
  GaussianRV Data(dat, 1.0);


  // generate a bunch of samples of the prior and model output
  GaussianRV Prior(PriorMu, PriorCov);

  // generate the prior samples from a Latin Hypercube
  Eigen::MatrixXd inSamps = Prior.LhsSample(Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    Eigen::VectorXd inSamp = inSamps.col(i);

    Eigen::VectorXd error(1);
    Data.sample(error);
    PriorSamps.col(i) = inSamp;
    DataSamps(0, i)   = inSamp(1) + error(0);
  }


  // build the map
  Model testMap = make_shared<InferenceMap2>(PriorSamps, DataSamps, 1, 1);

  // set the data
  Model trueData = make_shared<ModParameter>(1, 0.1);
  testMap.SetInput(trueData, 1);

  // get a bunch of samples of the posterior
  Model refInput = make_shared<ModParameter>(2, 0);
  testMap.SetInput(refInput, 0);

  RandVar RefPrior    = make_shared<GaussianRV>(2);
  RandVar postRandVar = make_shared<ComposedRandVar>(testMap, refInput, RefPrior);


  Eigen::VectorXd  samp(2);
  EmpiricalRandVar postSamps(2);
  postSamps.ExpectedSize(100000);

  for (unsigned int i = 0; i < 100000; ++i) {
    postRandVar.sample(samp);
    postSamps.addSamp(samp);
  }


  // make sure our results match the analytic solution
  Eigen::VectorXd postMu(2);
  Eigen::MatrixXd postCov(2, 2);
  postSamps.getMeanCov(postMu, postCov);

  //    std::cout << postMu << std::endl << std::endl;
  //    std::cout << postCov << std::endl;
  EXPECT_NEAR(0.82, postCov(0, 0), 1e-1);
  EXPECT_NEAR(0.3,  postCov(0, 1), 1e-1);
  EXPECT_NEAR(0.5,  postCov(1, 1), 1e-1);
  EXPECT_NEAR(0.03, postMu(0),     1e-1);
  EXPECT_NEAR(0.05, postMu(1),     1e-1);
}

TEST(ModellingInferenceMap2Test, BasicGaussTest)
{
  unsigned int Npts = 1000;

  // first, build the prior
  Eigen::MatrixXd PriorCov(2, 2);

  PriorCov << 1.0, 0.6, 0.6, 1.0;


  Eigen::MatrixXd PriorMu = Eigen::VectorXd::Zero(2);


  Eigen::MatrixXd PriorSamps(2, Npts);
  Eigen::MatrixXd DataSamps(1, Npts);

  // create a random variable for the additive error model
  Eigen::VectorXd dat = 0.0 * Eigen::VectorXd::Ones(1);
  GaussianRV Data(dat, 1.0);


  // generate a bunch of samples of the prior and model output
  GaussianRV Prior(PriorMu, PriorCov);

  // generate the prior samples from a Latin Hypercube
  Eigen::MatrixXd inSamps = Prior.LhsSample(Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    Eigen::VectorXd inSamp = inSamps.col(i);

    Eigen::VectorXd error(1);
    Data.sample(error);
    PriorSamps.col(i) = inSamp;
    DataSamps(0, i)   = inSamp(1) + error(0);
  }


  // build the map
  Model testMap = make_shared<InferenceMap2>(PriorSamps, DataSamps, PriorMu, PriorCov, 1, 1);

  // set the data
  Model trueData = make_shared<ModParameter>(1, 0.1);
  testMap.SetInput(trueData, 1);

  // get a bunch of samples of the posterior
  Model refInput = make_shared<ModParameter>(2, 0);
  testMap.SetInput(refInput, 0);

  RandVar RefPrior    = make_shared<GaussianRV>(2);
  RandVar postRandVar = make_shared<ComposedRandVar>(testMap, refInput, RefPrior);


  Eigen::VectorXd  samp(2);
  EmpiricalRandVar postSamps(2);
  postSamps.ExpectedSize(100000);

  for (unsigned int i = 0; i < 100000; ++i) {
    postRandVar.sample(samp);
    postSamps.addSamp(samp);
  }


  // make sure our results match the analytic solution
  Eigen::VectorXd postMu(2);
  Eigen::MatrixXd postCov(2, 2);
  postSamps.getMeanCov(postMu, postCov);

  //    std::cout << postMu << std::endl << std::endl;
  //    std::cout << postCov << std::endl;
  EXPECT_NEAR(0.82, postCov(0, 0), 1e-1);
  EXPECT_NEAR(0.3,  postCov(0, 1), 1e-1);
  EXPECT_NEAR(0.5,  postCov(1, 1), 1e-1);
  EXPECT_NEAR(0.03, postMu(0),     5e-2);
  EXPECT_NEAR(0.05, postMu(1),     5e-2);
}


TEST_F(ModellingCubicInferenceTest, Test1)
{
  Eigen::MatrixXd DataSamps(1, Npts);

  // generate the data samps
  double tempVar = 0.0;

  for (unsigned int i = 0; i < Npts; ++i) {
    Eigen::VectorXd inSamp = priorSamps.col(i);

    Eigen::VectorXd error(1);
    Data->sample(error);
    tempVar        += error[0] * error[0];
    DataSamps(0, i) = pow(inSamp(0) - b, 3) + error(0);
  }


  // build the map
  Eigen::VectorXd ws = Eigen::VectorXd::Ones(Npts);


  Model testMap = make_shared<InferenceMap>(priorSamps, DataSamps, xRefSamps, dRefSamps, ws, porder, "Hermite");

  // set the data
  Model trueData = make_shared<ModParameter>(1, 0.0);
  testMap.SetInput(trueData, 1);

  // get a bunch of samples of the posterior
  Model refInput = make_shared<ModParameter>(1, 0.0);
  testMap.SetInput(refInput, 0);

  RandVar postRandVar = make_shared<ComposedRandVar>(testMap, refInput, RefPrior);


  Eigen::VectorXd  samp(1);
  EmpiricalRandVar postSamps(1);
  postSamps.ExpectedSize(10000);


  // write the input-output relationship to a file
  ofstream fout("CubicMap1.txt");
  for (unsigned int i = 0; i < 10000; ++i) {
    // get the gaussian reference sample
    Eigen::VectorXd rsamp(1);
    rsamp[0] = RandomGenerator::GetNormal();

    // update the map
    refInput.Update(rsamp);

    // evaluate the output
    testMap.Eval(samp);

    fout << rsamp[0] << "  " << samp[0] << "\n";
    postSamps.addSamp(samp);
  }
  fout.close();

  postSamps.WriteRawBinary("CubicTestData1.dat");
}


TEST_F(ModellingCubicInferenceTest, Test2)
{
  Eigen::MatrixXd DataSamps(1, Npts);

  // generate the data samps
  for (unsigned int i = 0; i < Npts; ++i) {
    Eigen::VectorXd inSamp = priorSamps.col(i);
    DataSamps(0, i) = pow(inSamp(0) - b, 3);
  }

  // build the inference map
  Eigen::VectorXd ws = Eigen::VectorXd::Ones(Npts);


  Model testMap = make_shared<InferenceMap>(priorSamps, DataSamps, xRefSamps, dRefSamps, ws, porder, "Hermite");

  // set the data
  Model trueData = make_shared<ModParameter>(1, 0.0);
  testMap.SetInput(trueData, 1);

  // get a bunch of samples of the posterior
  Model refInput = make_shared<ModParameter>(1, 0.0);
  testMap.SetInput(refInput, 0);


  RandVar postRandVar = make_shared<ComposedRandVar>(testMap, refInput, RefPrior);


  Eigen::VectorXd  samp(1);
  EmpiricalRandVar postSamps(1);
  postSamps.ExpectedSize(10000);

  // write the input-output relationship to a file
  ofstream fout("CubicMap2.txt");
  for (unsigned int i = 0; i < 10000; ++i) {
    // get the gaussian reference sample
    Eigen::VectorXd rsamp(1);
    rsamp[0] = RandomGenerator::GetNormal();

    Eigen::VectorXd dsamp(1);
    Data->sample(dsamp);

    // update the data
    trueData.Update(dsamp);

    // update the map
    refInput.Update(rsamp);

    // evaluate the output
    testMap.Eval(samp);

    fout << rsamp[0] << "  " << samp[0] << "\n";
    postSamps.addSamp(samp);
  }
  fout.close();


  postSamps.WriteRawBinary("CubicTestData2.dat");
}


TEST(ModellingInferenceMapTest, BasicTest_model)
{
  unsigned int Npts = 500;

  // first, build the prior
  Eigen::MatrixXd PriorCov(2, 2);

  PriorCov << 1.0, 0.6, 0.6, 1.0;


  Eigen::MatrixXd PriorMu = Eigen::VectorXd::Zero(2);


  Eigen::MatrixXd PriorSamps(2, Npts);
  Eigen::MatrixXd DataSamps(1, Npts);

  // create a random variable for the additive error model
  Eigen::VectorXd dat = 0.0 * Eigen::VectorXd::Ones(1);
  GaussianRV Data(dat, 1.0);


  // generate a bunch of samples of the prior and model output
  GaussianRV Prior(PriorMu, PriorCov);

  // generate the prior samples from a Latin Hypercube
  Eigen::MatrixXd inSamps = Prior.LhsSample(Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    Eigen::VectorXd inSamp = inSamps.col(i);

    Eigen::VectorXd error(1);
    Data.sample(error);
    PriorSamps.col(i) = inSamp;
    DataSamps(0, i)   = inSamp(1) + error(0);
  }


  // generate a bunch of samples from the reference spaces
  RandVar RefPrior = make_shared<GaussianRV>(2);
  RandVar RefData  = make_shared<GaussianRV>(1);

  Eigen::MatrixXd PriorRefSamps(2, Npts);
  Eigen::MatrixXd DataRefSamps(1, Npts);


  for (unsigned int i = 0; i < Npts; ++i) {
    Eigen::VectorXd inSamp(2);
    RefPrior.sample(inSamp);

    Eigen::VectorXd inSamp2(1);
    RefData.sample(inSamp2);

    PriorRefSamps.col(i) = inSamp;
    DataRefSamps.col(i)  = inSamp2;
  }

  Eigen::VectorXd weights = Eigen::VectorXd::Ones(Npts);

  // build the map
  Model testMap = make_shared<InferenceMap>(PriorSamps, DataSamps, PriorRefSamps, DataRefSamps, weights, 1, "Hermite");

  // set the data
  Model trueData = make_shared<ModParameter>(1, 0.1);
  testMap.SetInput(trueData, 1);

  // get a bunch of samples of the posterior
  Model refInput = make_shared<ModParameter>(2, 0);
  testMap.SetInput(refInput, 0);

  RandVar postRandVar = make_shared<ComposedRandVar>(testMap, refInput, RefPrior);


  Eigen::VectorXd  samp(2);
  EmpiricalRandVar postSamps(2);
  postSamps.ExpectedSize(10000);

  for (unsigned int i = 0; i < 10000; ++i) {
    postRandVar.sample(samp);
    postSamps.addSamp(samp);
  }


  // make sure our results match the analytic solution
  Eigen::VectorXd postMu(2);
  Eigen::MatrixXd postCov(2, 2);
  postSamps.getMeanCov(postMu, postCov);


  EXPECT_NEAR(0.82, postCov(0, 0), 1e-1);
  EXPECT_NEAR(0.3,  postCov(0, 1), 1e-1);
  EXPECT_NEAR(0.5,  postCov(1, 1), 1e-1);
  EXPECT_NEAR(0.03, postMu(0),     1e-1);
  EXPECT_NEAR(0.05, postMu(1),     1e-1);
}
