#include "MUQ/Modelling/python/RosenbrockPairPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

RosenbrockPairPython::RosenbrockPairPython(double const a, double const b) : RosenbrockPair(a, b) {}

shared_ptr<RosenbrockDensity> RosenbrockPairPython::GetDensity() const
{
  return dynamic_pointer_cast<RosenbrockDensity>(density);
}

shared_ptr<RosenbrockRV> RosenbrockPairPython::GetRV() const
{
  return dynamic_pointer_cast<RosenbrockRV>(rv);
}

void muq::Modelling::ExportRosenbrockPair()
{
  py::class_<RosenbrockPairPython, shared_ptr<RosenbrockPairPython>, boost::noncopyable> exportRosenPair("RosenbrockPair", py::init<double const, double const>());

  exportRosenPair.def("GetDensity", &RosenbrockPairPython::GetDensity);
  exportRosenPair.def("GetRV", &RosenbrockPairPython::GetRV);
}
