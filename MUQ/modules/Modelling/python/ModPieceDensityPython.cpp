#include "MUQ/Modelling/python/ModPieceDensityPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

void muq::Modelling::ExportModPieceDensity()
{
  py::class_<ModPieceDensity, shared_ptr<ModPieceDensity>, py::bases<Density, ModPiece>, boost::noncopyable> exportModPieceDens("ModPieceDensity", py::init<shared_ptr<ModPiece> >());

  py::implicitly_convertible<shared_ptr<ModPieceDensity>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<ModPieceDensity>, shared_ptr<Density> >();
}
