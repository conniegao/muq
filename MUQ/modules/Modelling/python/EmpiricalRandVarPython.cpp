
#include "MUQ/Modelling/python/EmpiricalRandVarPython.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Utilities;

EmpiricalRandVarPython::EmpiricalRandVarPython(int dim) : EmpiricalRandVar(dim) {}

EmpiricalRandVarPython::EmpiricalRandVarPython(Eigen::VectorXd const& input) : EmpiricalRandVar(input) {}

EmpiricalRandVarPython::EmpiricalRandVarPython(Eigen::MatrixXd const& input) : EmpiricalRandVar(input) {}

shared_ptr<EmpiricalRandVarPython> EmpiricalRandVarPython::Create(boost::python::list const& input, bool isVector)
{
  if (isVector) {
    return make_shared<EmpiricalRandVarPython>(GetEigenVector<Eigen::VectorXd>(input));
  } else {
    Eigen::MatrixXd in = GetEigenMatrix(input).transpose();
    return make_shared<EmpiricalRandVarPython>(in);
  }
}

void muq::Modelling::ExportEmpiricalRandVar()
{
  // expose to python
  boost::python::class_<EmpiricalRandVarPython, std::shared_ptr<EmpiricalRandVarPython>, boost::python::bases<ModPiece>, boost::noncopyable> exportERV(
    "EmpiricalRandVar",
    boost::python::init<int>());

  // create other constructors
  exportERV.def("__init__", boost::python::make_constructor(&EmpiricalRandVarPython::Create));

  // expose public member functions
  exportERV.def("GetMean", &EmpiricalRandVar::PyGetMean);
  exportERV.def("GetCov", &EmpiricalRandVar::PyGetCov);
  exportERV.def("ExpectedSize", &EmpiricalRandVar::ExpectedSize);
  exportERV.def("AddSamp", &EmpiricalRandVar::PyAddSamp);
  exportERV.def("AddSamps", &EmpiricalRandVar::PyAddSamps);
  exportERV.def("GetSamp", &EmpiricalRandVar::PyGetSamp);
  exportERV.def("NumSamps", &EmpiricalRandVar::NumSamps);
  exportERV.def("WriteHDF5", &EmpiricalRandVar::WriteHDF5);
  exportERV.def("GetEss", &EmpiricalRandVar::PyGetEss);
  exportERV.def("GetAllSamples", &EmpiricalRandVar::PyGetAllSamples);
  exportERV.def("WriteRawBinary", &EmpiricalRandVar::WriteRawBinary);
  exportERV.def("SwapLastSample", &EmpiricalRandVar::SwapLastSample);

  // expose public member objects
  //   exportERV.def_readwrite("hdf5Output", &EmpiricalRandVar::hdf5Output);
  // exportERV.def_readwrite("hdf5OutputFrequency", &EmpiricalRandVar::hdf5OutputFrequency);

  // register parent pointer to python
  boost::python::register_ptr_to_python<std::shared_ptr<EmpiricalRandVar> >();

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<EmpiricalRandVarPython>, std::shared_ptr<EmpiricalRandVar> >();

  boost::python::implicitly_convertible<std::shared_ptr<EmpiricalRandVarPython>, std::shared_ptr<ModPiece> >();
}

