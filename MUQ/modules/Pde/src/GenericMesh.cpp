#include "MUQ/Pde/GenericMesh.h"

using namespace std;
using namespace muq::Pde;

GenericMesh::GenericMesh(boost::property_tree::ptree const&) {}

shared_ptr<GenericMesh::AvailableMeshTypes> GenericMesh::GetMeshMap()
{
  static shared_ptr<GenericMesh::AvailableMeshTypes> meshMap;

  if (!meshMap) { // if it doesn't exist, create it
    meshMap = make_shared<GenericMesh::AvailableMeshTypes>();
  }

  return meshMap;
}

shared_ptr<GenericMesh> GenericMesh::ConstructMesh(boost::property_tree::ptree const& param)
{
  // get the constructor from the map and create the mesh
  return (GenericMesh::GetMeshMap()->at(param.get<string>("mesh.type", "")))(param);
}

shared_ptr<libMesh::Mesh> GenericMesh::GetMeshPtr() const
{
  assert(mesh);
  return mesh;
}

unsigned int GenericMesh::NumElem() const
{
  assert(mesh);
  return mesh->n_elem();
}

