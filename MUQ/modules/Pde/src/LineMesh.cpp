#include "MUQ/Pde/LineMesh.h"
#include "MUQ/Pde/LibMeshInit.h"

using namespace std;
using namespace muq::Pde;

REGISTER_MESH(LineMesh)

LineMesh::LineMesh(boost::property_tree::ptree const& param) : GenericMesh(param)
{
  // get numerical mesh size
  const unsigned int N = param.get<unsigned int>("mesh.N", 0);
  const double L       = param.get<double>("mesh.L", 0.0);
  const double B       = param.get<double>("mesh.B", 0.0);

  mesh = make_shared<libMesh::Mesh>(LibMeshInit::libmeshInit->comm());
  libMesh::MeshTools::Generation::build_line(*mesh, N, B, L, libMesh::EDGE3);
}

