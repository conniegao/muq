#include "MUQ/Pde/GenericEquationSystems.h"

using namespace std;
#if MUQ_PYTHON == 1
using namespace muq::Utilities;
#endif
using namespace muq::Pde;

GenericEquationSystems::GenericEquationSystems(boost::property_tree::ptree const& params)
{
  // make the mesh associated with each equation system
  mesh = GenericMesh::ConstructMesh(params);

  // libmesh equation system can store multiple systems, which are defined on the mesh
  eqnsystem = make_shared<libMesh::EquationSystems>(*(mesh->GetMeshPtr()));
}

#if MUQ_PYTHON == 1
GenericEquationSystems::GenericEquationSystems(boost::python::dict const& dict) :
  GenericEquationSystems(PythonDictToPtree(dict)) {}
#endif

GenericEquationSystems::GenericEquationSystems(string const& xmlFile)
{
  // check to make sure the file exists
  ifstream ifile(xmlFile.c_str());
  boost::property_tree::ptree params;

  // if its good read it
  if (ifile.good()) {
    ifile.close();

    // read the file
    read_xml(xmlFile, params);
  } else { // crash if not
    cerr << "\nWARNING: Not able to open  " << xmlFile <<
    "  please make sure the file exists.  This may cause the program to crash.\n\n";
  }

  // make the mesh associated with each equation system
  mesh = GenericMesh::ConstructMesh(params);

  // libmesh equation system can store multiple systems, which are defined on the mesh
  eqnsystem = make_shared<libMesh::EquationSystems>(*(mesh->GetMeshPtr()));
}

shared_ptr<libMesh::EquationSystems> GenericEquationSystems::GetEquationSystemsPtr() const
{
  assert(eqnsystem);
  return eqnsystem;
}

shared_ptr<GenericMesh> GenericEquationSystems::GetGenericMeshPtr() const
{
  assert(mesh);
  return mesh;
}

void GenericEquationSystems::PrintToExodus(string const& file)
{
  assert(eqnsystem);
  libMesh::ExodusII_IO(eqnsystem->get_mesh()).write_equation_systems(file, *eqnsystem);
}

