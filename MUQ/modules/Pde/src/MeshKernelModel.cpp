
#include "MUQ/Pde/MeshKernelModel.h"

using namespace std;
using namespace muq::Geostatistics;
using namespace muq::Pde;

MeshKernelModel::MeshKernelModel(string const                              & paraName,
                                 shared_ptr<libMesh::EquationSystems> const& parasystem,
                                 vector<Eigen::VectorXd> const             & pts,
                                 unsigned int const                          numBasis,
                                 shared_ptr<CovKernel> const               & kernel) :
  KernelModel(pts, numBasis, kernel), paraName(paraName), parasystem(parasystem) {}

MeshKernelModel::MeshKernelModel(string const                              & paraName,
                                 shared_ptr<libMesh::EquationSystems> const& parasystem,
                                 vector<Eigen::VectorXd> const             & pts,
                                 unsigned int const                          numBasis,
                                 Eigen::VectorXd const                     & mean,
                                 shared_ptr<CovKernel> const               & kernel) :
  KernelModel(pts, numBasis, mean, kernel), paraName(paraName), parasystem(parasystem) {}

shared_ptr<MeshKernelModel> MeshKernelModel::Construct(shared_ptr<GenericEquationSystems> const& parasystem,
                                                       shared_ptr<CovKernel> const             & kernel,
                                                       boost::property_tree::ptree const       & param)
{


  const string paraName       = param.get<string>("MeshKernelModel.Name", "");
  const unsigned int order    = param.get<unsigned int>("MeshKernelModel.Order", 1);
  const unsigned int numBasis = param.get<unsigned int>("MeshKernelModel.KLModes", 10);

  const bool hasSys = parasystem->GetEquationSystemsPtr()->has_system(paraName);

  if (!hasSys) {

    libMesh::System& system = parasystem->GetEquationSystemsPtr()->add_system<libMesh::System>(paraName);

    if (order == 0) {
      system.add_variable(paraName, libMesh::CONSTANT, libMesh::MONOMIAL);
    } else {
      system.add_variable(paraName, libMesh::Order(order));
    }

    system.init();

    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }

    
    return make_shared<MeshKernelModel>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis, kernel);
  } else {
    libMesh::System& system       = parasystem->GetEquationSystemsPtr()->get_system<libMesh::System>(paraName);
    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info
        // for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }

    return make_shared<MeshKernelModel>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis, kernel);
  }
}

shared_ptr<MeshKernelModel> MeshKernelModel::Construct(shared_ptr<GenericEquationSystems> const& parasystem,
                                                       shared_ptr<CovKernel> const             & kernel,
                                                       Eigen::VectorXd const                   & mean,
                                                       boost::property_tree::ptree const       & param)
{
  const string paraName       = param.get<string>("MeshKernelModel.Name", "");
  const unsigned int order    = param.get<unsigned int>("MeshKernelModel.Order", 1);
  const unsigned int numBasis = param.get<unsigned int>("MeshKernelModel.KLModes", 10);

  const bool hasSys = parasystem->GetEquationSystemsPtr()->has_system(paraName);

  if (!hasSys) {
    libMesh::System& system = parasystem->GetEquationSystemsPtr()->add_system<libMesh::System>(paraName);

    if (order == 0) {
      system.add_variable(paraName, libMesh::CONSTANT, libMesh::MONOMIAL);
    } else {
      system.add_variable(paraName, libMesh::Order(order));
    }

    system.init();
    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }
    return make_shared<MeshKernelModel>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis, mean, kernel);
  } else {
    libMesh::System& system       = parasystem->GetEquationSystemsPtr()->get_system<libMesh::System>(paraName);
    const libMesh::MeshBase& mesh = system.get_mesh();

    vector<Eigen::VectorXd> pts(system.n_dofs());

    unsigned int dof = 0;
    for (unsigned int n = 0; n < mesh.n_nodes(); ++n) {
      libMesh::Node node = mesh.node(n);

      if (node.n_comp(system.number(), 0) > 0) { // if this is node holds info
        // for the parameter
        libMesh::Point point = mesh.point(n);

        pts[dof].resize(3);
        for (unsigned int i = 0; i < 3; ++i) {
          pts[dof](i) = point(i);
        }
        ++dof;
      }
    }
    return make_shared<MeshKernelModel>(paraName, parasystem->GetEquationSystemsPtr(), pts, numBasis, mean, kernel);
  }
}

Eigen::VectorXd MeshKernelModel::EvaluateImpl(Eigen::VectorXd const&
                                              inputs)
{
  Eigen::VectorXd result = LinearModel::EvaluateImpl(inputs);

  // convert to vector and set it as the solution in the parasystem
  muq::Utilities::Translater<Eigen::VectorXd, vector<double> > trans(result);

  parasystem->get_system(paraName).solution->operator=(*trans.GetPtr());

  return result;
}

