function WriteTriMesh(p, t, meshname)
% 
% WriteTriMesh Write ASCII file holding the mesh defined by p and t
% 
% INPUTS:
%   p: An N by dim array of nodal positions
%   t: An M by dim+1 array holding the nodes of each triangle
%   meshname: A string holding the name of the mesh.  The nodes will be
%             stored in a file meshname.node and the element will be stored
%             in a separate file meshname.ele


% first, open the element file
fid = fopen(strcat(meshname,'.ele'),'w');

% write the number of elements and the simplex size
fprintf(fid,'%d\t%d\n',size(t,1),size(t,2));

% loop over the elements
for i=1:size(t,1)
    fprintf(fid,'%d',i);
    for j=1:size(t,2)
        fprintf(fid,'\t%d',t(i,j));
    end
    fprintf(fid,'\n');
end



% open the node file
fid = fopen(strcat(meshname,'.node'),'w');

% write the number of elements and the dimension
fprintf(fid,'%d\t%d\n',size(p,1),size(p,2));


for i=1:size(p,1)
    fprintf(fid,'%d',i);
    for j=1:size(p,2)
        fprintf(fid,'\t%1.4e',p(i,j));
    end
    fprintf(fid,'\n');
end
