//
//  OptAlgBase.cpp
//
//
//  Created by Matthew Parno on 5/13/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

// self include
#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

// other muq includes
#include "MUQ/Optimization/Problems/OptProbBase.h"


using namespace muq::Optimization;
using namespace std;


/** Construct this base from a problem pointer and a property list.  Currently, this only stores the problem ptr and
 * does nothing with the property tree.
 */
OptAlgBase::OptAlgBase(std::shared_ptr<OptProbBase> ProbPtr,
                       boost::property_tree::ptree& properties,
                       bool                         unconst,
                       bool                         partBound,
                       bool                         fullBound,
                       bool                         linear,
                       bool                         nonlinear,
                       bool                         equality,
                       bool                         inequality,
                       bool                         stochastic) : directlyHandlesUnconst(unconst),
                                                                  handlesPartlyBoundConsts(partBound),
                                                                  requiresFullyBoundConsts(
                                                                    fullBound), handlesLinearConsts(linear),
                                                                  handlesNonlinearConsts(nonlinear),
                                                                  handlesEqualityConsts(equality),
                                                                  handlesInequalityConsts(inequality),
                                                                  handlesStochastic(stochastic), OptProbPtr(ProbPtr)
{
  // get function change tolerance -- i.e. stop when the function value changes less than this in an iteration
  ftol = properties.get("Opt.ftol", 1e-4);

  // get x change tolerance
  xtol = properties.get("Opt.xtol", 1e-4);

  // get absolute function tolerance
  fabstol = properties.get("Opt.fabs", -1e6);

  // additionally, read in step length
  stepLength = properties.get("Opt.StepLength", 1.0);

  // get gradient norm tolerance
  gtol = properties.get("Opt.gtol", 1e-6);

  // get the maximum number of iterations
  maxIts = properties.get("Opt.MaxIts", 100);

  // get the verbosity level
  verbose = properties.get("Opt.verbosity", 0);

  // get the constraint tolerance -- applies to both equalities and inequalities
  ctol = properties.get("Opt.ctol", 1e-5);
}

void CheckProblemAndSolver(std::shared_ptr<OptProbBase> ProbPtr, std::shared_ptr<OptAlgBase> SolvPtr)
{
  // check to make sure the solver can handle bound constraints
  if ((ProbPtr->isUnconstrained()) && (!SolvPtr->directlyHandlesUnconst)) {
    std::cerr <<
      "ERROR: The selected optimization algorithm cannot handle the given unconstrained problem.  Please select an alternative optimizer that can handle unconstrained problems.\n";
    assert(SolvPtr->directlyHandlesUnconst);
  }

  if ((ProbPtr->NumEqualities() > 0) && (!SolvPtr->handlesEqualityConsts)) {
    std::cerr <<
      "ERROR: The selected optimization algorithm cannot handle nonlinear equality constrained problems.  Please select an alternative optimizer or ConstraintHandler that can handle this class of problem.\n";
    assert(ProbPtr->NumEqualities() == 0);
  }

  if ((static_cast<unsigned int>(ProbPtr->NumInequalities()) != ProbPtr->inequalityConsts.Bounds.size()) &&
      (!SolvPtr->handlesInequalityConsts)) {
    std::cerr <<
      "ERROR: The selected optimization algorithm cannot handle nonlinear inequality constrained problems.  Please select an alternative optimizer or ConstraintHandler that can handle this class of problem.\n";
    assert(static_cast<unsigned int>(ProbPtr->NumInequalities()) == ProbPtr->inequalityConsts.Bounds.size());
  }

  if ((ProbPtr->isPartlyBoundConstrained()) && (!SolvPtr->handlesPartlyBoundConsts)) {
    std::cerr <<
      "ERROR: The selected optimization algorithm cannot handle partially bound constrained problems.  Please select an alternative optimizer or ConstraintHandler that can handle this class of problem.\n";
    assert(SolvPtr->handlesPartlyBoundConsts);
  }

  if ((!ProbPtr->isFullyBoundConstrained()) && (SolvPtr->requiresFullyBoundConsts)) {
    std::cerr <<
      "ERROR: The selected optimization algorithm cannot requires a fully bound constrained problems.  Please select an alternative optimizer or ConstraintHandler that can handle this class of problem.\n";
    assert(ProbPtr->isFullyBoundConstrained());
  }

  if ((ProbPtr->isLinearlyConstrained()) && (!SolvPtr->handlesLinearConsts)) {
    std::cerr <<
      "ERROR: The selected optimization algorithm cannot handle the linear constraints in this problem.  Please select an alternative optimizer or ConstraintHandler that can handle this class of problem.\n";
    assert(SolvPtr->handlesLinearConsts);
  }

  if (ProbPtr->isNonlinearlyConstrained() && (!SolvPtr->handlesNonlinearConsts)) {
    std::cerr <<
      "ERROR:  The selected optimization algorithm cannot directly handle the nonlinear constraints in the given problem.  Please select a ConstraintHandler other than Native to handle this problem with this optimizer.\n";
    assert(SolvPtr->handlesNonlinearConsts);
  }

  if (ProbPtr->isStochastic() && (!SolvPtr->handlesStochastic)) {
    std::cerr <<
      "ERROR:  The given problem is stochastic but the selected optimizer cannot handle stochastic problems.  Please use a different optimization scheme that can handle stochastic problems.\n";
    assert(SolvPtr->handlesStochastic);
  }
}

/** Create an instance of OptAlgBase to solve the optimization stored in ProbPtr using the algorithm and algorithm
 * parameters defined in the properties parameter list. */
shared_ptr<OptAlgBase> OptAlgBase::Create(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties)
{
  string Method = properties.get("Opt.Method", "BFGS_Line");

  if (properties.get("Opt.verbosity", 0) > 0) {
    std::cout << "Using optimization method: " << Method << std::endl;
  }

  string CMethod = properties.get("Opt.ConstraintHandler", "AugLag");

  shared_ptr<OptAlgBase> solvPtr;

  if (OptAlgBase::GetOptMap()->count(Method) == 0) {
    std::cerr << "\nERROR: The optimization algorithm, " << Method <<
      ", specified in the property tree does not exist.\n\tExisting algorithms are:\n";
    for (auto i = OptAlgBase::GetOptMap()->begin(); i != OptAlgBase::GetOptMap()->end(); ++i) {
      std::cout << "\t\t" << i->first << std::endl;
    }
    std::cout << "\n";
    assert(OptAlgBase::GetOptMap()->count(Method) > 0);
  }


  // if this is a constrained problem we have to do a little more work
  if (ProbPtr->isConstrained()) {
    // if the constraint handler is Native, check to make sure the Opt.Method can directly handle the constraint set in
    // this optimization problem
    if (!CMethod.compare("Native")) {
      // create a pointer to the optimization solver
      solvPtr = (OptAlgBase::GetOptMap()->at(Method))(ProbPtr, properties);

      // make sure the solver can actually solve this problem
      CheckProblemAndSolver(ProbPtr, solvPtr);
    } else {
      // make sure the Method and constraint handler are different -- helps avoid an infinite loop of AugLag creating
      // AugLag, etc...
      if (!Method.compare(CMethod)) {
        std::cerr <<
          "\nERROR: The specified constraint handler and optimization algorithm are the same.  If the optimization algorithm can directly handle constraints, set \"ConstraintHandler\" to \"Native\" \n";
        assert(Method.compare(CMethod));
      }

      // create a solver using the constraint handler.
      solvPtr = (OptAlgBase::GetOptMap()->at(CMethod))(ProbPtr, properties);

      // make sure the solver can actually solve this problem
      CheckProblemAndSolver(ProbPtr, solvPtr);
    }
  } else {
    // this is not a constrainted problem, so no problem-solver checking is necessary and we can just create the solver
    // directly
    solvPtr = (OptAlgBase::GetOptMap()->at(Method))(ProbPtr, properties);
  }


  return solvPtr;
}

std::shared_ptr<OptAlgBase::AvailableOptMapType> OptAlgBase::GetOptMap()
{
  static std::shared_ptr<OptAlgBase::AvailableOptMapType> map;

  // if the map doesn't yet exist, create it
  if (!map) {
    map = make_shared<OptAlgBase::AvailableOptMapType>();
  }
  return map;
}

