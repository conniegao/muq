
#include "MUQ/Optimization/Problems/SAAProb.h"

#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Optimization;
using namespace std;


SAAProb::SAAProb(shared_ptr<OptProbBase> StochProbPtrIn, int seedVal) : OptProbBase(StochProbPtrIn->GetDim()), seed(
                                                                          seedVal),
                                                                        StochProbPtr(StochProbPtrIn)
{}

/** Evaluate the objective by fixing the random seed . */
double SAAProb::eval(const Eigen::VectorXd& xc)
{
  muq::Utilities::RandomGenerator::SetSeed(seed);

  return StochProbPtr->eval(xc);
}

/** Evaluate the gradient */
double SAAProb::grad(const Eigen::VectorXd& xc, Eigen::VectorXd& gradient)
{
  muq::Utilities::RandomGenerator::SetSeed(seed);

  return StochProbPtr->grad(xc, gradient);
}

