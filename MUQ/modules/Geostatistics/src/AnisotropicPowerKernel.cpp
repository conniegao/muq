
#include "MUQ/Geostatistics/AnisotropicPowerKernel.h"
#include "MUQ/Utilities/LogConfig.h"

using namespace Eigen;
using namespace muq::Geostatistics;

AnisotropicPowerKernel::AnisotropicPowerKernel(Eigen::VectorXd lengthScalesIn, double PowerIn,
                                               double VarIn) : lengthScales(lengthScalesIn), Power(PowerIn), variance(
                                                                 VarIn), Nparams(1 +
                                                                                 lengthScales.rows())
{}


double AnisotropicPowerKernel::Kernel(const Eigen::Ref<const Eigen::VectorXd>& x,
                                      const Eigen::Ref<const Eigen::VectorXd>& y) const
{
  unsigned dimIn = x.rows();
  static Eigen::VectorXd diff;

  diff.resizeLike(x);
  for (unsigned i = 0; i < dimIn; ++i) {
    diff(i) = x(i) - y(i);
  }


  //   double   result = variance  * exp(-1.0 * pow((diff.array() / lengthScales.array()).sum(), Power) / Power);
  double result = variance  * exp(-1.0 * (diff.array().square().matrix().dot(lengthScales)));

  assert(!std::isnan(result)); //can underflow during optimization
  // evaluate the kernel
  return result;
}

Eigen::MatrixXd AnisotropicPowerKernel::BuildCov(const Eigen::MatrixXd& x) const
{
  MatrixXd lengthMat = lengthScales.array().sqrt().matrix().asDiagonal();
  MatrixXd lengthX   = lengthMat * x;

  MatrixXd xSquaredNorms = lengthX.colwise().squaredNorm();

  auto selfNorms = xSquaredNorms.replicate(x.cols(), 1);

  MatrixXd result = 2.0 * lengthX.transpose() * lengthX - selfNorms - selfNorms.transpose();

  result = variance * result.array().exp();
  return result;
}

void AnisotropicPowerKernel::BuildGradMat(const Eigen::MatrixXd& xs, std::vector<Eigen::MatrixXd>& GradMat) const
{
  //NOTE: copy/paste from IsotropicCovKernel

  // make sure all the dimensions match
  int dim = xs.cols();

  //only wipe the gradmat if it's not the right size
  if (static_cast<int>(GradMat.size()) != Nparams) {
    GradMat.resize(Nparams, Eigen::MatrixXd::Zero(dim, dim)); //just resize, no need to clear
  }

  //check the matrices are all the right size, on the off chance the dim has somehow changed
  for (int i = 0; i < Nparams; ++i) {
    GradMat.at(i).resize(dim, dim);
  }

  Eigen::VectorXd grad(Nparams);

  // outer loop -- column index of covariance matrix
  for (int col = 0; col < dim; ++col) {
    // inner loop -- row index of covariance matrx
    for (int row = 0; row <= col; ++row) {
      GetGrad(xs.col(col), xs.col(row), grad);

      for (int i = 0; i < Nparams; ++i) {
        GradMat[i](row, col) = grad[i];
        GradMat[i](col, row) = grad[i];
      }
    }
  }
}

void AnisotropicPowerKernel::GetGrad(const Eigen::Ref<const Eigen::VectorXd>& x,
                                     const Eigen::Ref<const Eigen::VectorXd>& y,
                                     Eigen::VectorXd                        & grad) const
{
  VectorXd diff = x - y;

  assert(false);
  grad                       = VectorXd::Zero(1 + lengthScales.rows());
  grad(0)                    = 1.0; //vary linearly with this term
  grad.tail(grad.rows() - 1) = Kernel(x, y) * diff.array() * lengthScales.array() * -2.0;
}

Eigen::VectorXd AnisotropicPowerKernel::KernelSpatialDerivative(Eigen::VectorXd const& x, Eigen::VectorXd const& xWrt)
{
  VectorXd diff = x - xWrt;

  return Kernel(x, xWrt) * 2.0 * (diff.array() * lengthScales.array());
}

void AnisotropicPowerKernel::SetParms(const Eigen::VectorXd& theta)
{
  assert(theta.rows() == Nparams);
  lengthScales = theta.head(Nparams - 1);
  variance     = theta(Nparams - 1);
}

Eigen::VectorXd AnisotropicPowerKernel::GetParms()
{
  VectorXd result(Nparams);

  result << lengthScales, variance;
  return result;
}

