#include "MUQ/Approximation/Regression/Regressor.h"

using namespace std;
using namespace muq::Modelling;
using namespace muq::Approximation;

Regressor::Regressor(unsigned int const inDim, unsigned int const outDim) :
  OneInputJacobianModPiece(inDim, outDim) {}

void Regressor::Fit(Eigen::MatrixXd const& xs, Eigen::MatrixXd const& ys) 
{
  InvalidateCache();

  return Fit(xs, ys, Eigen::VectorXd::Zero(inputSizes(0)));
}
