#include "MUQ/Approximation/utilities/PolychaosXMLHelpers.h"

#include <assert.h>
#include <map>
#include <string>
#include <iostream>

#include <boost/property_tree/xml_parser.hpp>

#include "MUQ/Utilities/LogConfig.h"

#include <MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h>
#include <MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h>
#include <MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h>
#include <MUQ/Utilities/Quadrature/ExpGrowthQuadratureFamily1D.h>
#include <MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h>
#include <MUQ/Utilities/Variable.h>
#include <MUQ/Utilities/VariableCollection.h>

#include <MUQ/Approximation/smolyak/SmolyakPCEFactory.h>

using namespace std;
using namespace muq::Utilities;
using namespace muq::Approximation;
using namespace boost::property_tree;

shared_ptr<VariableCollection> muq::Approximation::ConstructVariableCollection(boost::property_tree::ptree const& pt)
{
    LOG(INFO) << "Construce pce variable collection";

  //the map will sort the entries by key
  map<int, ptree> sortedVars;

  //make a sorted list of all the variables
  for (auto varPtree : pt) {
    //store pointers to the ptree
    if (varPtree.first.compare("Variable") == 0) {
      sortedVars[varPtree.second.get < int > ("index")] = (varPtree.second);
    }
  }

  //set up the quad and poly types
  GaussPattersonQuadrature1D::Ptr  gpQuad(new GaussPattersonQuadrature1D());
  GaussHermiteQuadrature1D::Ptr    hermiteQuad(new GaussHermiteQuadrature1D());
  ExpGrowthQuadratureFamily1D::Ptr expHermiteQuad(new ExpGrowthQuadratureFamily1D(hermiteQuad));

  LegendrePolynomials1DRecursive::Ptr legendrePoly(new LegendrePolynomials1DRecursive());
  HermitePolynomials1DRecursive::Ptr  hermitePoly(new HermitePolynomials1DRecursive());


  VariableCollection::Ptr varCollection(new VariableCollection());

  //now loop over the ptrees and construct them - varPtree is a pair<int, ptree>
  for (auto varPtree : sortedVars) {
    LOG(INFO) << "Variable block: " << varPtree.second.get<string>("type") << " count " <<
    varPtree.second.get<unsigned>("count", 1);

    //default to one block
    for (unsigned i = 0; i < varPtree.second.get<unsigned>("count", 1); ++i) {
      if (varPtree.second.get<string>("type").compare("Gaussian") == 0) {
        varCollection->PushVariable(varPtree.second.get("name", "x"), hermitePoly, expHermiteQuad);
      } else if (varPtree.second.get<string>("type").compare("Legendre") == 0) {
        varCollection->PushVariable(varPtree.second.get("name", "x"), legendrePoly, gpQuad);
      } else {
        assert(false); //bad ptree
      }
    }
  }

  return varCollection;
}

// shared_ptr<CachedFunctionWrapper> muq::Approximation::ConstructFunctionWrapper(boost::property_tree::ptree const&
// pt)
// {
//  return CachedFunctionWrapper::WrapExecutableFunction(pt.get<string>("ExternalFunction"),
//                                                    pt.get<unsigned>("InputDim"),
//                                                    pt.get<unsigned>("OutputDim"));
// }
//

// shared_ptr<SmolyakPCEFactory> muq::Approximation::ConstructSmolyakPCE(boost::property_tree::ptree const& pt)
// {
//   //set up the dependencies
//   CachedFunctionWrapper::Ptr fn = ConstructFunctionWrapper(pt.get_child("Polychaos.Function"));
//   VariableCollection::Ptr vars =  ConstructVariableCollection(pt.get_child("Polychaos.VariableCollection"));
//
//   //create the factory
//   SmolyakPCEFactory::Ptr pceFactory(new SmolyakPCEFactory(vars, fn));
//
//   return pceFactory;
// }


// PolynomialChaosExpansion::Ptr muq::Approximation::ComputePCE(boost::property_tree::ptree const& pt)
// {
//   //make the factory
//   SmolyakPCEFactory::Ptr pceFactory = ConstructSmolyakPCE(pt);
//
//   PolynomialChaosExpansion::Ptr pce = pceFactory->StartAdaptiveTimed(pt.get<int>("Polychaos.InitialSimplex", 1),
//                                                                   pt.get<double>("Polychaos.AdaptationTime", 0.0));
//
//   //try to grab the path
//   boost::optional<string> pceOutputPath = pt.get_optional<string>("Polychaos.PceOutputPath");
//   if(pceOutputPath)
//   {
//     //if there was one, print it
//     pce->print(*pceOutputPath);
//   }
//
//   return pce;
// }


