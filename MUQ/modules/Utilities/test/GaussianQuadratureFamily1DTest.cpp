#include <iostream>

#include <Eigen/Core>
#include "gtest/gtest.h"

#include "MUQ/Utilities/Quadrature/GaussianQuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/ExpGrowthQuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/LinearGrowthQuadratureFamily1D.h"
#include "MUQ/Utilities/EigenUtils.h"

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;

/**
 * Test the Lengendre quadrature points and weights of order 5 and 10 against pre-stored values,
 * which match Abramowitz and Stegun to all printed digits (on spot checks), and were computed with these functions.
 */
TEST(UtilitiesQuadrature1DTest, LegendreTest)
{
  //store of right answers
  RowVectorXd weightsStored10(10);

  weightsStored10 << 0.066671344308688, 0.14945134915058, 0.21908636251598, 0.26926671931, 0.29552422471475,
  0.29552422471475, 0.26926671931, 0.21908636251598, 0.14945134915058, 0.066671344308688;
  RowVectorXd nodesStored10(10);
  nodesStored10 << -0.97390652851717, -0.86506336668898, -0.67940956829902, -0.43339539412925, -0.14887433898163,
  0.14887433898163, 0.43339539412925, 0.67940956829902, 0.86506336668898, 0.97390652851717;


  RowVectorXd weightsStored5(5);
  weightsStored5 << 0.23692688505619, 0.47862867049937, 0.56888888888889, 0.47862867049937, 0.23692688505619;
  RowVectorXd nodesStored5(5);
  nodesStored5 << -0.90617984593866, -0.53846931010568, 2.1924363667095e-16, 0.53846931010568, 0.90617984593866;

  //compute and test
  GaussLegendreQuadrature1D LegendreQuad;

  int size   = 10;
  double tol = 1e-12; //known expected accuracy of these points/weights
  std::shared_ptr<RowVectorXd> nodes(LegendreQuad.GetNodes(size));
  std::shared_ptr<RowVectorXd> weights(LegendreQuad.GetWeights(size));

  EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored10, tol));
  EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored10, tol));

  size    = 5;
  nodes   = LegendreQuad.GetNodes(size);
  weights = LegendreQuad.GetWeights(size);

  EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored5, tol));
  EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored5, tol));


  //	cout.precision(20);
  //	cout.setf(ios::fixed);
  //
  //	LegendreQuad.GetNodes(96).raw_print_trans(cout, "nodes =");
  //	LegendreQuad.GetWeights(96).raw_print_trans(cout, "weights =");


  //The following code will output the results for the above code.

  /*cout.precision(14);
   *	cout << "rowvec weightsStored" << size << ";" << endl << "weightsStored" << size;
   *	for(int i=0; i<size; i++)
   *	{
   *		cout << "<< " << weights[i];
   *  }
   *  cout << ";" << endl;
   *
   *
   *
   *  cout << "rowvec nodesStored" << size << ";" << endl << "nodesStored" << size;
   *  for(int i=0; i<size; i++)
   *  {
   *  cout << "<< " << nodes[i];
   *  }
   *  cout << ";" << endl;*/
}


/**
 * Test the Hermite quadrature points and weights of order 6 and 12 against pre-stored values,
 * which match Abramowitz and Stegun to all printed digits (on spot checks), and were computed with these functions.
 */
TEST(UtilitiesQuadrature1DTest, HermiteTest)
{
  //store of correct answers
  RowVectorXd weightsStored12(12);

  weightsStored12 << 2.6585516843563e-07, 8.5736870435878e-05, 0.0039053905846291, 0.051607985615884, 0.26049231026416,
  0.57013523626248, 0.57013523626248, 0.26049231026416, 0.051607985615884, 0.0039053905846291, 8.5736870435879e-05,
  2.6585516843563e-07;
  RowVectorXd nodesStored12(12);
  nodesStored12 << -3.8897248978698, -3.0206370251209, -2.2795070805011, -1.5976826351526, -0.94778839124016,
  -0.31424037625436, 0.31424037625436, 0.94778839124016, 1.5976826351526, 2.2795070805011, 3.0206370251209,
  3.8897248978698;


  RowVectorXd weightsStored6(6);
  weightsStored6 << 0.0045300099055089, 0.15706732032286, 0.72462959522439, 0.72462959522439, 0.15706732032286,
  0.0045300099055088;
  RowVectorXd nodesStored6(6);
  nodesStored6 << -2.3506049736745, -1.3358490740137, -0.43607741192762, 0.43607741192762, 1.3358490740137,
  2.3506049736745;

  //compute and test
  GaussHermiteQuadrature1D HermiteQuad;


  int size                             = 6;
  double tol                           = 1e-12;
  std::shared_ptr<RowVectorXd> nodes   = HermiteQuad.GetNodes(size); //compute some nodes and weights
  std::shared_ptr<RowVectorXd> weights = HermiteQuad.GetWeights(size);

  EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored6, tol));     //test them
  EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored6, tol));

  size    = 12;
  nodes   = HermiteQuad.GetNodes(size);                           //compute some nodes and weights
  weights = HermiteQuad.GetWeights(size);

  EXPECT_TRUE(MatrixApproxEqual(*weights, weightsStored12, tol)); //test them
  EXPECT_TRUE(MatrixApproxEqual(*nodes, nodesStored12, tol));
}

TEST(UtilitiesQuadrature1DTest, ExpHermite)
{
  //just check there are the right number of nodes
  ExpGrowthQuadratureFamily1D expQuad(GaussHermiteQuadrature1D::Ptr(new GaussHermiteQuadrature1D));

  int size = 5;

  std::shared_ptr<RowVectorXd> nodes   = expQuad.GetNodes(size); //compute some nodes and weights
  std::shared_ptr<RowVectorXd> weights = expQuad.GetWeights(size);

  EXPECT_EQ(16,  nodes->cols());
  EXPECT_EQ(16,  weights->cols());
  EXPECT_EQ(31u, expQuad.GetPrecisePolyOrder(size));
}

TEST(UtilitiesQuadrature1DTest, LinearLegendre)
{
  //just check there are the right number of nodes
  LinearGrowthQuadratureFamily1D linQuad(GaussLegendreQuadrature1D::Ptr(new GaussLegendreQuadrature1D), 4);

  int size = 5;

  std::shared_ptr<RowVectorXd> nodes   = linQuad.GetNodes(size); //compute some nodes and weights
  std::shared_ptr<RowVectorXd> weights = linQuad.GetWeights(size);

  EXPECT_EQ(17,  nodes->cols());
  EXPECT_EQ(17,  weights->cols());
  EXPECT_EQ(33u, linQuad.GetPrecisePolyOrder(size));
}
