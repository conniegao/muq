import sys
import unittest
import numpy as np # math stuff in python
import random

from libmuqUtilities import RandomGenerator as rg

class RandomGeneratorTest(unittest.TestCase):
    def testGaussian(self):
        
        scalar = rg.GetNormal()
        
        vector = rg.GetNormalRandomVector(10)
        
        matrix = rg.GetNormalRandomMatrix(10,20)
        
    def testUniform(self):
        
        scalar = rg.GetUniform()
        
        vector = rg.GetUniformRandomVector(10)
        
        matrix = rg.GetUniformRandomMatrix(10,20)
        
    def testIntegers(self):
        
        scalar = rg.GetUniformInt(2,11)
        
        vector = rg.GetUniqueUniformInts(0,100,20)
        
    def testGamma(self):
        
        scalar = rg.GetGamma(1.0,2.0)

