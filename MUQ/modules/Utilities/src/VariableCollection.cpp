
#include "MUQ/Utilities/VariableCollection.h"

#include <vector>
#include <assert.h>

#include <boost/serialization/export.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <boost/serialization/vector.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

#include "MUQ/Utilities/Polynomials/RecursivePolynomialFamily1D.h"
#include "MUQ/Utilities/Variable.h"
#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"

using namespace std;
using namespace muq::Utilities;

VariableCollection::VariableCollection() {}

VariableCollection::~VariableCollection() {}

void VariableCollection::PushVariable(Variable::Ptr newVar)
{
  this->variables.push_back(newVar);
}

void VariableCollection::PushVariable(std::string                      name,
                                      RecursivePolynomialFamily1D::Ptr polyFamily,
                                      QuadratureFamily1D::Ptr          quadFamily)
{
  this->variables.emplace_back(make_shared<Variable>(name, polyFamily, quadFamily));
}

void VariableCollection::PushVariable(std::string                      name,
                                      QuadratureFamily1D::Ptr          quadFamily)
{
  this->variables.emplace_back(make_shared<Variable>(name, quadFamily));
}

void VariableCollection::PushVariable(std::string                      name,
                                      RecursivePolynomialFamily1D::Ptr polyFamily)
{
  this->variables.emplace_back(make_shared<Variable>(name, polyFamily));
}

unsigned int VariableCollection::length()
{
  return this->variables.size();
}

Variable::Ptr VariableCollection::GetVariable(unsigned int i)
{
  //check for out of bounds
  assert(i < this->variables.size());

  //return a reference
  return this->variables[i];
}

template<class Archive>
void VariableCollection::serialize(Archive& ar, const unsigned int version)
{
  ar& variables;
}

//BOOST_EXPORT_CLASS_IMPLEMENT(VariableCollection)


template void VariableCollection::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void VariableCollection::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

//
//BOOST_CLASS_EXPORT_IMPLEMENT(VariableCollection)

