#include "MUQ/Utilities/Polynomials/ProbabilistHermite.h"

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/access.hpp>

#include <boost/math/special_functions/factorials.hpp>
#include <boost/math/constants/constants.hpp>

using namespace muq::Utilities;


double ProbabilistHermite::alpha(double x, unsigned int k)
{
  assert(k < 250 && "Hermite are difficult to compute and are only tested to 200th order. May be unstable beyond this");
  return -1.0 * x;
}

double ProbabilistHermite::beta(double x, unsigned int k)
{
  return k;
}

double ProbabilistHermite::phi0(double x)
{
  return 1.0;
}

double ProbabilistHermite::phi1(double x)
{
  return x;
}

double ProbabilistHermite::GetNormalization_static(unsigned int n)
{
  return boost::math::constants::root_two_pi<double>() * boost::math::factorial<double>(n);
}

double ProbabilistHermite::gradient_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/HermiteH/20/01/01/
  if (order == 0) {
    //order 0 is a constant
    return 0;
  } else {
    return static_cast<double>(order) * evaluate_static(order - 1, x);
  }
}

double ProbabilistHermite::secondDerivative_static(unsigned int order, double x)
{
  //this definition provided by http://functions.wolfram.com/Polynomials/HermiteH/20/01/01/
  if (order < 2) {
    //order 0 is a constant
    return 0;
  } else {
    return static_cast<double>(order) * gradient_static(order - 1, x);
  }
}


Eigen::VectorXd ProbabilistHermite::GetMonomialCoeffs(const int order){
  
  Eigen::VectorXd monoCoeffs = Eigen::VectorXd::Zero(order+1);
  
  if(order==0){
    monoCoeffs(0) = 1.0;
  }else if(order ==1){
    monoCoeffs(1) = 1.0;
  }else if(order==2){
    monoCoeffs(0) = -1.0;
    monoCoeffs(2) = 1.0;
  }else if(order==3){
    monoCoeffs(1) = -3.0;
    monoCoeffs(3) = 1.0;
  }else if(order==4){
    monoCoeffs(0) = 3.0;
    monoCoeffs(2) = -6.0;
    monoCoeffs(4) = 1.0;
  }else if(order==5){
    monoCoeffs(1) = 15.0;
    monoCoeffs(3) = -10.0;
    monoCoeffs(5) = 1.0;
  }else{
    Eigen::VectorXd oldOldCoeffs = Eigen::VectorXd::Zero(order+1);
    Eigen::VectorXd oldCoeffs = Eigen::VectorXd::Zero(order+1);
    oldOldCoeffs(0) = 1.0; // constant term -- same as monomial
    oldCoeffs(1) = 1.0;    // linear term -- same as monomial
    for(int i=2; i<=order; ++i){
      monoCoeffs = Eigen::VectorXd::Zero(order+1);
      monoCoeffs.tail(order) = oldCoeffs.head(order);
      monoCoeffs -= (i-1)*oldOldCoeffs;
      
      oldOldCoeffs = oldCoeffs;
      oldCoeffs = monoCoeffs;
    }
  }
  return monoCoeffs;
  
}

template<class Archive>
void ProbabilistHermite::serialize(Archive& ar, const unsigned int version)
{
  //nothing to serialize except for the parent and the type of the object
  ar& boost::serialization::base_object<Static1DPolynomial<ProbabilistHermite>>(*this);
}

BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::Static1DPolynomial<ProbabilistHermite>)
BOOST_CLASS_EXPORT_IMPLEMENT(muq::Utilities::ProbabilistHermite)

template void ProbabilistHermite::serialize(boost::archive::text_oarchive& ar, unsigned int version);
template void ProbabilistHermite::serialize(boost::archive::text_iarchive& ar, const unsigned int version);

template void Static1DPolynomial<ProbabilistHermite>::serialize(boost::archive::text_oarchive& ar, const unsigned int version);
template void Static1DPolynomial<ProbabilistHermite>::serialize(boost::archive::text_iarchive& ar, const unsigned int version);


