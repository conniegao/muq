#include "MUQ/Utilities/python/QuadraturePython.h"

#include "MUQ/Utilities/Quadrature/QuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/ClenshawCurtisQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/ExpGrowthQuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/GaussChebyshevQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussProbabilistHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussianQuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/LinearGrowthQuadratureFamily1D.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;

boost::python::list QuadratureFamily1D::PyGetWeights(unsigned int const order){
  return GetPythonVector(*GetWeights(order));
}

boost::python::list QuadratureFamily1D::PyGetNodes(unsigned int const order){
  return GetPythonVector(*GetNodes(order));
}

void muq::Utilities::ExportQuadratureFamily(){
  
  py::class_<QuadratureFamily1D, shared_ptr<QuadratureFamily1D>, boost::noncopyable> exportQuad("QuadratureFamily1D", py::no_init);
  
  exportQuad.def("GetWeights", &QuadratureFamily1D::PyGetWeights);
  exportQuad.def("GetNodes", &QuadratureFamily1D::PyGetNodes);
  exportQuad.def("GetPrecisePolyOrder", &QuadratureFamily1D::GetPrecisePolyOrder);
}

void muq::Utilities::ExportGaussPattersonQuadrature(){
  py::class_<GaussPattersonQuadrature1D, shared_ptr<GaussPattersonQuadrature1D>, py::bases<QuadratureFamily1D>, boost::noncopyable> exportQuad("GaussPattersonQuadrature1D");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<GaussPattersonQuadrature1D>, shared_ptr<QuadratureFamily1D>>();
}

void muq::Utilities::ExportLinearGrowthQuadratureFamily(){
  py::class_<LinearGrowthQuadratureFamily1D, shared_ptr<LinearGrowthQuadratureFamily1D>, py::bases<QuadratureFamily1D>, boost::noncopyable> exportQuad("LinearGrowthQuadratureFamily1D",py::init<std::shared_ptr<QuadratureFamily1D>,unsigned int>());
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<LinearGrowthQuadratureFamily1D>, shared_ptr<QuadratureFamily1D>>();
}

void muq::Utilities::ExportClenshawCurtisQuadrature(){
  py::class_<ClenshawCurtisQuadrature1D, shared_ptr<ClenshawCurtisQuadrature1D>, py::bases<QuadratureFamily1D>, boost::noncopyable> exportQuad("ClenshawCurtisQuadrature1D");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<ClenshawCurtisQuadrature1D>, shared_ptr<QuadratureFamily1D>>();
}

void muq::Utilities::ExportExpGrowthQuadratureFamily(){
  py::class_<ExpGrowthQuadratureFamily1D, shared_ptr<ExpGrowthQuadratureFamily1D>, py::bases<QuadratureFamily1D>, boost::noncopyable> exportQuad("ExpGrowthQuadratureFamily1D",py::init<std::shared_ptr<QuadratureFamily1D>>());
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<ExpGrowthQuadratureFamily1D>, shared_ptr<QuadratureFamily1D>>();
}

void muq::Utilities::ExportGaussChebyshevQuadrature(){
  py::class_<GaussChebyshevQuadrature1D, shared_ptr<GaussChebyshevQuadrature1D>, py::bases<QuadratureFamily1D>, boost::noncopyable> exportQuad("GaussChebyshevQuadrature1D");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<GaussChebyshevQuadrature1D>, shared_ptr<QuadratureFamily1D>>();
}

void muq::Utilities::ExportGaussProbHermiteQuadrature(){
  py::class_<GaussProbabilistHermiteQuadrature1D, shared_ptr<GaussProbabilistHermiteQuadrature1D>, py::bases<GaussianQuadratureFamily1D>, boost::noncopyable> exportQuad("GaussProbabilistHermiteQuadrature1D");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<GaussProbabilistHermiteQuadrature1D>, shared_ptr<QuadratureFamily1D>>();
  py::implicitly_convertible<shared_ptr<GaussProbabilistHermiteQuadrature1D>, shared_ptr<GaussianQuadratureFamily1D>>();
}

void muq::Utilities::ExportGaussHermiteQuadrature(){
  py::class_<GaussHermiteQuadrature1D, shared_ptr<GaussHermiteQuadrature1D>, py::bases<GaussianQuadratureFamily1D>, boost::noncopyable> exportQuad("GaussHermiteQuadrature1D");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<GaussHermiteQuadrature1D>, shared_ptr<QuadratureFamily1D>>();
  py::implicitly_convertible<shared_ptr<GaussHermiteQuadrature1D>, shared_ptr<GaussianQuadratureFamily1D>>();
}


void muq::Utilities::ExportGaussianQuadratureFamily(){
  py::class_<GaussianQuadratureFamily1D, shared_ptr<GaussianQuadratureFamily1D>, py::bases<QuadratureFamily1D>, boost::noncopyable> exportQuad("GaussianQuadratureFamily1D",py::no_init);

  
  // convert to parents
  py::implicitly_convertible<shared_ptr<GaussianQuadratureFamily1D>, shared_ptr<QuadratureFamily1D>>();
}

void muq::Utilities::ExportGaussLegendreQuadrature(){
  py::class_<GaussLegendreQuadrature1D, shared_ptr<GaussLegendreQuadrature1D>, py::bases<GaussianQuadratureFamily1D>, boost::noncopyable> exportQuad("GaussLegendreQuadrature1D");
  
  // convert to parents
  py::implicitly_convertible<shared_ptr<GaussLegendreQuadrature1D>, shared_ptr<QuadratureFamily1D>>();
  py::implicitly_convertible<shared_ptr<GaussLegendreQuadrature1D>, shared_ptr<GaussianQuadratureFamily1D>>();
}