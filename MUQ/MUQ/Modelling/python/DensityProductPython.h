
#ifndef DENSITYPRODUCTPYTHON_H_
#define DENSITYPRODUCTPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Modelling/DensityProduct.h"

namespace muq {
namespace Modelling {
/// Export muq::Modelling::DensityProduct to python
inline void ExportDensityProduct()
{
  // expose to python
  boost::python::class_<DensityProduct, std::shared_ptr<DensityProduct>,
                        boost::python::bases<Density, ModPiece>, boost::noncopyable> exportDensityProduct(
    "DensityProduct",
    boost::python::init<int const>());

  // allow conversion to python
  boost::python::implicitly_convertible<std::shared_ptr<DensityProduct>, std::shared_ptr<ModPiece> >();
}
} // namespace muq
} // namespace Modelling

#endif // ifndef DENSITYPRODUCTPYTHON_H_
