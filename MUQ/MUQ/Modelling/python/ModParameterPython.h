#ifndef MODPARAMETERPYTHON_H_
#define MODPARAMETERPYTHON_H_

#include "MUQ/Modelling/ModParameter.h"

namespace muq {
  namespace Modelling {
    void ExportModParameter();
  } 
}

#endif
