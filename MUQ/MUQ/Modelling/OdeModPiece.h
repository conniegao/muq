
#ifndef OdeModPiece_h_
#define OdeModPiece_h_

#include <boost/property_tree/ptree_fwd.hpp>

// muq includes
#include "MUQ/Modelling/ModPiece.h"

#include <string>

namespace muq {
namespace Modelling {
/** @class OdeModPiece
 * @brief Provides a mechanism for defining transient models from ODE right hand sides and an observation model.
 * @details This class allows ODE's to be defined from a right hand side function and an observation function.  This
 * class solves methods of the form
 * \f[
 * \frac{dx}{dt} = f(x,t,p)
 * \f]
 * where x is the state vector, t is the scalar time, and p is a parameter vector.  The output of this modpiece is given
 * by an ``observation" function $g(x,t,p)$ that is evaluated on the state at fixed times.
 */
class OdeModPiece : public ModPiece {
public:

  /** Constructor
   * @param[in] fIn A modpiece defining the right hand side of the ODE.  Must take at least two inputs, the first being
   * a length 1 Eigen::VectorXd holding the time, and the second holding an Eigen::VectorXd with the state.
   * @param[in] gIn A modpiece defining the observation vector, must have the same number of inputs as fIn.
   * @param[in] evalTimes Times to evaluate the state and observation ModPiece.
   * @param[in] parameterSizes Holds the size of each parameter input.  Note that the first size must be the size of the
   * state vector passed to f and g.
   * @param[in] properties Holds integration properties such as integrator method (Forward Euler, Backward Euler,
   * etc...) as well as integration tolerances
   */
  OdeModPiece(std::shared_ptr<ModPiece>    fIn,
              std::shared_ptr<ModPiece>    gIn,
              Eigen::VectorXd const      & evalTimes,
              boost::property_tree::ptree properties);


  OdeModPiece(std::shared_ptr<ModPiece> fIn, std::shared_ptr<ModPiece> gIn, Eigen::VectorXd const& evalTimes);

  OdeModPiece(std::shared_ptr<ModPiece> fIn, Eigen::VectorXd const& evalTimes, boost::property_tree::ptree properties);

  OdeModPiece(std::shared_ptr<ModPiece> fIn, Eigen::VectorXd const& evalTimes);

protected:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;


  //virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
  //                                           Eigen::VectorXd const             & target,
  //                                           int const                           inputDimWrt = 0) override;


  //virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& input,
  //                                    Eigen::VectorXd const             & sensitvity,
  //                                    int const                           inputDimWrt) override;


  // the right hand side and observation modPieces
  std::shared_ptr<ModPiece> f, g;

  // observation times
  const Eigen::VectorXd obsTimes;

  // store the integration properties
  // boost::property_tree::ptree& integrateProperties;

private:

  std::string linSolver;
  double      abstol, reltol;
  std::string intMethod, solveMethod;
  double      maxStepSize;
  int checkPtGap;

  bool preferAdjoint; // do we prefer using adjoint sensitivities or jacobian sensitivites?
  
  void RunCvodesForward(std::vector<Eigen::VectorXd> const& inputs,
                        bool                                runSens,
                        int const                           inputDimWrt,
                        Eigen::VectorXd                   & eval,
                        Eigen::MatrixXd                   & jac) const;

  void RunCvodesAdjoint(std::vector<Eigen::VectorXd> const& inputs,
                        Eigen::VectorXd              const& outputSens,
                        int const                           inputDimWrt,
                        Eigen::VectorXd                   & gradient) const;
};

// OdeModPiece
} // namespace Modelling
} // namespace muq


#endif // ifndef OdeModPiece_h_
