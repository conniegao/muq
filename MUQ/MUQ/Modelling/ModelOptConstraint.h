
#ifndef _ModelOptConstraint_h
#define _ModelOptConstraint_h

#include "MUQ/Optimization/OptConstraintBase.h"
#include "MUQ/Modelling/Model.h"

namespace muq {
namespace Modelling {
/** @class ModelOptConstraint
 *  @author Matthew Parno
 *  @brief Allows a forward model to be used as an equality or inequality constraint in an optimization problem.
 *  @details This class allows users to define an equality or inequality constraint based on a forward model implemented
 *     using Models.
 */
class ModelOptConstraint : public muq::Optimization::OptConstraintBase {
public:

  /** The usual constructor, user must specify whether the constraint will be an equality or inequality, what Model
   *  should be used as input, and which Model should be used as output.  Remember that all the Models live on a graph,
   *  so when you call InMod.Update(), all models "downstream" will also be updated, which means that the output can be
   *  found with OutMod.Eval().
   *  @param[in] isEquality boolean specifying whether this constraint should be treated as an equality constraint (if
   *     value is true) or an inequality constraint (if value is false).
   *  @param[in] MIn The input model.  The Update() function of this model is called.
   *  @param[in] MOut The output model.  The Eval() function of this model is used to get the value of the constraint.
   *  @param[in] cval The value of the constraint.  The optimization problem will be to find a parameter such that
   *     ModOut.Eval() returns cval in the case of an equality constraint, or that ModOut.Eval() returns a vector g,
   * such
   *     that g-cval <=0 holds componentwise.
   */
  ModelOptConstraint(bool isEquality, const Model& MIn, const Model& MOut, const Eigen::VectorXd cval);

  /** Evaluate the constraint.
   *  @param[in] xc Where to evaluate the constraint.
   *  @return A vector containing A*xc-b
   */
  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc);

  /** Get the gradient of the constraint at a point xc, given the sensitivity of some objective to the output of the
   *  constraint.
   *  @param[in] xc Where to evaluate the constraint.
   *  @param[in] SensIn The sensitivity of some objective to the output of this constraint -- i.e. the Sensitivity of
   * the
   *     Augmented Lagrangian subproblem to the output of the constraint.
   *  @param[out] grad The gradient
   *  @return A vector containing A*xc-b
   */
  virtual Eigen::VectorXd grad(const Eigen::VectorXd& xc, const Eigen::VectorXd& SensIn, Eigen::VectorXd& grad);

protected:

  Model ModIn, ModOut;
  Eigen::VectorXd ConstVal;
};

// class ModelOptConstraint
} // namesapce Modelling
} // namespace muq


#endif // ifndef _ModelOptConstraint_h
