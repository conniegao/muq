
#ifndef STANMODELINTERFACE_H
#define STANMODELINTERFACE_H

#include <vector>
#include <iostream>

#include <stan/model/prob_grad.hpp>

#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"


namespace muq {
namespace Inference {
/**
 * A class to make one of our models available to the Stan mcmc methods. The class stan::model::prob_grad
 * is their equivalent of a SamplingProblem, so we provide an inheriting class that calls into our models. We need only
 *provide
 * a log_prob and grad_log_prob method, so this is easy to provide from any muq::Inference::AbstractSamplingProblem.
 **/
class StanModelInterface : public stan::model::prob_grad {
public:

  ///Construct an interface by passing in a muq model.
  StanModelInterface(std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem);

  virtual ~StanModelInterface() {}

  ///Run the grad and the density and return them, unless initialized is false.
  virtual double grad_log_prob(std::vector<double>& params_r,
                               std::vector<int>   & params_i,
                               std::vector<double>& gradient,
                               std::ostream        *output_stream = 0);


  ///Run the density and return it, unless initialized is false.
  virtual double log_prob(std::vector<double>& params_r, std::vector<int>& params_i, std::ostream *output_stream = 0);

  double temperature = 1.0;

private:

  ///Our copy of the sampling problem we're wrapping
  std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem;
};
}
}

#endif // STANMODELINTERFACE_H
