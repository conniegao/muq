
#ifndef STANHMC_H
#define STANHMC_H

#include <boost/random.hpp>
#include <stan/mcmc/adaptive_hmc.hpp>

#include "MUQ/Inference/MCMC/MCMCKernel.h"


namespace muq {
namespace Inference {
class StanModelInterface;


/**
 * This class is a wrapper around Stan's adaptive HMC class. It adapts the step size based on an 
 * acceptance probability target, but performs a fixed number of HMC steps. You can tune the number
 * of HMC leaps, but that's it.
 * 
 * Note that we do not provide the random number generator for this class, so be careful
 * about that assumption.
 *
 * See also Inference/ProblemClasses/StanModelInterface.h
 */
class StanHMC : public MCMCKernel {
public:

  StanHMC(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
          boost::property_tree::ptree                            & properties);

  virtual ~StanHMC() {}

  ///Perform setup using the start state. Most do not use this, but the Stan interfaces do.
  virtual void SetupStartState(Eigen::VectorXd const& xStart) override;


  //   typedef std::tuple < std::shared_ptr < muq::Inference::MCMCState >, bool> NewState; //the new state and whether
  // it reflects an accepted move
  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;


  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const;

  /** Get the acceptance rate.
  *             @return The acceptance rate in [0,1]
  */
  virtual double GetAccept() const;
  
  virtual void PrintStatus() const override;
	
private:

  /// keep track of the number of accepted steps
  int numAccepts = 0;

  /// keep track of the total number of times ConstructNextState was called
  int totalSteps = 0;

  std::shared_ptr < stan::mcmc::adaptive_hmc < boost::mt19937 >> hmcSampler;

  std::shared_ptr<muq::Inference::StanModelInterface> modelInterface;

  int Nleaps;
};
}
}

#endif // STANNUTS_H
