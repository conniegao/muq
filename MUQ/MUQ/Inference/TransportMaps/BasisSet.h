#ifndef BasisSet_h_
#define BasisSet_h_

#include "MUQ/Inference/TransportMaps/UnivariateBasis.h"

#include <memory>
#include <vector>

namespace muq{
  namespace Inference{
    
    class BasisSet{
    
    public:
      unsigned int size() const{return bases.size();};
      const std::shared_ptr<UnivariateBasis>& at(int dim) const{return bases.at(dim);};
      std::shared_ptr<UnivariateBasis>& at(int dim){return bases.at(dim);};
      
      BasisSet& operator+=(const BasisSet& rhs);
      BasisSet& operator+=(std::shared_ptr<UnivariateBasis> const& rhs);
      
      void Add(std::shared_ptr<UnivariateBasis> const& rhs);
      
      void push_back(std::shared_ptr<UnivariateBasis> const& rhs){bases.push_back(rhs);};
      void reserve(int size){bases.reserve(size);};
      
    private:
      std::vector<std::shared_ptr<UnivariateBasis>> bases;
    };
    
    std::shared_ptr<BasisSet> operator+=(std::shared_ptr<BasisSet> const& lhs, std::shared_ptr<BasisSet> const& rhs);
    std::shared_ptr<BasisSet> operator+=(std::shared_ptr<BasisSet> const& lhs, std::shared_ptr<UnivariateBasis> const& rhs);
  }
}




#endif