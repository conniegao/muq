/** @defgroup Inference
   @brief Contains tools for running MCMC, computing MAP points, importance sampling, and building transport maps.
 
    <h2>MCMC Overview:</h2>
     Markov chain Monte Carlo (MCMC) is one way to approximate general probility distributions with samples.
     MCMC methods define a Markov chain over the parameter space whose stationary distribution is the target distribution.
     Thus, after some initial burnin, the states of the Markov chain can be used as samples of the target distribution.

     MUQ has many different MCMC algorithms and an extensible interface for adding new methods.  However, all of the methods
     can be used in the same way.  First, we define a sampling problem, this could be a Bayesian posterior 
     (as implemented in muq::Inference::InferenceProblem ), or a general probability density (as implemented 
      in muq::Inference::SamplingProblem).  After defining the problem, we set the MCMC proposal, and the run the chain.
      
	  An example of defining a sampling problem and running a simple Metropolis-Hastings random walk algorithm is shown in the steps below.
      
	  First, include the appropriate header files:
\code{.cpp}

#include <Eigen/Core>  // needed for matrices and vectors

#include <boost/property_tree/ptree.hpp>  // A ptree instance will hold the parameters defining our MCMC chain

#include "MUQ/Modelling/EmpiricalRandVar.h"               // An EmpiricalRandVar instance will hold the resulting Markov chain
#include "MUQ/Modelling/GaussianDensity.h"                // needed to define the target density
#include "MUQ/Inference/MCMC/MCMCBase.h"                  // Needed for the MCMC methods
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h" // A SamplingProblem instance will be passed to the MCMC solver

using namespace std;
using namespace muq::Inference;
using namespace muq::Modelling;

\endcode

Now to create the muq::Inference::SamplingProblem.  We will first create a  muq::Modelling::GaussianDensity defining the target density, 
and then create a muq::Inference::SamplingProblem that the MCMC algorithms can work with.

\code{.cpp}

// define the mean of the target density
Eigen::VectorXd mean(2);
mean(0) = 1.5;
mean(2) = 2.5;

// define the covariance of the target density -- must be symmetric
Eigen::MatrixXd gamma(2,2);
gamma(0,0) = 4.0;
gamma(1,0) = 2.0;
gamma(0,1) = 2.0;
gamma(1,1) = 6.0;

// create a Gaussian density with the mean and covariance from above
shared_ptr<GaussianDensity> targetDensity = make_shared<GaussianDensity>(mean,gamma);

// use the targetDensity to define a sampling problem
shared_ptr<AbstractSamplingProblem> problem = make_shared<SamplingProblem>(targetDensity);

\endcode
Now that the sampling problem has been defined, we are ready to define the MCMC sampler and perform the sampling.
  The following snippet shows a ptree defining a simple Metropolis-Hastings random walk algorithm.
  Keep in mind that ptrees can easily be constructed from XML files, enabling parameters to be set at run-time.  
\code{.cpp}
boost::property_tree::ptree params;
params.put("MCMC.Method", "SingleChainMCMC");    // We are going to run a single MCMC chain
params.put("MCMC.Kernel", "MHKernel");           // The transition kernel with be based on the Metropolis-Hastings rule
params.put("MCMC.Proposal", "MHProposal");       // The propolsal used by the Metropolis-Hastings kernel will be a Gaussian random walk
params.put("MCMC.Steps", 1000);                  // The number of steps to use in the chain
params.put("MCMC.BurnIn", 10);                   // Of the 1000 steps taken by the chain, throw away the first 10 as burn-in steps
params.put("MCMC.MH.PropSize", 2.0);             // The variance of the isotropic Gaussian proposal is set to 2 
params.put("Verbose", 3);                        // The MCMC algorith should be verbose, setting this to 0 instead, would yield no output

// using the parameters in the above ptree and the sampling problem, construct the MCMC instance
shared_ptr<MCMCBase> mcmcSolver = MCMCBase::ConstructMCMC(problem, params);

// run the chain from an initial starting point
Eigen::VectorXd startingPoint = Eigen::VectorXd::Zero(dim);
shared_ptr<EmpiricalRandVar> mcmcChain = mcmcSolver->Sample(startingPoint);
\endcode
The muq::Modelling::EmpiricalRandVar instance mcmcChain now contains the MCMC chain.  
From this object, we can approximate moments of the target density, compute the effective sample size of the chain,
 or store the results in an HDF5 file.  See the documentation for muq:Modelling::EmpiricalRandVar for details.

Options for the ptree entries can be found in the class definitions.  For different kernels, consult the children of muq::Inference::MCMCKernel,
 which include
<ul>
<li>muq::Inference::DR -- An transition kernel using the modified delayed rejection acceptance probability of \cite Mira2001 .</li>
<li>muq::Inference::MHKernel -- The basic Metropolis-Hastings rule \cite Metropolis1953 \cite Hastings1970 .</li>
<li>muq::Inference::TransportMapKernel -- The transport-map accelerated MCMC kernel of \cite Parno2014 </li>
<li>muq::Inference::StanHMC -- A wrapper to the adaptive Hamiltonian Monte Carlo method implemented in STAN \cite STAN</li>
<li>muq::Inference::StanNUTS -- A wrapper to the No-U-Turn sampler in STAN \cite STAN</li>
<li>muq::Inference::IncrementalApproximationMCMC -- A kernel based on the local incremental approximations in \cite Conrad2014 </li>
</ul>
For the various proposals in MUQ, see the definitions for children of muq::Inference::MCMCProposal.  This includes
<ul>
<li>muq::Inference::MHProposal -- A basic zero-mean Gaussian random walk proposal</li>
<li>muq::Inference::AM -- An implementation of the adaptive-Metropolis algorithm in \cite Haario2001 .  This can be combined with the DRKernel to create the DRAM method from \cite Haario2006 </li>
<li>muq::Inference::PreMALA -- A preconditioned Metropolis Adjusted Langevin Algorithm (MALA).</li>
<li>muq::Inference::AMALA -- An implementation of the adatpive MALA method in \cite Atchade2006 </li>
<li>muq::Inference::MMALA -- An implementation of the simplified Manifold MALA (smMALA) approach described by \cite Girolami2011 .  The metric is defined as part of the sampling problem.</li>
<li>muq::Inference::GaussianIndependence -- An indepence proposal based on a fixed Gaussian distribution.</li>
<li>muq::Inference::MixtureProposal -- A mixture proposal that combines two or more other proposal mechanisms.</li>
</ul>


<h2>MAP Solver Overview:</h2>
In many cases, we want to find the maximum of a probability density, i.e. the mode.  In the Bayesian setting, the posterior mode is often called the maximum aposteriori point (MAP).
  We can find this point with the tools from the \ref Optimization module.  The process for finding the MAP is very similar running MCMC: (1) An instance of muq::Inference::SamplingProblem is created,
 (2) optimization parameters are defined in a property tree, and (3) The MAP solver is run.  For example, assume a muq::Inference::SamplingProblem instance has been created as above.  Finding the
maximum of the density simply involves
\code{.cpp}
boost::property_tree::ptree params;
params.put("Opt.Method", "BFGS_Line");    // We are going to use a BFGS method to compute the maximum

// using the parameters in the above ptree and the sampling problem, construct a MAP solver
shared_ptr<MAPbase> mapSolver = MAPbase::Create(problem,params);

// run the optimization algorithm to get the maximum
Eigen::VectorXd startingPt = Eigen::VectorXd::Zero(dim);
Eigen::VectorXd MapPt = mapSolver->Solve(startingPt);
\endcode
The type of optimization method we want to use (and subsequent tolerances/options) are set in the property tree.  More details can be found in the documentation for the \ref Optimization module.  

<h2>Transport Map Overview:</h2>
Transport maps are multivariate random variable transformations that are useful in many areas of statistics and uncertainty quantification.  MUQ can construct transport maps from samples of a target distribution to a Gaussian reference distribution.



 */

