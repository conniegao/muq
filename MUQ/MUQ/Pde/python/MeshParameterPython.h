#ifndef MESHPARAMETERPYTHON_H_
#define MESHPARAMETERPYTHON_H_

#include "MUQ/Utilities/python/PythonStaticConstructorWrapper.h"

#include "MUQ/Modelling/python/ModPiecePython.h"

#include "MUQ/Pde/MeshParameter.h"

namespace muq {
namespace Pde {

  /// A python wrapper around muq::Pde::MeshParameter
  /**
     Allows the user to implement children of muq::Pde::MeshParameter in python
   */
 class MeshParameterPython : public MeshParameter, public boost::python::wrapper<MeshParameter> {
 public:
   
   virtual ~MeshParameterPython() = default;

   /// Constructor
   /**
      @param[in] systemName The name of the system
      @param[in] system The system holding the mesh the parameter is define over
      @param[in] para The options for the muq::Pde::MeshParameter (see muq::Pde::MeshParameter::Create)
    */
   MeshParameterPython(std::string const& systemName, std::shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& pt);

   /// Python static Create methods
   /**
      Cannot use muq::Pde::MeshParameter::Create because it return the parent class muq::Pde::MeshParameter.  The python create method return itself.  However, the options are the same (see muq::Pde::MeshParameter::Create).
      @param[in] system The system holding the mesh the parameter is define over
      @param[in] para The options for the muq::Pde::MeshParameter
      \return The muq::Pde::MeshParameter 
   */
  static std::shared_ptr<MeshParameterPython> PyCreate(std::shared_ptr<GenericEquationSystems> const& system,
							      boost::python::dict const            & para);

  /// Evaluate the mesh parameter at a point 
  /**
     The user must override this; this function evaluates the parameter at a point.
     @param[in] x x-coordinate 
     @param[in] y y-coordinate (zero if less than two dimensions) 
     @param[in] z z-coordinate (zero if less than three dimensions) 
     @param[in] var The variable name
  */
  virtual libMesh::Number ParameterEvaluate(double const x, double const y, double const z, std::string const& var) const override;

  /// Evaluate method for when the parameter has no inputs 
  /**
     \return The parameter at each DOF
   */
  boost::python::list PyEvaluate();

  /// Evaluat method for when the parameter has inputs
  /**
     \return The parameter at each DOF
  */
  boost::python::list PyEvaluateInputs(boost::python::list const& inputs);

 private:
 };

 /// Export the parameter to python interface
 void ExportMeshParameter();

} // namespace Pde
} // namespace muq

#endif // ifndef MESHPARAMETERPYTHON_H_
