#ifndef REGRESSORPYTHON_H_
#define REGRESSORPYTHON_H_

#include "MUQ/Approximation/Regression/Regressor.h"

namespace muq {
  namespace Approximation {
    void ExportRegressor();
  } // namespamce Approximation
} // namespace muq

#endif
