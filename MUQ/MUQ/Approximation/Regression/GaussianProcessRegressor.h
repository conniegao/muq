#ifndef _GaussianProcessRegressor_h
#define _GaussianProcessRegressor_h

#include <vector>

#include <Eigen/Core>
#include <Eigen/Cholesky>
#include <Eigen/QR>
#include <boost/graph/graph_concepts.hpp>

// other muq includes
#include "MUQ/Approximation/Regression/Regressor.h" //here for the constructor macro

namespace muq {
namespace Geostatistics {
class AnisotropicPowerKernelNugget;
}

namespace Approximation {

/** @class GaussianProcessRegressor
 *
 *  @brief A basic implementation of kriging for a scalar valued function.
 *
 *  This class provides a basic implementation of kriging.  The idea is to build an approximation
 *     to a forward expression (could be an objective function, error term, etc...) by fitting a gaussian process to
 *     input-output data of the forward expression.
 *
 * The algorithms for this implementation are taken from the DACE: http://www2.imm.dtu.dk/~hbni/dace/
 *
 * It fits a non-zero mean regression function using the AnisotropicPowerKernel, which estimates correlation lengths
 * for each input dimension, and is currently pinned to be a Gaussian kernel.
 *
 *  \todo Add a constructor that takes the forward expression as input and uses some sort of LHS or design of
 * experiments
 *     to build the input/output data used for construction
 */
class GaussianProcessRegressor : public Regressor {
public:


  
  GaussianProcessRegressor(int dimIn, int dimOut);
  GaussianProcessRegressor(int dimIn,int dimOut, boost::property_tree::ptree& properties);

  virtual ~GaussianProcessRegressor() = default;
  
  /** @brief Evaluate the approximation and return result as scalar
   *  @details This function will evaluate the regression function in child classes
   *  @param[in] x A vector holding the location to evaluate \f$\tilde{f}(x)\f$
   *  @return A double holding \f$\tilde{f}(x)\f$
   */
  virtual Eigen::VectorXd          EvaluateImpl(const Eigen::VectorXd& x) override;

  /** @brief Perform the regression for a set of input and outputs.
   *  @details Using the given input an output points, implementations of this function should approximate this
   *     input/output relationship using some sort of regression technique.  For example, to approximate the function
   *     \f$y=f(x)\f$, a set of input points \f$x_i\f$ and a set of output points \f$y_i\f$ could be passed to this
   *     function.
   *  @param[in] xs A matrix holding the input points, each column is a point.
   *  @param[in] ys A matrix holding the output points, each columns is a point and there must be the same number of
   *     columns as xs.
   * @param[in] center The center is ignored
   */
  virtual void            Fit(const Eigen::MatrixXd& xsIn, const Eigen::MatrixXd& ysIn, Eigen::VectorXd const& center) override;

  ///Same as other fit, without extraneous center parameter
  virtual void            Fit(const Eigen::MatrixXd& xsIn, const Eigen::MatrixXd& ysIn) override;

	
  virtual Eigen::VectorXd GetPredictedVariance(const Eigen::VectorXd& x);


  virtual Eigen::MatrixXd JacobianImpl(const Eigen::VectorXd& x) override;
  
  void FitNoOpt(const Eigen::MatrixXd& xsIn, const Eigen::MatrixXd& ysIn);
  Eigen::VectorXd SelectNextPointRand(Eigen::VectorXd const& x, std::vector<Eigen::VectorXd> &candidates);
  
  Eigen::VectorXd GetCorrelationLengths();
  Eigen::VectorXd GetNuggetVariance(const Eigen::VectorXd& x);
  
  double GetNugget();

private:

  
  std::shared_ptr<muq::Geostatistics::AnisotropicPowerKernelNugget> Kernel;

  Eigen::MatrixXd scaledXs;
  Eigen::MatrixXd scaledYs;
  
  Eigen::VectorXd outputEmpiricalMean;
  
  Eigen::VectorXd xScales;
  Eigen::VectorXd yScales;
  double nuggetScale = 1e-6;
  double correlationLengthScale = .1;
  
  Eigen::LDLT<Eigen::MatrixXd> cholCreator;
	Eigen::MatrixXd KinvYs;
	
};

}
} // namespace muq

#endif // ifndef _GaussianProcessRegressor_h
