
#ifndef GAUSSLEGENDREQUADRATURE1D_H_
#define GAUSSLEGENDREQUADRATURE1D_H_


#include <boost/serialization/access.hpp>
#include <Eigen/Core>

#include "MUQ/Utilities/Quadrature/GaussianQuadratureFamily1D.h"

namespace muq {
  namespace Utilities {
/**
 * Impelements 1D gaussian quadrature for legendre rules, that is, uniform unity weight
 * over domain [-1,1] by extending the virtual class GaussianQuadratureFamily1D.
 */
class GaussLegendreQuadrature1D : public GaussianQuadratureFamily1D {
public:

  GaussLegendreQuadrature1D();
  virtual ~GaussLegendreQuadrature1D();

  ///Implements the function that provides the monic  coefficients.
  virtual Eigen::RowVectorXd GetMonicCoeff(unsigned int const j) const;

  ///Implements the function that provides the integral of the weight function over the domain.
  virtual double             IntegralOfWeight() const;

private:

  ///Make class serializable
  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);
};
}
}
#endif /* GAUSSLEGENDREQUADRATURE1D_H_ */
