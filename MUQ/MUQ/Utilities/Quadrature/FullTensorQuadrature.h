#ifndef FULLTENSORQUADRATURE_H_
#define FULLTENSORQUADRATURE_H_

#include <Eigen/Core>

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Utilities/Quadrature/Quadrature.h"


namespace muq {
  namespace Utilities {

class VariableCollection;

/**
 * The simplest possible quadrature rule, a full tensor expansion.
 * Creates a the complete tensor product of the points of the given order
 * 1D quadrature rules for each dimension. The 1D quadrature order can
 * be isotropic or vary per dimension. Note that an isotropic order
 * does not mean isotropic number of points in each dimension, if different
 * 1D quadrature rules are used.
 *
 * Probably not the most useful class for actual analysis, because you have to know exactly
 * what order you want, but is an important building block for sparse
 * quadrature routines.
 */
class FullTensorQuadrature : public Quadrature {
public:

  typedef std::shared_ptr<FullTensorQuadrature> Ptr;

  ///Isotropic order
  FullTensorQuadrature(std::shared_ptr<muq::Utilities::VariableCollection> variables, unsigned int const order);

  ///Non-isotropic order
  FullTensorQuadrature(std::shared_ptr<muq::Utilities::VariableCollection> variables, Eigen::RowVectorXu const& order);
  virtual ~FullTensorQuadrature();

  ///Implementation of the nodes and weights computation.
  void GetNodesAndWeights(Eigen::MatrixXd& nodes, Eigen::VectorXd& weights);

private:

  ///Holds the anisotropic order of the quadrature
  Eigen::RowVectorXu order;
};
}
}

#endif /* FULLTENSORQUADRATURE_H_ */
