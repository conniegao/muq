
#ifndef RANDOMGENERATORPYTHON_H
#define RANDOMGENERATORPYTHON_H

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Utilities/python/PythonTranslater.h"

namespace muq {
namespace Utilities {
class RandomGeneratorPython : public RandomGenerator, public boost::python::wrapper<RandomGenerator> {
public:

  ///Convenience method - return a vector of random numbers
  static boost::python::list GetUniformRandomVectorPy(int const n);

  ///Convenience method - return a matrix of random numbers
  static boost::python::list GetUniformRandomMatrixPy(int const m, int const n);

  /// Generate N unique random integers from the range lb, ub.  This function is similar to randperm in matlab
  static boost::python::list GetUniqueUniformIntsPy(int lb, int ub, int N);

  ///Convenience method - return a vector of random numbers
  static boost::python::list GetNormalRandomVectorPy(int const n);

  ///Convenience method - return a matrix of random numbers
  static boost::python::list GetNormalRandomMatrixPy(int const m, int const n);
};

void ExportRandomGenerator();
}
}

#endif // ifndef HDF5WRAPPERPYTHON_H
