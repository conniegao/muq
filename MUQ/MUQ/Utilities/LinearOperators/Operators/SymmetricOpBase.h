
#ifndef _SymmetricOpBase_h
#define _SymmetricOpBase_h

#include <Eigen/Core>
#include "MUQ/Utilities/LinearOperators/Operators/LinOpBase.h"
#include "MUQ/Utilities/LinearOperators/Solvers/CG.h"

namespace muq {
namespace Utilities {
class SymmetricOpBase : public LinOpBase {
public:

  SymmetricOpBase(int size) : LinOpBase(size, size) {}

  /** Apply the symmetric linear operator to a vector */
  virtual Eigen::VectorXd apply(const Eigen::VectorXd& x) = 0;

private:
};
} // namespace Utilities
} // namespace muq

#endif // ifndef _SymmetricOpBase_h
