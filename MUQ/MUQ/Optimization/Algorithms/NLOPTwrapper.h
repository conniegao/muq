
#ifndef _NLOPTWRAPPER_H_
#define _NLOPTWRAPPER_H_

// external library includes
#include <nlopt.h>

// local muq-related includes
#include "MUQ/Optimization/Algorithms/OptAlgBase.h"


namespace muq {
namespace Optimization {
/** @class NLOPTwrapper
 *   @ingroup Optimization
 *   @brief Wrapper around NLOPT optimization algorithms
 *   @details This provides a wrapper around the NLOPT package produced by Steven Johnson at MIT.  NLOPT provides a C
 *      implementation of many optimization algorithms.  The goal of this class is to allow muq to seemlessly call the C
 *      implementations of the algorithms in NLOPT.
 */
class NLOPT : public OptAlgBase {
public:

  REGISTER_OPT_CONSTRUCTOR(NLOPT)
  /** Construct an nlopt based optimization solver from the settings in a ptree.  The ptree should use NLOPT as
   *  Opt.Method and should have an Opt.NLOPT section definign the parameters necessary for the specific NLOPT
   * algorithm.
   *  @param[in] ProbPtr A shared pointer to the optimization problem object
   *  @param[in] properties Parameter list containing optimization method and parameters
   */
  NLOPT(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);

  /** Solve the optimization using NLOPT.
   *  @param[in] x0 The initial point to start the optimization
   *  @return The point found by the optimization algorithm.  Note that you could GetStatus() after running solve to
   * make
   *     sure this point is a local min.
   */
  virtual Eigen::VectorXd solve(const Eigen::VectorXd& x0);

protected:

  // convert a string to an nlopt_algorithm
  static nlopt_algorithm str2alg(const std::string& method);

  // stopping criteria
  double ftol_rel, ftol_abs, xtol_rel, xtol_abs, minf_max_delta;

  int maxEvals, population;
  unsigned long randSeed;

  // store NLOPT algorithm
  nlopt_algorithm algorithm, localAlgorithm;

private:

  REGISTER_OPT_DEC_TYPE(NLOPT)
};
} // namespace Optimization
} // namespace muq

#endif // ifndef _NLOPTWRAPPER_H_
