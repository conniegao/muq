
#ifndef _LinearLeastSquares_h
#define _LinearLeastSquares_h

#include <vector>
#include <memory>

#include <Eigen/Core>

namespace muq {
namespace Utilities {
class LinOpBase;
}
}


namespace muq {
namespace Optimization {
class LinearLeastSquares {
public:

  /** Solve a standard overdetermined linear least squares problem (Ax-d) using Eigen types. */
  static Eigen::VectorXd solve(const Eigen::MatrixXd& A, const Eigen::MatrixXd& d);

  /** Solve a quadratically constrained overdetermined(min(Ax-d) s.t. x^TBx = s) linear least squares problem using
   *  Eigen types. */
  static Eigen::VectorXd solve(const Eigen::MatrixXd& V, const Eigen::VectorXd& d, const Eigen::MatrixXd& B, double s);

  static Eigen::VectorXd solve(const Eigen::MatrixXd& V, const Eigen::VectorXd& d, const Eigen::VectorXd& B, double s);

  /** Solve a least squares system defined by the linear operator AOp and the transpose of this linear operator in the
   *  operator ATransOp. This function implements the lsqr method which is equilavent to solving the normal equations
   *  with the conjugate gradient method.
   */
  static Eigen::VectorXd solve(std::shared_ptr<muq::Utilities::LinOpBase> AOp,
                               std::shared_ptr<muq::Utilities::LinOpBase> ATransOp,
                               const Eigen::VectorXd                           & d);
};
} // namespace Optimization
} // namespace muq

#endif // ifndef _LinearLeastSquares_h
