/** \example pde-example-1.cpp
 *   <h1>Introduction</h1>
 *   <p>
 *   The first MuqPdeLib tutorial!  This tutorial will explain how to implement, solve, and use partial differential
 *equations in MUQ!
 *   </p>
 *
 *   <h3>About this example</h3>
 *   Lets begin with the system of equations
 *   \f{eqnarray*}{
 *   \frac{d}{d x} \left( \kappa \frac{du}{dx} \right) &=& f_1 \\
 *   \frac{d}{d x} \left( \kappa \frac{dv}{dx} \right) &=& f_2
 *   \f}
 *   for scalar \f$\kappa\f$ and \f$f_i\f$ with boundary conditions
 *  \f{eqnarray*}{
 *  \begin{array}{ccc}
 *  \left.\frac{du}{dx}\right|_{x=0} = 0 & \mbox{and} & \left.\frac{dv}{dx}\right|_{x=0} = 0 \\
 *  u(1) = 0 & \mbox{and} & v(1) = 1.
 *  \end{array}
 *  \f}
 *  This system of equations has analytical solution
 *  \f{eqnarray*}{
 *  u(x) &=& x^2-2 \\
 *  v(x) &=& x^2
 *  \f}
 *
 *  The weak form is
 *  \f{eqnarray*}{
 *   \int_0^1 \kappa \frac{d \phi}{d x} \frac{du}{dx} - \phi f_1 \, dx &=& 0 \\
 *   \int_0^1 \kappa \frac{d \phi}{d x} \frac{dv}{dx} - \phi f_2 \, dx &=& 0
 * \f}
 *  The residual needs to implement the integrands of the weak form at a point.
 */

#include "MUQ/Pde/LibMeshInit.h"
#include "MUQ/Pde/PDEModPiece.h"

using namespace std;
using namespace muq::Pde;

/**
 *  This is the class that implements the residual.  In this case, it inherits from muq::PDE::NontransientPDE because
 *this system does not depend on time.  The user needs to implement the residaul; if boundary condtions are not
 *overwritten homogenous Neumann are assumed.
 */
class PoissonExample : public Nontransient {
public:

  // Required constructor
  PoissonExample(Eigen::VectorXi const& inputSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree const& para) :
    Nontransient(inputSizes, system, para) {}
  
private:

  /*
   *  Implement the residual as a templated member function. Three scalar types are requried:
   *  returnScalar - The scalar type RETURNED by the function unknownScalar - The scalar type of the state variables (u
   *and v in this case) parameterScalar - The scalar type of the parameters (kappa, f1, and f2 in this case)
   */
  template<typename returnScalar, typename unknownScalar, typename parameterScalar>
  inline returnScalar ResidualImpl(string const                        & varName,
                                   libMesh::Real const                   phi,
                                   libMesh::RealGradient const         & dphi,
                                   vector<unknownScalar> const&,
                                   vector<vector<unknownScalar> > const& grad) const
  {
    // get the parameter number form the system
    const unsigned int var = vars.right.at(varName);

    // make a scalar for the force
    parameterScalar force = 0.0;

    // Each equation has a different forcing
    if (varName.compare("u") == 0) {        // u equation
      // the name of the force "f1" is an option in the ptree given to the constructor
      force = GetScalarParameter<parameterScalar>("f1");
    } else if (varName.compare("v") == 0) { // v equation
      // the name of the force "f2" is an option in the ptree given to the constructor
      force = GetScalarParameter<parameterScalar>("f2");
    }

    // the name of the kappa parameter "kappa" is an option in the ptree given to the constructor
    const parameterScalar kappa = GetScalarParameter<parameterScalar>("kappa");

    /* The differential operator becomes (-kappa * dphi * gradu) in the weak form.  "dphi" is three dimensional (x, y,
     * and z).  Since this problem is 1D the y and z entries will ALWAYS be 0:
     *  dphi: [dphidx, dphidy=0, dphidz=0]
     *
     *  gradu is a vector of vectors:
     *  gradu: [dudx, dudy=0, dudz=0] gradv: [dvdx, dvdy=0, dvdz=0]
     */
    returnScalar resid = -kappa *dphi(0) * grad[var][0] - phi * force;

    // return the residual
    return resid;
  }

  // The pure virtual functions we need to overwrite cannot be templated; this macro takes care of that
  RESIDUAL(ResidualImpl)

  // If a point is on the dirichlet boundary return true (defaults to always false)
  inline virtual bool IsDirichlet(libMesh::Point const& point) const override
  {
    if (point(0) == 1.0) { // right side is dirichlet
      return true;
    }
    return false;
  }

  // Since the dirichlet conditions are not homogenous we need to overwrite this function too
  inline virtual double DirichletBCs(string const& varName, libMesh::Point const&) const override
  {
    // u equation dirichlet condition
    if (varName.compare("u") == 0.0) {
      return 0.0;
    }

    // v equation dirichlet condition
    if (varName.compare("v") == 0.0) {
      return 1.0;
    }
    return 0.0;
  }
};

// three of three macros to register this PDE
REGISTER_PDE(PoissonExample)

int main(int argc, char **argv)
{
  // libmesh must be initialized
  LibMeshInit initLibMesh(argc, argv);

  boost::property_tree::ptree param;

  // parameters to create a mesh
  param.put("mesh.type", "LineMesh");
  param.put("mesh.N", 25);
  param.put("mesh.L", 1.0);

  // some of the PDE parameters
  param.put("PDE.Unknowns", "u,v");
  param.put("PDE.UnknownOrders", "1,2");
  param.put("PDE.GaussianQuadratureOrder", 4);
  param.put("PDE.Name", "PoissonExample");
  param.put("PDE.Parameters", "kappa,f1,f2");
  param.put("PDE.IsScalarParameter", "true,true,true");

  // solvers
  param.put("KINSOL.LinearSolver", "Dense");
  param.put("KINSOL.MaxNewtonStep", 1.0e2);

  // create an equation system (holds the PDEs, spatially dependent parameters, and mesh)
  auto system = make_shared<GenericEquationSystems>(param);

  Eigen::VectorXi inputSizes(3);
  inputSizes << 1, 1, 1;

  // create a nontransient PDE
  auto pde = ConstructPDE(inputSizes, system, param);

  Eigen::VectorXd result = pde->Evaluate(Eigen::VectorXd::Zero(pde->outputSize),
                                         1.0 / 8.0 * Eigen::VectorXd::Ones(1),
                                         1.0 / 2.0 * Eigen::VectorXd::Ones(1),
                                         1.0 / 4.0 * Eigen::VectorXd::Ones(1));

  // save the result to an exodus file (notice it matches the analytic solution)
  pde->PrintToExodus("example-1.exo", result);
}

