/** \example inference-example-2.cpp
 *  <h3> About this example </h3>
 *  <p> This example demonstrates how to sample from a user specified target distribution.
 *  We will sample from a 2D 'banana' distribution using Delayed-Rejection Adaptive Metropolis
 *  (DRAM). The DRAM algorithm is introduced in "DRAM: Efficient adaptive MCMC" by H. Haario
 *  et al. found at http://www.math.helsinki.fi/reports/Preprint374.pdf.
 * 
 *  For our example, the target distribution is: 
 *  \f[
 *  \pi_\mathbf{x}(x_1,x_2) \propto \exp{\left( -\frac{1}{2}\left[\left(\frac{x_1}{a}\right)^2
 *  + \left(a\,x_2 - ab\,x_1^2-a^2\right)^2 \right] \right)}
 *  \f]
 *  where
 *  \f[ a = 1 \;\;\;\; b = 4 \f]
 *  Once again will will follow the three steps: (1) define the target density
 *  (2) sample from the density using MCMC, and (3) extract useful information
 *  from the samples.
 *  </p>
 *
 *  <h3> Line by line explanation </h3>
 *  See example-2.cpp for finely documented code.
 */

#include <iostream>
#include <Eigen/Core>
#include <boost/property_tree/ptree.hpp>

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/MCMC/MCMCBase.h"
#include "MUQ/Inference/MCMC/MCMCProposal.h"

using namespace std;
using namespace Eigen;

using namespace muq::Modelling;
using namespace muq::Inference;

/// PART I: Defining the target density and posing it as a sampling problem
// instead of using a predefined density we will directly create a class which
// inherits from Density and implement its log density
// the class BananaDens will create a our banana shaped density
class BananaDens : public muq::Modelling::Density {
public:
  BananaDens() : Density(2 * Eigen::VectorXi::Ones(1), // input sizes (we have one input of size 2)
			 true, // does this class have an efficient Gradient implemented?
			 false, // does this class have an efficient Jacobian implemented?
			 false, // does this class have an efficient Jacobian action implemented?
			 false // does this class have an efficient Hessian implemented?
			 ) {}
  // constants for our banana density
  const double a = 1.0;
  const double b = 4.0;

private:
  // implement the log density 
  virtual double LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override
  {
    // first, do the transformation to the Gaussian domain
    double x1 = input.at(0) (0) / a;
    double x2 = a * input.at(0) (1) - a * b * (pow(input.at(0) (0), 2.0) + a * a);

    return -0.5 * (x1 * x1 + x2 * x2);
  }

  // implement the gradient
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sens,
                                       int                                 inputDimWrt) override
  {
    Eigen::VectorXd output(2);
    const double    y1 = input.at(0) (0);
    const double    y2 = input.at(0) (1);

    output(0) = -y1 / a + 2 * a * a * y2 * b * y1 - 2 *a *a *b *b *pow(y1, 3.0) - 2 * pow(a, 4.0) * b * b * y1;
    output(1) = -a * a * y2 + a * a * b * (y1 * y1 + a * a);
    return output;
  }
};

int main()
{
  int dim = 2;

  // create an instance of our banana density
  auto ourDens = make_shared<BananaDens>();

  // the class SamplingProblem uses the density to create a sampling problem 
  // that MCMC solvers can use
  auto problem = make_shared<SamplingProblem>(ourDens);
  
  /// PART II: Defining the MCMC algorithm and parameters for DRAM
  // just like in example-1, we will use the ptree to specify parameters for DRAM
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "DR");
  params.put("MCMC.DR.ProposalSpecification", "DRAM");
  params.put("MCMC.Steps", 1000);
  params.put("MCMC.BurnIn", 10);
  params.put("MCMC.MH.PropSize", 1.5);
  params.put("MCMC.DR.stages", 3);
  params.put("MCMC.DR.NumSteps", 500000);
  params.put("MCMC.AM.AdaptSteps", 2);
  params.put("MCMC.AM.AdaptStart", 5);
  params.put("MCMC.AM.AdaptScale", 2.4 * 2.4 / 2.0);
  params.put("Verbose", 1);

  // the following line connects the problem (of type SamplingProblem)
  // and the property tree that we just constructed to fully construct
  // the MCMC task
  auto mcmc = MCMCBase::ConstructMCMC(problem, params);

  /// PART III: Sampling using MCMC and extracting information from samples
  // we sample from the distribution by calling Sample() and passing in
  // an Eigen vector for the starting point. this constructs an EmpRvPtr 
  // which is a pointer to a class that stores all the samples.
  // the following commands are identical to that of the first example
  Eigen::VectorXd startingPoint = Eigen::VectorXd::Zero(dim);
  EmpRvPtr mcmcChain = mcmc->Sample(startingPoint);

  // we can obtain the mean and covariance of samples by calling getMean() 
  // getCov() from the EmpRvPtr
  Eigen::VectorXd mean = mcmcChain->getMean();
  Eigen::MatrixXd cov  = mcmcChain->getCov();
  cout << "\nSample Mean:\n";
  cout << mean.transpose() << endl << endl;
  cout << "Sample Covariance:\n";
  cout << cov << endl << endl;

  // we can find the effective sample size (as an Eigen vector) using getEss()
  Eigen::VectorXd ess = mcmcChain->getEss();
  // find the minimum and maximum ess
  cout << "Minimum ESS: " << ess.minCoeff() << endl;
  cout << "Maximum ESS: " << ess.maxCoeff() << endl << endl;

  // the samples themselves can be written out in plain text for processing
  mcmcChain->WriteRawText("mcmcChain.txt");
  cout << "Raw samples are written in mcmcChain.txt" << endl;
}

